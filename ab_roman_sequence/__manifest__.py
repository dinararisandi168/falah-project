# -*- coding: utf-8 -*-
{
    'name': 'Roman Sequence',
    'summary': '''
        Converter Modul Roman Sequence''',
    'description': '''
        Converter Modul Roman Sequence Odoo 10 to Odoo 12 
    ''',
    'author': 'Yastaqiim Muqorrobin',
    'website': "http://www.ismata.co.id",
    'category': 'Custom Module',
    'version': '0.1',
    'depends': ['base'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
    'application': True,
    'installable': True,
}