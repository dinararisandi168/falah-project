{
    'name': "Finance & Accounting Menu",

    'summary': """
        Finance & Accounting Menu
    """,

    'description': """
        All Menu Finance & Accounting
    """,

    'author': "PT. ISMATA NUSANTARA ABADI",
    'website': "http://www.ismata.co.id",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'account', 'stock', 'sale','ab_account_budget', 'hr_payroll', 'om_account_asset', 'aa_payment_request', 'ab_purchase', 'ab_billyet'],
    'data': ['views/views.xml'],
}