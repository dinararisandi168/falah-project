from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from datetime import datetime, date
from dateutil import relativedelta
import math


class ReportCutiWizard(models.TransientModel):
    _name = 'report.cuti.wizard'
    _description = 'Report Cuti Wizard'
    
    date_from = fields.Date(string='Start Date', default=datetime.now().strftime('%Y-%m-01'))
    date_to = fields.Date(string='End Date', default=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1)))
    month = fields.Selection([
        ('01', 'Januari'),
        ('02', 'Februari'),
        ('03', 'Maret'),
        ('04', 'April'),
        ('05', 'Mei'),
        ('06', 'Juni'),
        ('07', 'Juli'),
        ('08', 'Agustus'),
        ('09', 'September'),
        ('10', 'Oktober'),
        ('11', 'November'),
        ('12', 'Desember'),
    ], string='Bulan', default=datetime.now().strftime('%m'))
    year = fields.Char(string='Tahun', default=datetime.now().strftime('%Y'))

    @api.onchange('month')
    def _onchange_month(self):
        if self.month:
            TanggalAwal = ("%s-%s-01" % (self.year,self.month))
            self.date_from = datetime.strptime(TanggalAwal, "%Y-%m-%d").date()
            self.date_to = str(self.date_from + relativedelta.relativedelta(months=+1, day=1, days=-1))

    @api.onchange('year')
    def _onchange_year(self):
        angka = ['0','1','2','3','4','5','6','7','8','9']
        for i in self.year:
            if i in angka and len(self.year) == 4:
                TanggalAwal = ("%s-%s-01" % (self.year,self.month))
                self.date_from = datetime.strptime(TanggalAwal, "%Y-%m-%d").date()
                self.date_to = str(self.date_from + relativedelta.relativedelta(months=+1, day=1, days=-1))
            else:
                self.year = datetime.now().strftime("%Y")
                TanggalAwal = ("%s-%s-01" % (self.year,self.month))
                self.date_from = datetime.strptime(TanggalAwal, "%Y-%m-%d").date()
                self.date_to = str(self.date_from + relativedelta.relativedelta(months=+1, day=1, days=-1))
                return {
                    'warning':
                    {
                        'title': "Something Wrong",
                        'message': 'Tidak boleh memasukkan karakter huruf\natau lebih dari 4 karakter pada field ini.'
                    }
                }
                
        

    @api.multi
    def download_xlsx_report(self):
        template_report = 'ab_hr_leave.report_cuti_xlsx'
        return self.env.ref(template_report).report_action(self)
