# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools.float_utils import float_round
from odoo.addons.ab_role.models.models import _get_uid_from_group
import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta


class HREmployee(models.Model):
    _inherit = 'hr.employee'

    current_leave_state = fields.Selection(selection_add=[('done', 'Done'),('validate2', 'Reviewed HOD')])

    def _get_remaining_leaves(self):
        self._cr.execute("""
            SELECT
                sum(h.number_of_days) AS days,
                h.employee_id
            FROM
                (
                    SELECT holiday_status_id, number_of_days,
                        state, employee_id
                    FROM hr_leave_allocation
                    UNION ALL
                    SELECT holiday_status_id, (number_of_days * -1) as number_of_days,
                        state, employee_id
                    FROM hr_leave
                ) h
                join hr_leave_type s ON (s.id=h.holiday_status_id)
            WHERE
                h.state='done' AND
                (s.allocation_type='fixed' OR s.allocation_type='fixed_allocation') AND
                h.employee_id in %s
            GROUP BY h.employee_id""", (tuple(self.ids),))
        return dict((row['employee_id'], row['days']) for row in self._cr.dictfetchall())

    @api.multi
    def _compute_leaves_count(self):
        query = """ with master_jatah as (select sum(hlr.number_of_days) as jatah,he.id as karyawan
                from hr_leave_report hlr
                left join hr_employee he on he.id = hlr.employee_id
                where hlr.number_of_days > 0 and he.id = %s
                group by karyawan),

                master_cuti as(
                SELECT he.id as karyawan, sum(hl.number_of_days) as cuti
                FROM hr_leave hl
                LEFT JOIN hr_employee he on he.id = hl.employee_id
                LEFT JOIN hr_job hj on hj.id = he.job_id
                WHERE hl.state = 'done'
                group by karyawan
                )

                SELECT mj.karyawan,mj.jatah,coalesce(mc.cuti,0),(mj.jatah-coalesce(mc.cuti,0)) as hasil_akhir
                FROM master_jatah as mj
                left join master_cuti mc on mc.karyawan = mj.karyawan
                 """ % (self.id)
        self._cr.execute(query)
        data = self.env.cr.dictfetchall()
        for x in data:
            inc = [x["hasil_akhir"]]

            for employee in self:
                employee.leaves_count = inc[0]


class HRLeaveType(models.Model):
    _inherit = 'hr.leave.type'

    @api.multi
    def get_days(self, employee_id):
        # need to use `dict` constructor to create a dict per id
        result = dict((id, dict(max_leaves=0, leaves_taken=0, remaining_leaves=0, virtual_remaining_leaves=0)) for id in self.ids)

        requests = self.env['hr.leave'].search([
            ('employee_id', '=', employee_id),
            ('state', 'in', ['confirm', 'validate1', 'validate2', 'validate', 'done']),
            ('holiday_status_id', 'in', self.ids)
        ])

        allocations = self.env['hr.leave.allocation'].search([
            ('employee_id', '=', employee_id),
            ('state', 'in', ['confirm', 'validate1', 'validate2', 'validate']),
            ('holiday_status_id', 'in', self.ids)
        ])

        for request in requests:
            status_dict = result[request.holiday_status_id.id]
            status_dict['virtual_remaining_leaves'] -= (request.number_of_hours_display
                                                        if request.leave_type_request_unit == 'hour'
                                                        else request.number_of_days)
            if request.state == 'validate':
                status_dict['leaves_taken'] += (request.number_of_hours_display
                                                if request.leave_type_request_unit == 'hour'
                                                else request.number_of_days)
                status_dict['remaining_leaves'] -= (request.number_of_hours_display
                                                    if request.leave_type_request_unit == 'hour'
                                                    else request.number_of_days)

        for allocation in allocations.sudo():
            status_dict = result[allocation.holiday_status_id.id]
            if allocation.state == 'validate':
                # note: add only validated allocation even for the virtual
                # count; otherwise pending then refused allocation allow
                # the employee to create more leaves than possible
                status_dict['virtual_remaining_leaves'] += (allocation.number_of_hours_display
                                                            if allocation.type_request_unit == 'hour'
                                                            else allocation.number_of_days)
                status_dict['max_leaves'] += (allocation.number_of_hours_display
                                              if allocation.type_request_unit == 'hour'
                                              else allocation.number_of_days)
                status_dict['remaining_leaves'] += (allocation.number_of_hours_display
                                                    if allocation.type_request_unit == 'hour'
                                                    else allocation.number_of_days)

        return result

    @api.multi
    def _compute_group_days_leave(self):
        grouped_res = self.env['hr.leave'].read_group(
            [('holiday_status_id', 'in', self.ids), ('holiday_type', '=', 'employee'), ('state', '=', 'done'),
             ('date_from', '>=', fields.Datetime.to_string(datetime.datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)))],
            ['holiday_status_id'],
            ['holiday_status_id'],
        )
        grouped_dict = dict((data['holiday_status_id'][0], data['holiday_status_id_count']) for data in grouped_res)
        for allocation in self:
            allocation.group_days_leave = grouped_dict.get(allocation.id, 0)


class HRLeave(models.Model):
    _inherit = "hr.leave"
    _description = "Cuti Falah"

    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('confirm', 'Submitted'),
        ('validate1', 'Reviewed LOD'),
        ('validate2', 'Reviewed HOD'),
        ('validate', 'Reviewed HR'),
        ('done', 'Approved'),
        ('refuse', 'Refused'),
    ], string='Status', readonly=True, track_visibility='onchange', copy=False, default='draft')
    keterangan = fields.Text(string='Description', readonly=True, states={'draft': [('readonly', '=', False)]})
    sudah_cuti = fields.Float(string="Sudah Ambil(Cuti Tahunan)", store=True)
    sisa_cuti_tahunan = fields.Float(string="Sisa Cuti Tahunan", store=True)
    lod_reviewed = fields.Boolean(string='LOD Reviewed')
    is_approval = fields.Boolean(string='Is Approval', compute="_is_approval")
    # sisa_cuti = fields.Float(string="Sisa Cuti", related="employee_id.leaves_count")

    @api.depends('employee_id')
    def _is_approval(self):
        if self.state == 'confirm' and self.employee_id.parent_id.user_id.id == self.env.user.id:
            self.is_approval = True
        elif self.state == 'validate1' and self.employee_id.parent_id.parent_id.user_id.id == self.env.user.id:
            self.is_approval = True
        elif self.state == 'validate2' and self.env.user.id in self.env['res.groups'].search([('id', '=', self.env.ref("ab_role.group_hod_human_resource").id)]).users.ids:
            self.is_approval = True

    # Tombol Submit
    def action_confirm(self):
        res = super(HRLeave, self).action_confirm()
        # self.message_post(
        #     body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
        #     subject=self.name,
        #     partner_ids=[(4, self.employee_id.parent_id.user_id.partner_id.id)],
        #     message_type="notification",
        #     subtype="mail.mt_comment",
        # )
        # user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_lod_lod").id)
        user_ids = []
        lod_group = _get_uid_from_group(self, self.env.ref("ab_role.group_lod_lod").id)
        for i in lod_group:
            user_ids.append(i)
        hr_group = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        for j in hr_group:
            user_ids.append(j)

        single = []
        for i in user_ids:
            if i not in single:
                single.append(i)

        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in single]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'user', self.employee_id.parent_id.user_id)
        self.write({"state": "confirm"})
        return res

    # Tombol Review
    def action_approve(self):
        if self.state == 'confirm':
            if self.department_id.parent_id.name not in ['Project']:
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                    subject=self.name,
                    partner_ids=[(4, self.employee_id.parent_id.parent_id.user_id.partner_id.id)],
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
                self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'group', 'ab_role.group_hod_human_resource')
                self.write({'state': 'validate2'})
            elif self.department_id.parent_id.name == 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'group', 'ab_role.group_hod_hod')
                return self.write({'state': 'validate1'})

        elif self.state == 'validate1':
            self.write({'state': 'validate2'})
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or review. \nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
        elif self.state == 'validate2':
            self.write({'state': 'validate'})
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                subject=self.name,
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )

    def action_approve2(self):
        if self.state == 'confirm':
            if self.department_id.parent_id.name == 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'group', 'ab_role.group_hod_human_resource')
                return self.write({'state': 'validate2'})

            elif self.department_id.parent_id.name != 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'group', 'ab_role.group_hod_human_resource')
                return self.write({'state': 'validate2'})
                
        elif self.state == 'validate1':
            if self.department_id.parent_id.name == 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'group', 'ab_role.group_hod_human_resource')
                return self.write({'state': 'validate2'})
    
    def action_approve3(self):
        if self.state == 'validate2':
            if self.department_id.parent_id.name == 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                return self.write({'state': 'validate'})
            elif self.department_id.parent_id.name != 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                return self.write({'state': 'validate'})
                
        if self.state == 'confirm':
            # if self.department_id.parent_id.name == 'Project':
                user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
                partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
                    subject=self.name,
                    partner_ids=partner_ids,
                    message_type="notification",
                    subtype="mail.mt_comment",
                    )
                return self.write({'state': 'validate'})

    # Tombol Approve
    def action_done(self):
        absen = self.env['hr.attendance']
        att = absen.create({
            'employee_id': self.employee_id.id,
            'check_in': self.request_date_from,
            'check_out': self.request_date_to,
        })
        att.update({'attendance_category': 'cuti' if self.holiday_status_id.name not in ['Unpaid'] else 'pot_gaji'})

        return self.write({'state': 'done'})

    @ api.multi
    def cetak_minta_cuti(self):
        return self.env.ref("ab_hr_leave.cetak_permintaan_cuti").report_action(self)

    @ api.onchange("number_of_days_display","employee_id","holiday_status_id","request_date_from","request_date_to")
    def get_cuti_number(self):

        cuti_karyawan = self.env["hr.leave"].search(
            [("employee_id", "=", self.employee_id.id)]
        )
        leave_tahun = self.env["hr.leave.allocation"].search(
            [("holiday_status_id", "=", self.holiday_status_id.id),("employee_id", "=", self.employee_id.id)], limit=1
        )
        hak_cuti_tahun = leave_tahun.number_of_days_display

        if self.number_of_days_display != 0.0:
            if self.holiday_status_id.name == "Cuti Tahunan":
                self.sudah_cuti = 0
                for info in cuti_karyawan:
                    if (
                        info.employee_id == self.employee_id
                        and info.state == "done"
                    ):
                        self.sudah_cuti += info.number_of_days_display

                self.sisa_cuti_tahunan = hak_cuti_tahun - self.sudah_cuti


# class HRLeaveDataReport(models.AbstractModel):
#     _name = "report.ab_hr_leave.report_permintaan_cuti"
#     _description = "Kirim data untuk report permintaan cuti"

#     @api.model
#     def _get_report_values(self, docids, data=None):
#         docs = self.env["hr.leave"].browse(docids[0])
#         sudah_cuti = 0
#         cuti_karyawan = self.env["hr.leave"].search(
#             [("employee_id", "=", docs.employee_id.id)], limit=12
#         )

#         leave_tahun = self.env["hr.leave.allocation"].search(
#             [("name", "=", "Cuti Tahunan")], limit=1
#         )

#         for info in cuti_karyawan:
#             sudah_cuti += info.number_of_days_display

#         hak_cuti_tahun = 12
#         sisa_cuti = hak_cuti_tahun - sudah_cuti
#         return {
#             "data": data,
#             "hak_cuti": hak_cuti_tahun,
#             "sudah_cuti": sudah_cuti,
#             "sisa_cuti": sisa_cuti,
#             "docs": docs,
#         }


class HrHolidaySummaryReport(models.AbstractModel):
    _inherit = 'report.hr_holidays.report_holidayssummary'

    def _get_leaves_summary(self, start_date, empid, holiday_type):
        res = []
        count = 0
        start_date = fields.Date.from_string(start_date)
        end_date = start_date + relativedelta(days=59)
        for index in range(0, 60):
            current = start_date + timedelta(index)
            res.append({'day': current.day, 'color': ''})
            if self._date_is_day_off(current):
                res[index]['color'] = '#ababab'
        # count and get leave summary details.
        holiday_type = ['validate', 'done'] if holiday_type == 'both' else ['confirm'] if holiday_type == 'Confirmed' else ['done']
        holidays = self.env['hr.leave'].search([
            ('employee_id', '=', empid), ('state', 'in', holiday_type),
            ('date_from', '<=', str(end_date)),
            ('date_to', '>=', str(start_date))
        ])
        for holiday in holidays:
            # Convert date to user timezone, otherwise the report will not be consistent with the
            # value displayed in the interface.
            date_from = fields.Datetime.from_string(holiday.date_from)
            date_from = fields.Datetime.context_timestamp(holiday, date_from).date()
            date_to = fields.Datetime.from_string(holiday.date_to)
            date_to = fields.Datetime.context_timestamp(holiday, date_to).date()
            for index in range(0, ((date_to - date_from).days + 1)):
                if date_from >= start_date and date_from <= end_date:
                    res[(date_from-start_date).days]['color'] = holiday.holiday_status_id.color_name
                date_from += timedelta(1)
            count += holiday.number_of_days
        self.sum = count
        return res
