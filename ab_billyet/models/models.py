# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError


class BillyetCheck(models.Model):
    _name = "billyet.check"
    _inherit = "mail.thread"
    _description = "Billyet Check"

    state = fields.Selection(
        string="Status",
        selection=[("draft", "Draft"), ("done", "Done")],
        default="draft",track_visibility="onchange"
    )
    user_id = fields.Many2one(
        comodel_name="res.users",
        string="Created By",
        default=lambda self: self.env.uid,
        readonly=True,track_visibility="onchange"
    )
    type = fields.Selection(
        string="Type",
        selection=[("billyet", "Billyet Giro"), ("check", "Billyet Check")],
        required=True,track_visibility="onchange"
    )
    name = fields.Char(string="Check No",track_visibility="onchange")
    tanggal = fields.Date(string="Tanggal Giro/Check",track_visibility="onchange")
    date = fields.Date(string="Tanggal",track_visibility="onchange")
    remarks = fields.Char(string="Remarks",track_visibility="onchange")
    billyet_line = fields.One2many(comodel_name="billyet.check.line", inverse_name="billyet_id", string="Lines",track_visibility="onchange")
    catatan_revisi = fields.Text(string='Catatan Revisi',readonly=True,track_visibility="onchange")

    @api.multi
    def cetak_billyet_check(self):
        return self.env.ref("ab_billyet.cetak_billyet_check").report_action(self)

    def done(self):
        self.state = "done"

    def draft(self):
        self.state = "draft"

    @api.multi
    def unlink(self):
        for i in self:
            if i.state != "draft":
                raise UserError(
                    ("Form Billyet tidak bisa dihapus pada state %s !") % (i.state)
                )
        return super(BillyetCheck, self).unlink()

    def terbilang_(self, n):
        n = abs(int(n))

        satuan = [
            "",
            "Satu",
            "Dua",
            "Tiga",
            "Empat",
            "Lima",
            "Enam",
            "Tujuh",
            "Delapan",
            "Sembilan",
            "Sepuluh",
            "Sebelas",
        ]

        if n >= 0 and n <= 11:
            hasil = [satuan[n]]
        elif n >= 12 and n <= 19:
            hasil = self.terbilang_(n % 10) + ["Belas"]
        elif n >= 20 and n <= 99:
            hasil = self.terbilang_(
                n / 10) + ["Puluh"] + self.terbilang_(n % 10)
        elif n >= 100 and n <= 199:
            hasil = ["Seratus"] + self.terbilang_(n - 100)
        elif n >= 200 and n <= 999:
            hasil = self.terbilang_(n / 100) + \
                ["Ratus"] + self.terbilang_(n % 100)
        elif n >= 1000 and n <= 1999:
            hasil = ["seribu"] + self.terbilang_(n - 1000)
        elif n >= 2000 and n <= 999999:
            hasil = self.terbilang_(n / 1000) + \
                ["Ribu"] + self.terbilang_(n % 1000)
        elif n >= 1000000 and n <= 999999999:
            hasil = (
                self.terbilang_(n / 1000000) +
                ["Juta"] + self.terbilang_(n % 1000000)
            )
        elif n >= 1000000000 and n <= 999999999999:
            hasil = (
                self.terbilang_(n / 1000000000)
                + ["Milyar"]
                + self.terbilang_(n % 1000000000)
            )
        else:
            hasil = (
                self.terbilang_(n / 1000000000)
                + ["Triliun"]
                + self.terbilang_(n % 100000000000)
            )
        return hasil

    def terbilang(self, n):

        if n == 0:
            return "nol"
        t = self.terbilang_(n)
        while "" in t:
            t.remove("")
        return " ".join(t)


class BillyetCheckLine(models.Model):
    _name = "billyet.check.line"
    _description = "Billyet Lines"

    name = fields.Char(string="Deskripsi Pembayaran")
    partner_id = fields.Many2one(comodel_name="res.partner", string="Dibayarkan Kepada")
    amount = fields.Float(string="Jumlah")
    billyet_id = fields.Many2one(comodel_name="billyet.check", string="Parent")

class Wizard(models.TransientModel):
    _name = 'catatan.wizard.billyet'
    _description = 'Wizard Revisi Billyet'

    text_revisi = fields.Text(string='Catatan Revisi')

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            billyet = self.env['billyet.check'].browse(active_id)
            billyet.catatan_revisi = self.text_revisi
            billyet.draft()