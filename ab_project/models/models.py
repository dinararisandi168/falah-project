# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta
from lxml import etree
from odoo.addons.ab_role.models.models import _get_uid_from_group
from dateutil import relativedelta


class AttachmentLink(models.Model):
    _inherit = "ir.attachment"
    _description = "All Atachment"

    project_id = fields.Many2one("project.project")
    is_project = fields.Boolean()


class ProjectProject(models.Model):
    _inherit = "project.project"
    _description = "Project Charter dll yang berhubungan dengan project"

    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("review", "PM Reviewed"),
            ("verify", "HR Reviewed"),
            ("approve", "FAT Reviewed"),
            ("plan", "CEO Approved / Draft Soldes"),
            ("submit2", "Submitted"),
            ("review2", "PM Reviewed"),
            ("verify2", "HR Reviewed"),
            ("approve2", "FAT Reviewed"),
            ("ps", "CEO Approved / Planning Schedule"),
            ("excute", "Submitted"),
            ("pm_review", "Reviewed PM"),
            ("close", "Approved CEO / Starting Project"),
            ("pc", "Project Closed"),
        ],
        default="draft",
        track_visibility="onchange",
    )
    mom_line = fields.One2many('add.mom', 'project_id', string='MOM',store=True)
    project_on = fields.Selection(
        [
            ("initiate", "Initiating"),
            ("plan", "Planning"),
            ("ps", "Executing"),
            ("close", "Closing"),
        ], string="Project On", readonly=True, default="initiate", invisible="1", store=True,)
    asistant_ids = fields.Many2many('res.users', string='Assistance')
    user_id = fields.Many2one("res.users", string="Project Leader", default=lambda self:self.env.user, track_visibility="onchange")
    catatan_revisi = fields.Text(string="Revisions", readonly="1", track_visibility="onchange")
    project_no = fields.Char(string="Project No", default="FIT-YY-MM/XXX", track_visibility="onchange")
    main_con_id = fields.Many2one("res.partner", string="Main Con/Project Partner", domain=[("customer", "=", True)], track_visibility="onchange")
    date_prepared = fields.Date(string="Date Prepared", default=fields.Date.today(), track_visibility="onchange")
    end_user_id = fields.Many2one("res.partner", string="End User", track_visibility="onchange", domain=[("customer", "=", True)])
    bin_item_id = fields.Many2one("res.partner", string="Bin Item", track_visibility="onchange", domain=[("customer", "=", True)])
    # categ_ids = fields.Many2many('calendar.event.type', 'meeting_category_rel', 'event_id', 'type_id', 'Tags',track_visibility="onchange")
    tag_ids = fields.Many2many("project.tags", string="Tags", oldname="categ_ids")
    customer_id = fields.Many2one("res.partner", string="Customer Name", domain=[("customer", "=", True)])
    customer_org_id = fields.Many2one("res.partner", string="Customer Organization", domain=[("customer", "=", True)], track_visibility="onchange")
    start_date = fields.Date(string="Estimasi Project Period", track_visibility="onchange")
    end_date = fields.Date(string="To", track_visibility="onchange")
    project_duration = fields.Char(string="Project Duration", track_visibility="onchange", store=True)
    project_purpose = fields.Text(string="Project Purpose / Justification", track_visibility="onchange")
    project_desc = fields.Text(string="Project Description", track_visibility="onchange")
    hl_requirement = fields.Text(string="High Level Requirements", track_visibility="onchange")
    hl_risk = fields.Text(string="High Level Risk", track_visibility="onchange")
    hl_solution = fields.Text(string="High Level Solution Concept", track_visibility="onchange")
    assumption_work = fields.Text(string="Assumption Scope of Work", track_visibility="onchange")
    currency_id = fields.Many2one("res.currency", string="Currency", required=True, default=lambda self: self.env.user.company_id.currency_id, track_visibility="onchange")
    # budget_assumption = fields.Float(string="Budget Assumption",currency_field="currency_id", track_visibility="onchange")
    # target_cost = fields.Float(string="Target Project Cost",currency_field="currency_id", track_visibility="onchange")
    budget_assumption = fields.Monetary(string="Budget Assumption", currency_field="currency_id")
    target_cost = fields.Monetary(string="Target Project Cost", currency_field="currency_id")
    stakeholder_line = fields.One2many("stakeholder.list", "project_id", string="Stakeholder List", track_visibility="onchange")
    member_line = fields.One2many("member.list", "project_id", string="Project Member List", track_visibility="onchange")
    partner_product_line = fields.One2many("product.partner", "project_id", string="Product Partner", track_visibility="onchange")
    attachment_line = fields.One2many("ir.attachment", "project_id", string="Attachment")
    attachment_ids = fields.Many2many(comodel_name="ir.attachment", string="Attachment List")
    analytic_account_id = fields.Many2one("account.analytic.account", string="Analytic Account", copy=False, ondelete="set null",
                                          help="Link this project to an analytic account if you need financial management on projects. "
                                          "It enables you to connect projects with budgets, planning, cost and revenue analysis, timesheets on projects, etc.",
                                          )
    is_design = fields.Boolean(string="")
    is_hrd = fields.Boolean(compute='_compute_is_hrd', default=lambda self: self.user_has_groups("ab_role.group_hod_human_resource"))
    
    
    @api.model
    def default_get(self, fields):
        rec = super(ProjectProject, self).default_get(fields)
        rec.update(
            {
                "user_id": self.env.user.id,
            }
        )
        return rec

    def _compute_is_hrd(self):
        for o in self:
            if o.user_has_groups("ab_role.group_hod_human_resource"):
               o.is_hrd = True
            else:
                o.is_hrd = False

    @api.onchange("mom_line")
    def check_duplicate_mom(self):
        penampungan = []
        if self.mom_line:
            for tampung in self.mom_line:
                if tampung.calendar_id not in penampungan:
                    penampungan.append(tampung.calendar_id)
                else:
                    raise UserError("Action Denied! Duplicated MOM Detected!")

    
    # @api.multi
    # def send_notif_member(self):
    #     user_ids = []
    #     for i in self:
    #         for member in i.member_line:
    #             if member.partner_id.user_ids:
    #                 user_ids.append(member.partner_id.user_ids.ids)
    #     partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
    #     self.message_post(
    #         body="You are added in this project!",
    #         subject=self.name,
    #         partner_ids=partner_ids,
    #         message_type="notification",
    #         subtype="mail.mt_comment",
    #     )

    @api.multi
    def action_submit(self):
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        subject = self.name
        self.message_post(
            # body = self.env['send.email.notification'].send
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to submit.\nThanks",
            # # subject=self.name,
            partner_ids=partner_ids,
            # message_type="notification",
            # subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'project.project', subject, 'group', 'project.group_project_manager')
        self.write({"state": "submit"})

    @api.multi
    def action_review(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "review"})

    @api.multi
    def action_verify(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Review.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "verify"})

    @api.multi
    def action_submit_ceo(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Approve.\nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'project.project', 'approve', 'group', 'ab_role.group_direktur')
        self.write({"state": "approve"})

    @api.multi
    def action_plannn(self):
        user_ids = []
        for i in self:
            for member in i.member_line:
                if member.partner_id.user_ids:
                    self.env['send.email.notification'].send_project(self.project_no, self.user_id.name, 'user', member.partner_id.user_ids)
                    user_ids.append(member.partner_id.user_ids.ids)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="You are added in this project!",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "plan"})
        self.project_on = "plan"

    @api.multi
    def action_set_to_draft(self):
        self.write({"state": "draft"})
        self.project_on = "initiate"

    @api.onchange("start_date", "end_date")
    def onchange_time(self):
        # start = self.start_date
        # stop = self.end_date
        # if self.start_date and self.end_date:
        #     hasil = stop-start
        #     self.project_duration = "%s days" %hasil.days
        start = self.start_date
        stop = self.end_date
        if self.start_date and self.end_date:
            # hasil = (stop.year - start.year) * 12 + (stop.month - start.month)
            stop += timedelta(days=1)
            if stop >= start:
                diff = relativedelta.relativedelta(stop, start)
                months = diff.years * 12 + diff.months
                days = diff.days
                self.project_duration = "%s Months %s Days" % (months, days)

    @api.model
    def create(self, vals):
        vals["project_no"] = self.env["ir.sequence"].next_by_code("project.no")
        if not vals.get("analytic_account_id"):
            analytic_account = self.env["account.analytic.account"].create(
                {
                    "name": vals.get("name", "Unknown Analytic Account"),
                    "company_id": vals.get("company_id", self.env.user.company_id.id),
                    "partner_id": vals.get("partner_id"),
                    "active": True,
                }
            )
            vals["analytic_account_id"] = analytic_account.id
        res = super(ProjectProject, self).create(vals)
        return res

    def _get_users(self, group_user):
        res = []
        temp_group = []

        for group in group_user:
            users = self.env['ir.model.data'].xmlid_to_object(group)
            if users:
                for i in users.users:
                    temp_group.append(i.partner_id.id)

        for i in temp_group:
            if not i in res:
                res.append(i)

        user_ids = _get_uid_from_group(self, self.env.ref(group_user[0]).id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]

        return res

    # def _get_group_from_state(self, state):
    #     group = []

    #     # Project Charter notification group target
    #     if state == 'submit':
    #         group.append('project.group_project_manager')
    #     elif state == 'review':
    #         group.append('ab_role.group_hod_human_resource')
    #     elif state == 'verify':
    #         group.append('ab_role.group_hod_fat')
    #     elif state == 'approve':
    #         group.append('ab_role.group_direktur')

    #     # Solution Design notification group target
    #     elif state == 'submit2':
    #         group.append('project.group_project_manager')
    #     elif state == 'review2':
    #         group.append('ab_role.group_hod_human_resource')
    #     elif state == 'verify2':
    #         group.append('ab_role.group_hod_fat')
    #     elif state == 'approve2':
    #         group.append('ab_role.group_direktur')

    #     # Project Schedule notification group target
    #     elif state == 'excute':
    #         group.append('ab_role.group_direktur')

    #     if group:
    #         return self._get_users(group)

    def _get_project_on_name(self, project_on):
        if project_on == 'initiate':
            return 'Project Charter'
        elif project_on == 'plan':
            return 'Solution Design'
        elif project_on == 'ps':
            return 'Plan Schedule'
        else:
            return 'Project'

    # def write(self, values):
    #     for project in self:
    #         if not project.analytic_account_id and not values.get(
    #             "analytic_account_id"
    #         ):
    #             project._create_analytic_account()

    #         if values.get('state'):
    #             state = _(dict(self.env['project.project'].fields_get(allfields=['state'])['state']['selection'])[values['state']])
    #             users = self._get_group_from_state(values['state'])
    #             project_on = self._get_project_on_name(self.project_on)

    #             if users:
    #                 self.message_post(
    #                     body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, \nThis is outstanding transaction that need your action to review. \nThanks",
    #                     subject=project_on + ' - ' + self.name + ' - [Need Review]',
    #                     partner_ids=users,
    #                     message_type="notification",
    #                     subtype="mail.mt_comment",
    #                 )
    #     result = super(ProjectProject, self).write(values)
    #     return result

    @api.model
    def _init_data_analytic_account(self):
        self.search([("analytic_account_id", "=", False)])._create_analytic_account()

    def _create_analytic_account(self):
        for project in self:
            analytic_account = self.env["account.analytic.account"].create(
                {
                    "name": project.name,
                    "company_id": project.company_id.id,
                    "partner_id": project.partner_id.id,
                    "active": True,
                }
            )
            project.write({"analytic_account_id": analytic_account.id})

    @api.multi
    def unlink(self):
        """ Delete the empty related analytic account """
        analytic_accounts_to_delete = self.env["account.analytic.account"]
        for project in self:
            # hanya bisa di hapus di state draft
            if project.state != "draft":
                raise UserError(
                    ("Project tidak bisa dihapus pada state %s !") % (project.state)
                )
            if project.analytic_account_id and not project.analytic_account_id.line_ids:
                analytic_accounts_to_delete |= project.analytic_account_id
        result = super(ProjectProject, self).unlink()
        analytic_accounts_to_delete.unlink()
        return result

    # @api.multi
    # def unlink(self):
    #     for o in self:
    #         if o.state != 'draft':
    #             raise UserError(("STO tidak bisa dihapus pada state %s !") % (o.state))
    #     return super(ProjectProject, self).unlink()

    def compute_wight_all_task(self):
        lcon = True
#        def generate_weight(tasks):
#            for task in tasks:
#                list_prorate = [z.id for z in tasks if z.distribution == "prorate"]
#                total_fix = sum([z.weight for z in tasks if z.distribution == "fix"])
#                prorate = (
#                    (100 - total_fix) / len(list_prorate)
#                    if len(list_prorate) != 0
#                    else 100
#                )
#                if task.distribution == "prorate":
#                    task.weight = prorate
#                if task.child_ids:
#                    generate_weight(task.child_ids)

#        list_task = self.env["project.task"]
#        for task in self.task_ids:
#            if not task.parent_id:
#                list_task += task
#        generate_weight(list_task)

    @api.model
    def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
        result = super(ProjectProject, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(result["arch"])
        if self.env.context.get("buka_menu_pc", []) and (view_type == "tree" or view_type == "form"):
            if self.user_has_groups("ab_role.group_project_leader") or self.user_has_groups("project.group_project_manager"):
                for node_form in doc.xpath("//form"):
                    node_form.set("edit", "true")
                    node_form.set("create", "true")
                for tree in doc.xpath("//tree"):
                    tree.set("edit", "true")
                    tree.set("create", "true")
            else:
                for node_form in doc.xpath("//form"):
                    node_form.set("create", "false")
                for tree in doc.xpath("//tree"):
                    tree.set("create", "false")
            # if not self.user_has_groups("ab_role.group_project_leader") or not self.user_has_groups("project.group_project_manager"):
            #     for node_form in doc.xpath("//form"):
            #         node_form.set("create", "false")
            #     for tree in doc.xpath("//tree"):
            #         tree.set("create", "false")
                    
            if self.env.context.get("project_on", '') == 'ps' and view_type == "form":
                if self.user_has_groups("ab_role.group_project_leader") or self.user_has_groups("ab_role.group_project_admin") or self.user_has_groups("project.group_project_manager"):
                    for node_form in doc.xpath("//form"):
                        node_form.set("edit", "true")
                else:
                    for node_form in doc.xpath("//form"):
                        node_form.set("edit", "false")

        if self.env.context.get("is_soldes", []) and view_type == "form":
            if self.user_has_groups("ab_role.group_project_leader"):
                for node_form in doc.xpath("//form"):
                    node_form.set("edit", "true")
                    # node_form.set("create", "true")
              
        result["arch"] = etree.tostring(doc, encoding="unicode")
        return result


class StakeholderList(models.Model):
    _name = "stakeholder.list"
    _description = "List Stakeholder di Project"

    partner_id = fields.Many2one(
        "res.partner",
        string="Person Name",
        domain="[('customer', '=', True)]",
        store=True,
    )
    organization_id = fields.Many2one("res.partner", string="Organization", related="partner_id.parent_id")
    position = fields.Char(string="Position", related="partner_id.function")
    remark = fields.Text(string="Remarks")
    project_id = fields.Many2one("project.project", string="Link Project")


class RoleMemberList(models.Model):
    _name = "role.member.list"
    _inherit="mail.thread"

    name = fields.Char(string='Role Name', required=True)


class MemberList(models.Model):
    _name = "member.list"
    _description = "Member List di Project"

    partner_id = fields.Many2one("res.partner", string="Person Name", store=True)
    organization_id = fields.Many2one("res.partner", string="Organization", related="partner_id.parent_id")
    # role = fields.Char(string="Role")
    role_id = fields.Many2one('role.member.list', string='Role')
    remark = fields.Text(string="Remarks")
    project_id = fields.Many2one("project.project", string="Link Project")


class ProdukPartner(models.Model):
    _name = "product.partner"
    _description = "Product partner"

    partner_id = fields.Many2one("res.partner", string="Partner Name", domain=[("supplier", "=", True)], store=True)
    pic_id = fields.Many2one("res.partner", string="PIC", domain=[("supplier", "=", True)], store=True)
    produk = fields.Char(string="Product")
    remark = fields.Text(string="Remarks")
    project_id = fields.Many2one("project.project", string="Link Project")


class AccountAnalyticAccount(models.Model):
    _inherit = "account.analytic.account"
    _description = "Analytic Account"

    project_ids = fields.One2many("project.project", "analytic_account_id", string="Projects")

    @api.multi
    def unlink(self):
        projects = self.env["project.project"].search(
            [("analytic_account_id", "in", self.ids)]
        )
        has_tasks = self.env["project.task"].search_count(
            [("project_id", "in", projects.ids)]
        )
        if has_tasks:
            raise UserError(
                _(
                    "Please remove existing tasks in the project linked to the accounts you want to delete."
                )
            )
        return super(AccountAnalyticAccount, self).unlink()

    @api.constrains("company_id")
    def _check_company_id(self):
        for record in self:
            if record.project_ids:
                raise UserError(
                    _(
                        "You cannot change the company of an analytical account if it is related to a project."
                    )
                )


class Wizard(models.TransientModel):
    _name = "catatan.wizard"
    _description = "Wizard Revisi"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def isi_catatan(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            project = self.env["project.project"].browse(active_id)
            # closure = self.env["closure.summary"].browse(active_id)
            if project.project_on == "initiate":
                project.catatan_revisi = self.text_revisi
                project.write({"state": "draft"})
                project.project_on = "initiate"
            elif project.project_on == "plan":
                project.catatan_revisi = self.text_revisi
                project.write({"state": "plan"})
                project.project_on = "plan"
            elif project.project_on == "ps":
                project.catatan_revisi = self.text_revisi
                project.write({"state": "ps"})
                project.project_on = "ps"
            else:
                project.catatan_revisi = self.text_revisi

            # closure.write({"state": "draft"})
            # closure.catatan_revisi = self.text_revisi
