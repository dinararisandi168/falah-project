# -*- coding: utf-8 -*-
{
    'name': "ab_install_acsess",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "PT. ISMATA NUSANTARA ABADI",
    'website': "http://www.ismata.co.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','l10n_id_ismata','ab_role','aa_payment_request','ab_account','ab_account_budget','ab_asset','ab_attachment',
    'ab_bast_bast','ab_billyet','ab_crm','ab_disposisi','ab_fat_menu','ab_form_helpdesk','ab_ga_menu','ab_hr_attendance','ab_hr_cicilan',
    'ab_hr_employees','ab_hr_inventory','ab_hr_leave','ab_hr_overtime','ab_hr_payroll','ab_hr_working_time','ab_karyawan_baru','ab_minutes_meeting','ab_payroll_account','ab_payroll_attendance','ab_payroll_pinjaman','ab_payroll_pph21','ab_payroll_thr','ab_ppn_terhutang',
    'ab_prestasi_kerja','ab_project','ab_project_task','ab_purchase','ab_request_contract','ab_res_partner','ab_sale_order','ab_stock_mutasi','ab_surat_keluarmasuk','ab_surat_tugas','account','analytic','attachment_preview','auth_signup','backend_theme_v12','barcodes','bus','calendar','contacts','helpdesk_lite','muk_web_preview_msoffice','muk_web_preview',
    'om_account_accountant','om_account_asset','om_account_budget','project_native','project','purchase','purchase_requisition','purchase_stock','sale','uom','product','account_dynamic_reports','dynamic_xlsx','report_xlsx','hr_organizational_chart'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/menu_access.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}