from odoo import api, fields, models
from odoo.exceptions import UserError
from lxml import etree
from odoo.addons.ab_attachment.models.models import _send_message


class DisposisiDisposisi(models.Model):
    _name = "disposisi.disposisi"
    _inherit = "mail.thread"
    _description = "Disposisi"

    name = fields.Char(string="Perihal", track_visibility="onchange")
    partner_id = fields.Many2one(comodel_name="res.partner", string="Surat Dari", track_visibility="onchange")
    date = fields.Date(string="Diterima tanggal", track_visibility="onchange")
    no_agenda = fields.Char(string="No. Agenda", track_visibility="onchange")

    state = fields.Selection(
        string="state",
        selection=[
            ("draft", "Draft"),
            ("submit", "CEO Assign"),
            ("confirm", "HOD Follow Up"),
            ("done", "CEO Close"),
        ],
        default="draft", track_visibility="onchange"
    )
    category = fields.Selection(
        [
            ("penting", "Penting"),
            ("biasa", "Biasa"),
            ("rahasia", "Rahasia"),
        ],
        string="Sifat", track_visibility="onchange"
    )
    note = fields.Text(string="CEO Notes", track_visibility="onchange")
    department_id = fields.Many2one(comodel_name="hr.department", string="Divisi", track_visibility="onchange")
    employee_id = fields.Many2one(comodel_name="hr.employee", string="Nama", track_visibility="onchange")
    catatan = fields.Char(string="Catatan Perencanaan", track_visibility="onchange")
    catatan_akhir = fields.Char(string="Catatan Akhir Pelaksanaan", track_visibility="onchange")

    @api.multi
    def cetak_disposisi_disposisi(self):
        return self.env.ref("ab_disposisi.cetak_disposisi_disposisi").report_action(self)

    @api.multi
    def unlink(self):
        for i in self:
            if i.state != "draft":
                raise UserError(
                    ("Form Disposisi tidak bisa dihapus pada state %s !") % (i.state))
        return super(DisposisiDisposisi, self).unlink()

    def submit(self):
        for record in self:
            partner = record.employee_id.user_id.partner_id.id
            if partner:
                record.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                    subject="Disposisi - %s - [Need Verify or Approve]" % (record.name),
                    partner_ids=[(6, 0, [partner])],
                    message_type="notification",
                    subtype="mail.mt_comment",)
        if self.employee_id.department_id == 253 or self.employee_id.department_id == 257:
            self.env['send.email.notification'].send(self.id, 'hr.leave', 'submit', 'user', self.employee_id.user_id)
        self.write({"state": "submit"})

    def follow_up(self):
        self.state = "confirm"
        for record in self:
            partner = record.employee_id.user_id.partner_id.id
            if partner:
                record.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                    subject="Disposisi - %s - [Need Verify or Approve]" % (record.name),
                    partner_ids=[(6, 0, [partner])],
                    message_type="notification",
                    subtype="mail.mt_comment",
                )

    def confirm(self):
        self.state = "done"
        for record in self:
            partner = record.employee_id.user_id.partner_id.id
            if partner:
                record.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
                    subject="Disposisi - %s - [Done]" % (record.name),
                    partner_ids=[(6, 0, [partner])],
                    message_type="notification",
                    subtype="mail.mt_comment",
                )

    def draft(self):
        self.state = "draft"

    @api.multi
    def action_set_to_draft(self):
        return {
            'name': 'Set to Draft',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'disposisi.wizard',
            'context': {'default_disposisi_id': self.id},
            'target': 'new',
        }
