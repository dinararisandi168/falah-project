from odoo import _, api, fields, models
from odoo.exceptions import UserError

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    project_id = fields.Many2one('project.project', string='Project', track_visibility="onchange")
    is_project = fields.Boolean()

    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("waiting", "Waiting Another Operation"),
            ("confirmed", "Waiting"),
            ("assigned", "Ready"),
            ("done", "Done"),
            ("cancel", "Cancelled"),
            ("submit", "Submitted"),
            ("review", "PM Reviewed"),
            ("approve", "Approval CEO"),

        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
    )


    @api.multi
    def action_submit(self):
        self.env['send.email.notification'].send(self.id, 'stock.picking', 'submit', 'group', 'project.group_project_manager')
        self.write({"state": "submit"})
 
    @api.multi
    def action_review(self):
        self.env['send.email.notification'].send(self.id, 'stock.picking', 'submit', 'group', 'ab_role.group_direktur')
        self.write({"state": "review"})

    @api.multi
    def action_approve(self):
        self.write({"state": "approve"})    
    
class StockInventory(models.Model):
    _inherit = 'stock.inventory'

    barcode = fields.Char(string='Barcode')

    @api.onchange('barcode')
    def barcode_scanning(self):
        match = False
        product_obj = self.env['product.product']
        product_id = product_obj.search([('barcode', '=', self.barcode)])
        if self.barcode and not product_id:
            raise Warning('No product is available for this barcode')
        if self.barcode and self.move_ids_without_package:
            for line in self.move_ids_without_package:
                if line.product_id.barcode == self.barcode:
                    line.quantity_done += 1
                    match = True
        if self.barcode and not match:
            if product_id:
                raise Warning('This product is not available in the order.'
                              'You can add this product by clicking the "Add an item" and scan')

    def write(self, vals):
        res = super(StockInventory, self).write(vals)
        if vals.get('barcode') and self.move_ids_without_package:
            for line in self.move_ids_without_package:
                if line.product_id.barcode == vals['barcode']:
                    print(line.quantity_done)
                    line.quantity_done += 1
                    self.barcode = None
        return res

class StockInventoryLine(models.Model):
    _inherit = 'stock.inventory.line'

    barcode = fields.Char(string='Barcode')

    @api.onchange('barcode')
    def _onchange_barcode_scan(self):
        product_rec = self.env['product.product']
        if self.barcode:
            product = product_rec.search([('barcode', '=', self.barcode)])
            if self.barcode and not product:
                raise UserError('No product is available for this barcode.')
            else:    
                self.product_id = product.id
    
    @api.onchange('product_id')
    def _onchange_product_id(self):
        if self.product_id:
            self.barcode = self.product_id.barcode

        
