
from odoo import fields, models, api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning
from odoo.tools.translate import _
from datetime import datetime, timedelta
from odoo.http import content_disposition, request
from odoo.modules.module import get_resource_path
import xlsxwriter


class ReportListItemXlsx(models.AbstractModel):
    _name = 'report.ab_hr_inventory.product_list_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        heading_format = workbook.add_format({'align': 'left', 'valign': 'vleft', 'bold': True, 'size': 10})
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        sub_heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 9, 'color': 'white', 'bg_color': '#4472c4', 'border_color': 'white', 'border' : 1})
        format = workbook.add_format({'size': 13, 'align': 'center'})
        bold = workbook.add_format({'bold': True, 'align': 'center','size': 16,})
        red = workbook.add_format({'bold': True, 'align': 'center','color':'red','size': 18})
        no_format = workbook.add_format({'num_format': '#,###0.000'})
        cell_number_format = workbook.add_format({'align': 'right', 'bold': False, 'size': 12, 'num_format': '#,###0.000'})
        cell_currency_number_format = workbook.add_format({'align': 'right', 'bold': False, 'size': 12, 'num_format': '"Rp" #,###0.00'})
        cell_text_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'color' : 'blue', 'size': 12, 'top': 1, 'left': 1, 'right': 1})
        normal_num_bold = workbook.add_format({'bold': True, 'num_format': '#,###0.000'})
        normal_currency_num_bold = workbook.add_format({'bold': True, 'num_format': '"Rp" #,###0.00'})
        normal_date = workbook.add_format({'align': 'left', 'bold': False, 'num_format': 'dd/mm/yy hh:mm'})
        normal_num_bold_no_currency = workbook.add_format({'bold': True, 'num_format': '#,###0.00'})

        # add sheet
        worksheet = workbook.add_worksheet('List Item')

        # setting width of the column
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 20)
        worksheet.set_column('C:C', 60)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 20)
        worksheet.set_column('F:F', 25)
        worksheet.set_column('G:G', 35)
        worksheet.set_column('H:H', 10)

        # worksheet.freeze_panes(9, 1)
        # worksheet.freeze_panes(9, 2)
        # worksheet.freeze_panes(9, 3)
        # worksheet.freeze_panes(9, 4)
        # worksheet.freeze_panes(9, 5)
        # worksheet.freeze_panes(9, 6)
        # worksheet.freeze_panes(9, 7)
        # worksheet.freeze_panes(9, 8)
        
        row = 5
        no = 0
        NO = []

        self.env.cr.execute(
            """
                SELECT pt.default_code as item_code,pt.name as produk, sum(sq.quantity) as quantity, pt.list_price as unit_price, COALESCE(ip.name,'-') as item, COALESCE(it.name,'-') as part
                FROM product_product pp
                left join product_template pt on pt.id = pp.product_tmpl_id
                left join stock_quant sq on sq.product_id = pp.id
                left join item_type it on it.id = pp.item_id
                left join inventory_part ip on ip.id = pp.inventory_id
                group by item_code,produk,unit_price,item,part
            """,
        )
        #  where sq.quantity > 0
        # res = self.env.cr.fetchone()
        data = self.env.cr.dictfetchall()
        worksheet.merge_range(0, 1, 0, 7, 'PT Falah Inovasi Teknologi', bold)
        worksheet.merge_range(1, 1, 1, 7, 'Item List', red)
        worksheet.merge_range(2, 1, 2, 7, 'As of %s' % (fields.Date.today().strftime("%d %b %Y")), bold)
        worksheet.merge_range(4, 1, 5, 1, 'Item No', cell_text_format_top_left_right)
        worksheet.merge_range(4, 2, 5, 2, 'Item Description', cell_text_format_top_left_right)
        worksheet.merge_range(4, 3, 5, 3, 'Quantity', cell_text_format_top_left_right)
        worksheet.merge_range(4, 4, 5, 4, 'Unit Price', cell_text_format_top_left_right)
        worksheet.merge_range(4, 5, 5, 5, 'Item Type', cell_text_format_top_left_right)
        worksheet.merge_range(4, 6, 5, 6, 'Inventory Part', cell_text_format_top_left_right)

        for x in data:
            inc = [
                x["item_code"],
                x["produk"],
                x["quantity"],
                x["unit_price"],
                x["item"],
                x["part"],
            ]
            NO.append(no)
        
            no += 1
            row += 1
            col = 1

            for v in inc:
                worksheet.write(row, col, v, text_style)
                col += 1 
        # worksheet.write_column('A7', NO, text_style)

        