# -*- coding: utf-8 -*-
{
    "name": "ab_hr_inventory",
    "summary": """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. Ismata Nusantara Abadi",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "stock", "product", "project", "ab_role","account","purchase","mrp"],
    # always loaded
    "data": [
        "security/ir.model.access.csv",
        "views/stock_picking_views.xml",
        "report/action_reports.xml",
        "views/views.xml",
        # "views/templates.xml",
        "report/report_receipt.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}
