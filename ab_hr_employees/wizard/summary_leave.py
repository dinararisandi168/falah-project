import time

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, Warning
from datetime import datetime
from dateutil import relativedelta


class HolidaysSummaryDept(models.TransientModel):
    _inherit = 'hr.holidays.summary.dept'
    _description = 'HR Leaves Summary Report By Department'

    @api.multi
    def print_summary(self):
        self.ensure_one()
        [data] = self.read()
        if not data.get('depts'):
            raise UserError(_('You have to select at least one department.'))
        departments = self.env['hr.department'].browse(data['depts'])
        datas = {
            'ids': [],
            'model': 'hr.department',
            'form': data
        }
        return self.env.ref('ab_hr_employees.action_report_summary_leave').with_context(from_transient_model=True).report_action(departments, data=datas)

    @api.multi
    def download_xlsx(self):
        return self.env.ref('ab_hr_employees.report_summary_leave_xlsx').report_action(self)
