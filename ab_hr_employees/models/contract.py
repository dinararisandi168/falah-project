# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from datetime import date


class HrContract(models.Model):
    _inherit = "hr.contract"

    tunj_jabatan = fields.Monetary(string="Tunjangan Jabatan")
    # potongan_terlambat = fields.Monetary(string="Potongan Terlambat")

    @api.model
    def get_current_contract(self):
        self.env.cr.execute(""" UPDATE public.hr_contract
        SET state = 'pending'
        WHERE EXTRACT(MONTH FROM date_end) - 1 = %s """ % (date.today().month))
