# -*- coding: utf-8 -*-
{
    "name": "ab_hr_employees",
    "summary": """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. Ismata Nusantara Abadi",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr", "hr_holidays","web_settings_dashboard", "ab_role", "contacts", "hr_recruitment","product","project","hr_payroll"],
    # always loaded
    "data": [
        "security/security_contact.xml",
        "security/ir.model.access.csv",
        "views/views.xml",
        "views/hr_applicant.xml",
        "views/project_department.xml",
        "views/menu.xml",
        "views/templates.xml",
        'views/data_cuti.xml',
        'views/contacts.xml',
        # 'views/summary_leave.xml',

        # 'report/action_report.xml',
        # 'report/summary_leave_report.xml',

        "views/tunjangan_jabatan.xml",
        "data/cron.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}
