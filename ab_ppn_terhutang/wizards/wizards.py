from odoo import _, api, fields, models

class TerhutangWizard(models.TransientModel):
    _name = 'terhutang.wizard'
    _description = 'Set To Draft Terhutang'

    ppn_terhutang_id = fields.Many2one('ppn.terhutang', string='Terhutang')
    reason = fields.Text(string='Alasan')

    @api.multi
    def action_confirm(self):
        if self.ppn_terhutang_id:
            self.ppn_terhutang_id.message_post(body="Alasan : %s" % (self.reason))
            self.ppn_terhutang_id.set_to_draft()
    