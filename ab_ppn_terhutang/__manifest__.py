# -*- coding: utf-8 -*-
{
    "name": "PPN TERHUTANG FALAH",
    "summary": """
        ____""",
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. ISMATA NUSANTARA ABADI - 087881071515",
    "website": "www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "account", "hr"],
    # always loaded
    "data": [
        "data/vendor.xml",
        "security/ir.model.access.csv",
        "views/views.xml",
        "report/report_pajak.xml",
        'wizards/report_pajak_wizard.xml',
        'wizards/wizards_views.xml',
        "views/res_config_views.xml",
        "views/templates.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}
