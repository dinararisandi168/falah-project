from odoo import fields, models, api
from datetime import timedelta, datetime
from dateutil import relativedelta
import datetime
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell
from io import BytesIO as StringIO
import base64
from PIL import Image
import os 

class ReportPajakKeluaranXlsx(models.AbstractModel):
    _name = 'report.ab_ppn_terhutang.report_pajak_keluaran_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_bold_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'bold': True})
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 14})
        sub_header = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#4781fd')
        worksheet = workbook.add_worksheet('Rekap Pajak Keluaran')
        logo = StringIO(base64.b64decode(self.env.user.company_id.logo))
        worksheet.insert_image(
                "B2", "logo.png", {"image_data": logo, "x_scale": 0.3, "y_scale": 0.3}
            )
        worksheet.set_column('A:A', 20)
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 3, 20)
        worksheet.set_column(4, 4, 30)
        worksheet.set_column(5, 7, 15)
        worksheet.merge_range('A7:A8', 'NO', cell_text_format_top_left_right)
        worksheet.merge_range('D2:F3', 'REKAP PAJAK KELUARAN', heading_format)
        value = dict(obj._fields['month'].selection).get(obj.month)
        BULAN = ("%s %s" % (value.upper(), obj.year))
        worksheet.merge_range('D4:F4', BULAN, sub_header)

        no = 1
        NO = []

        row = 7

        query = """ SELECT ai.l10n_id_tax_number as no_fp, ai.date_invoice as tanggal, rp.vat as npwp, rp.name as nama, ai.amount_untaxed as dpp, ai.amount_tax as ppn, ai.amount_total as total
                    FROM account_invoice ai
                    LEFT JOIN res_partner rp on rp.id = ai.partner_id
                    WHERE ai.type = 'out_invoice' and ai.l10n_id_tax_number is not null and ai.date_invoice >= '%s' and ai.date_invoice <= '%s' and ai.state = 'paid' """ % (obj.date_from,obj.date_to)
        
        self.env.cr.execute(query)
        data = self.env.cr.dictfetchall()
        worksheet.merge_range('B7:B8', 'No. FP Keluaran', cell_text_format_top_left_right)
        worksheet.merge_range('C7:C8', 'Tanggal FP', cell_text_format_top_left_right)
        worksheet.merge_range('D7:D8', 'NPWP\nLawan Transaksi', cell_text_format_top_left_right)
        worksheet.merge_range('E7:E8', 'Nama Lawan Transaksi', cell_text_format_top_left_right)
        worksheet.merge_range('F7:F8', 'DPP', cell_text_format_top_left_right)
        worksheet.merge_range('G7:G8', 'PPN', cell_text_format_top_left_right)
        worksheet.merge_range('H7:H8', 'Amount', cell_text_format_top_left_right)

        total_dpp = []
        total_ppn = []
        total_amount = []

        for x in data:

                inc = [
                    x["no_fp"],
                    str(x["tanggal"]),
                    x["npwp"],
                    x["nama"],
                    "RP. " + '{:,}'.format(int(x["dpp"])),
                    "RP. " + '{:,}'.format(int(x["ppn"])),
                    "RP. " + '{:,}'.format(int(x["total"])),
                ]
                total = [
                    int(x["dpp"]),
                    int(x["ppn"]),
                    int(x["total"]),
                ]
                total_dpp.append(total[0])
                total_ppn.append(total[1])
                total_amount.append(total[2])
                NO.append(no)
                no += 1
                row += 1
                col = 1

                for v in inc:
                    worksheet.write(row, col, v, text_style)
                    col += 1 
      
        worksheet.merge_range(row + 1,0,row + 1,4, 'Total', text_bold_style)
        worksheet.write(row + 1,5, "RP. " + '{:,}'.format(sum(total_dpp)), text_bold_style)
        worksheet.write(row + 1,6, "RP. " + '{:,}'.format(sum(total_ppn)), text_bold_style)
        worksheet.write(row + 1,7, "RP. " + '{:,}'.format(sum(total_amount)), text_bold_style)
      
        worksheet.write_column('A9', NO, text_style)
