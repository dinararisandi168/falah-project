from odoo import _, api, fields, models


class ResCompany(models.Model):
    _inherit = "res.company"
    
    account_id = fields.Many2one('account.account', string='Akun Utang PPn')
    acc_tax_in_id = fields.Many2one('account.account', string='Akun PPN masukan')
    acc_tax_out_id = fields.Many2one('account.account', string='Akun PPN keluaran')


class PpnResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    account_id = fields.Many2one('account.account', string='Akun Utang PPn', related='company_id.account_id', readonly=False)
    acc_tax_in_id = fields.Many2one('account.account', string='Akun PPN masukan', related='company_id.acc_tax_in_id', readonly=False)
    acc_tax_out_id = fields.Many2one('account.account', string='Akun PPN keluaran', related='company_id.acc_tax_out_id', readonly=False)