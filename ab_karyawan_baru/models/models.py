# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.addons.ab_role.models.models import _get_uid_from_group
from odoo.addons.ab_attachment.models.models import _send_message


class KaryawanBaru(models.Model):
    _name = "karyawan.baru"
    _inherit = "mail.thread"

    _inherit = "mail.thread"
    _description = "Karyawan Baru"

    name = fields.Char(string="Reference", readonly=True, default="/", track_visibility="onchange")
    no_agenda = fields.Char(string="No Agenda", track_visibility="onchange")
    date_submission = fields.Date(string="Tanggal Pengajuan", track_visibility="onchange")
    due_date = fields.Date(string="Expected Join Date", track_visibility="onchange")
    category = fields.Selection(
        [
            ("urgent", "Urgent"),
            ("reguler", "Reguler"),
        ],
        string="Sifat", track_visibility="onchange", default="reguler"
    )
    project_id = fields.Many2one("project.project", string="Project Terkait", track_visibility="onchange")
    departments_id = fields.Many2one("hr.department", string="Divisi Yang Meminta", track_visibility="onchange",
                                     default=lambda self: self.env['hr.employee'].search([('user_id', '=', self.env.user.id)], limit=1).department_id.id)
    job_id = fields.Many2one("hr.job", string="Posisi Yang Diminta", track_visibility="onchange")
    number_employees = fields.Integer(string="Jumlah Karyawan Yang Diminta", track_visibility="onchange")
    responsible = fields.Text(string="Tanggung Jawab", track_visibility="onchange")
    qualification = fields.Text(string="Kualifikasi", track_visibility="onchange")
    user_id = fields.Many2one("res.users", string="Diajukan Oleh", default=lambda self: self.env.user, track_visibility="onchange")
    user_one = fields.Selection([("hrd", "HRD Department")], string="Diketahui Oleh", track_visibility="onchange")
    user_two = fields.Selection([("ceo", "CEO")], string="Disetujui Oleh", track_visibility="onchange")
    catatan_revisi = fields.Text(string="Revisions", readonly="1", track_visibility="onchange")
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submitted", "Submitted"),
            ("approved", "Verified"),
            ("done", "Done"),
            ("cancel", "Cancel"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft", track_visibility="onchange"
    )

    @api.multi
    def action_set_to_draft(self):
        self.write({"state": "draft"})

    @api.multi
    def action_cancel(self):
        self.write({"state": "cancel"})

    @api.multi
    def action_submitted(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_human_resource").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject='Permintaan Karyawan Baru' + ' - ' + self.name + ' - [Need Review]',
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "submitted"})

    @api.multi
    def action_confirm(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject='Permintaan Karyawan Baru' + ' - ' + self.name + ' - [Need Review]',
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "approved"})

    @api.multi
    def action_done(self):
        self.write({"state": "done"})

    @api.multi
    def unlink(self):
        for i in self:
            if i.state != "draft":
                raise UserError(
                    ("Form Karyawan Baru tidak bisa dihapus pada state %s !")
                    % (i.state)
                )
        return super(KaryawanBaru, self).unlink()

    # untuk membuat outo save dari Reference
    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("karyawan.baru")
        return super(KaryawanBaru, self).create(vals)


class WizardKaryawanBaru(models.TransientModel):
    _name = "wizard.karyawan.baru"
    _description = "Wizard Karyawan Baru"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def revisi_state_draft(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            project = self.env["karyawan.baru"].browse(active_id)
            if project.write({"state": "draft"}):
                project.catatan_revisi = self.text_revisi
            else:
                project.catatan_revisi = self.text_revisi
