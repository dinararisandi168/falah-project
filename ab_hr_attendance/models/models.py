# -*- coding: utf-8 -*-

from datetime import date, datetime, timedelta

import dateutil
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import models, fields, api
from lxml import etree


class HrAttendance(models.Model):
    _inherit = "hr.attendance"
    _order = "check_in asc, employee_id asc"

    potongan_telat = fields.Integer(
        string="Potongan Telat", compute="_potongan_telat", store=True, readonly=True
    )

    telat = fields.Float(
        string="Terlambat(menit)", compute="_telat", store=True, readonly=True
    )
    pulang_cepat = fields.Float(
        string="Pulang Cepat", compute="_pulang_cepat", store=True, readonly=True
    )
    potongan_cepat = fields.Integer(
        string="Potongan Cepat", compute="_potongan_cepat", store=True, readonly=True
    )
    day = fields.Char(string="Day", compute="_get_day", store=True)

    attendance_category = fields.Selection(
        [
            ("terlambat", "Terlambat"),
            ("sakit", "Sakit"),
            ("cuti", "Cuti"),
            ("tanpa_keterangan", "Tanpa Keterangan"),
            ("izin", "Izin Yang Disetujui/ Tugas Kerja"),
            ("tdk_absen", "Tidak Absen"),
            ("pot_gaji", "Potong Gaji"),
        ],
        string="Attendancy Category", compute='_check_keterangan', store=True
    )

    # @api.model
    # def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
    #     result = super(HrAttendance, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
    #     doc = etree.XML(result["arch"])
    #     if view_type == "tree" or view_type == "form":
    #         if self.user_has_groups("ab_hr_attendance.group_hr_attendance_head"):
    #             for tree in doc.xpath("//tree"):
    #                 tree.set("create", "false")
    #                 tree.set("edit", "false")
    #             for form in doc.xpath("//form"):
    #                 form.set("create", "false")
    #                 form.set("edit", "false")

    #     result["arch"] = etree.tostring(doc, encoding="unicode")
    #     return result

    @api.model
    def create(self, vals):
        res = super(HrAttendance, self).create(vals)
#        if self.user_has_groups("ab_hr_attendance.group_hr_attendance_head"):
#            raise UserError(('Kepala divisi tidak bisa membuat absensi.'))
        return res

    @api.multi
    def unlink(self):
        for o in self:
            if o.user_has_groups("ab_hr_attendance.group_hr_attendance_head"):
                raise UserError(('Kepala divisi tidak bisa menghapus absensi.'))
        return super(HrAttendance, self).unlink()

    @api.depends('telat')
    def _check_keterangan(self):
        for record in self:
            if record.telat >= 1:
                record.attendance_category = 'terlambat'
                
    @api.model
    def auto_potong_gaji(self):
        # CHECK OUT OTOMATIS
        today = datetime.today().date().strftime("%Y-%m-%d")
        a = today + " 00:00:00"
        b = today + " 23:59:00"
        c = today + " 14:00:00"

        # checkin_ids = self.search([('check_in', '>', a), ('check_in', '<', b), ('check_out', '=', False)])
        # checkin_ids.write({'check_out': c})
        emp_obj = self.env["hr.employee"]
        # POTONG GAJI
        day = datetime.today().strftime("%A")
        if day not in ("Saturday", "Sunday"):
            sql_employees = """ select id from hr_employee"""
            self.env.cr.execute(sql_employees)
            emp_ids = [emp_obj.browse(x["id"]) for x in self.env.cr.dictfetchall()]
            for x in emp_ids:
                att_ids = self.search(
                    [
                        ("check_in", ">", a),
                        ("check_in", "<", b),
                        ("employee_id", "=", x.id),
                    ]
                )
                if not att_ids:
                    self.create(
                        {
                            "employee_id": x.id,
                            "check_in": today + " 01:00:00",
                            "check_out": today + " 10:00:00",
                            # "check_out": today + " 10:00:00",
                            "keterangan": "tdk_absen",
                        }
                    )

    @api.one
    @api.depends("check_in")
    def _get_day(self):
        date = datetime.strptime(str(self.check_in), "%Y-%m-%d %H:%M:%S") + timedelta(
            hours=7
        )
        self.day = date.strftime("%A")

    @api.one
    @api.depends("check_out")
    def _pulang_cepat(self):
        day_list = ["Saturday", "Sunday"]
        for attendance in self:
            if attendance.check_out:
                check_out = datetime.strptime(
                    str(attendance.check_out), "%Y-%m-%d %H:%M:%S"
                ) + timedelta(hours=7)
                hari = check_out.strftime("%A")
                jam_pulang = 0
                if hari not in day_list:
                    tanggal = datetime.strptime(str(check_out)[0:10], "%Y-%m-%d")
                    hariini = self.env["resource.calendar.attendance"].search(
                        [("tanggal", "=", tanggal)], limit=1
                    )
                    if hariini:
                        jam_pulang = hariini.hour_to
                    jam_check_out = attendance.check_out.time()
                    jam_check_out = (jam_check_out.hour + jam_check_out.minute/60)+7
                    delta = jam_pulang - jam_check_out
                    val = delta * 60
                    if val >= 1:
                        attendance.pulang_cepat = val

    @api.depends("pulang_cepat")
    def _potongan_cepat(self):
        for attendance in self:
            if attendance.pulang_cepat:
                if attendance.pulang_cepat <= 44:
                    pass
                    # attendance.potongan_cepat = int(attendance.employee_id) / 22 * 0.6
                else:
                    pass
                    # attendance.potongan_cepat = int(attendance.employee_id) / 22

    @api.depends("check_in")
    def _telat(self):
        terlambat = 0
        for attendance in self:
            if attendance.check_in:
                jam_check_in = attendance.check_in.time()
                jam_check_in = (jam_check_in.hour + jam_check_in.minute/60)+7
                obj_workingtime = self.env['resource.calendar.attendance']
                waktu_kerja = obj_workingtime.search([('tanggal', '>=', attendance.check_in), ('tanggal', '<=', attendance.check_in), ('calendar_id.working_time', '!=', False)], limit=1)
                if waktu_kerja:
                    jam_masuk = waktu_kerja.hour_from
                    terlambat = jam_check_in - jam_masuk
            attendance.telat = terlambat * 60




    # @api.onchange('attendance_category')
    # def _terlambat(self):
    #     for sel in self:
    #         if sel.attendance_category != "terlambat":
    #             sel._telat() = 00.00

    @api.depends("telat")
    def _potongan_telat(self):
        for atn in self:
            value = 0
            value_telat = atn.telat
            if atn.telat > 15:
                while value_telat > 0:
                    value += 25000
                    value_telat -= 30
                    if value == 100000:
                        break
            atn.potongan_telat = value

    @api.constrains("check_in", "check_out", "employee_id")
    def _check_validity(self):
        """Verifies the validity of the attendance record compared to the others from the same employee.
        For the same employee we must have :
            * maximum 1 "open" attendance record (without check_out)
            * no overlapping time slices with previous employee records
        """
        for attendance in self:
            # we take the latest attendance before our check_in time and check it doesn't overlap with ours
            last_attendance_before_check_in = self.env["hr.attendance"].search(
                [
                    ("employee_id", "=", attendance.employee_id.id),
                    ("check_in", "<=", attendance.check_in),
                    ("id", "!=", attendance.id),
                ],
                order="check_in desc",
                limit=1,
            )
            if (
                last_attendance_before_check_in
                and last_attendance_before_check_in.check_out
                and last_attendance_before_check_in.check_out >= attendance.check_in
            ):
                raise exceptions.ValidationError(
                    _(
                        "Cannot create new attendance record for %(empl_name)s, the employee was already checked in on %(datetime)s"
                    )
                    % {
                        "empl_name": attendance.employee_id,
                        "datetime": fields.Datetime.to_string(
                            fields.Datetime.context_timestamp(
                                self, fields.Datetime.from_string(attendance.check_in)
                            )
                        ),
                    }
                )

            if not attendance.check_out:
                # if our attendance is "open" (no check_out), we verify there is no other "open" attendance
                # no_check_out_attendances = self.env['hr.attendance'].search([
                #     ('employee_id', '=', attendance.employee_id.id),
                #     ('check_out', '=', False),
                #     ('id', '!=', attendance.id),
                # ])
                # if no_check_out_attendances:
                pass
                # raise exceptions.ValidationError(_(
                #     "Cannot create new attendance record for %(empl_name)s, the employee hasn't checked out since %(datetime)s") % {
                #                                      'empl_name': attendance.employee_id.name_related,
                #                                      'datetime': fields.Datetime.to_string(
                #                                          fields.Datetime.context_timestamp(self,
                #                                                                            fields.Datetime.from_string(
                #                                                                                no_check_out_attendances.check_in))),
                #                                  })
            else:
                # we verify that the latest attendance with check_in time before our check_out time
                # is the same as the one before our check_in time computed before, otherwise it overlaps
                last_attendance_before_check_out = self.env["hr.attendance"].search(
                    [
                        ("employee_id", "=", attendance.employee_id.id),
                        ("check_in", "<=", attendance.check_out),
                        ("id", "!=", attendance.id),
                    ],
                    order="check_in desc",
                    limit=1,
                )
                if (
                    last_attendance_before_check_out
                    and last_attendance_before_check_in
                    != last_attendance_before_check_out
                ):
                    raise exceptions.ValidationError(
                        _(
                            "Cannot create new attendance record for %(empl_name)s, the employee was already checked in on %(datetime)s"
                        )
                        % {
                            "empl_name": attendance.employee_id,
                            "datetime": fields.Datetime.to_string(
                                fields.Datetime.context_timestamp(
                                    self,
                                    fields.Datetime.from_string(
                                        last_attendance_before_check_out.check_in
                                    ),
                                )
                            ),
                        }
                    )
