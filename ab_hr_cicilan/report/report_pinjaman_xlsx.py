from odoo import fields, models, api
from datetime import timedelta, datetime, date
from dateutil import relativedelta
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell


class VendorReportXlsx(models.AbstractModel):
    _name = 'report.ab_hr_cicilan.report_pinjaman_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 14})
        sub_header = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 11, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#80a7fa')
        worksheet = workbook.add_worksheet('Laporan Pinjaman Karyawan')
        worksheet.set_column('A:A', 20)
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 1, 30)
        worksheet.set_column(2, 4, 20)
        worksheet.set_column(5, 6, 10)
        worksheet.set_column(7, 8, 20)
        worksheet.merge_range(4, 0, 5, 0, 'NO', cell_text_format_top_left_right)
        worksheet.merge_range('D1:F2', 'LAPORAN PINJAMAN KARYAWAN', heading_format)
        worksheet.merge_range(4, 1, 5, 1, 'Name', cell_text_format_top_left_right)
        worksheet.merge_range(4, 2, 5, 2, 'Posisi/Jabatan', cell_text_format_top_left_right)
        worksheet.merge_range(4, 3, 5, 3, 'Gaji Pokok', cell_text_format_top_left_right)
        worksheet.merge_range(4, 4, 5, 4, 'Saldo Hutang Dari\nBulan Sebelumnya', cell_text_format_top_left_right)
        worksheet.merge_range(4, 5, 4, 6, 'Kasbon', cell_text_format_top_left_right)
        worksheet.write('F6', 'Amount', cell_text_format_top_left_right)
        worksheet.write('G6', 'Tanggal', cell_text_format_top_left_right)
        worksheet.merge_range(4, 7, 5, 7, 'Pembayaran', cell_text_format_top_left_right)
        worksheet.merge_range(4, 8, 5, 8, 'Saldo Hutang\nBulan Ini', cell_text_format_top_left_right)
        
        value = dict(obj._fields['month'].selection).get(obj.month)
        BULAN = ("%s %s" % (value.upper(), obj.year))
        worksheet.merge_range('D3:F3', BULAN, sub_header)


        TanggalAwal = ("%s-%s-01" % (obj.year,int(obj.month)-1))
        date_from_before = datetime.strptime(TanggalAwal, "%Y-%m-%d").date()
        date_to_before = str(date_from_before + relativedelta.relativedelta(months=+1, day=1, days=-1))

        no = 1
        NO = []

        row = 5

        query = """with master_data as(
                SELECT hc.id as id_md,he.name as karyawan, hj.name as posisi,contract.wage as gaji,hc.value as kasbon,hc.date as tanggal_kasbon
                from hr_contract contract 
                left join hr_employee he on he.id = contract.employee_id
                left join hr_cicilan hc on hc.employee_id = he.id
                left join hr_job hj on hj.id = he.job_id
                where contract.state = 'open' and hc.state = 'done' and hc.lunas = False
                ),
                pembayaran as (
                SELECT hrc.cicilan_value as terbayar, hrc.cicilan_id
                from hr_cicilan_line hrc
                left join hr_payslip hp on hp.id = hrc.payslip_id
                where hrc.payslip_id is not null and hrc.cicilan_id is not null and hp.date_from >= '%s' and hp.date_from <= '%s'
                order by hrc.id desc
                limit 1
                ),

                sisa as (
                SELECT hrc.cicilan_id, COALESCE(hrc.sisa_cicilan,0) AS sisa_bulan_kemarin
                from hr_cicilan_line hrc
                left join hr_payslip hp on hp.id = hrc.payslip_id
                where payslip_id is not null and cicilan_id is not null and hp.date_from >= '%s' and hp.date_from <= '%s'
                order by hrc.id desc
                limit 1
                ),

                sisa_sekarang as (
                SELECT hrc.cicilan_id as cicilan, COALESCE(hrc.sisa_cicilan,0) AS sisa_bulan_sekarang
                from hr_cicilan_line hrc
                left join hr_payslip hp on hp.id = hrc.payslip_id
                where payslip_id is not null and cicilan_id is not null and hp.date_from >= '%s' and hp.date_from <= '%s'
                order by hrc.id desc
                limit 1
                )

                select md.karyawan,md.posisi,md.gaji,ss.sisa_bulan_kemarin,md.kasbon,md.tanggal_kasbon,COALESCE(sum(pb.terbayar),0) as pembayaran,COALESCE(COALESCE(sk.sisa_bulan_sekarang,ss.sisa_bulan_kemarin),md.kasbon) as sisa_sekarang
                from master_data md
                left join pembayaran pb on pb.cicilan_id = md.id_md
                left join sisa ss on ss.cicilan_id = md.id_md
                left join sisa_sekarang sk on sk.cicilan = md.id_md
                group by md.karyawan,md.posisi,md.gaji,ss.sisa_bulan_kemarin,md.kasbon,md.tanggal_kasbon,sisa_sekarang



        """ % (obj.date_from,obj.date_to,date_from_before,date_to_before,obj.date_from,obj.date_to)
        
        self.env.cr.execute(query)
        data = self.env.cr.dictfetchall()

        for x in data:

                inc = [
                    x["karyawan"],
                    x["posisi"],
                    "RP. " + '{:,}'.format(int(x["gaji"])),
                    "RP. " + '{:,}'.format(int(x["sisa_bulan_kemarin"])) if x['sisa_bulan_kemarin'] else '-',
                    "RP. " + '{:,}'.format(int(x["kasbon"])),
                    str(x["tanggal_kasbon"]),
                    "RP. " + '{:,}'.format(int(x["pembayaran"])) if x['pembayaran'] else '-',
                    "RP. " + '{:,}'.format(int(x["sisa_sekarang"])) if x['sisa_sekarang'] else '-',
                ]
                NO.append(no)
                no += 1
                row += 1
                col = 1

                for v in inc:
                    worksheet.write(row, col, v, text_style)
                    col += 1 
      
       
        worksheet.write_column('A7', NO, text_style)
