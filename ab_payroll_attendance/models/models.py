# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    @api.multi
    def get_result_data(self):
        res = super(HrPayslip, self).get_result_data()
        for x in self:
            date_from = x.date_from
            date_to = x.date_to
            payslip = x
            
            # Mengambil Data Absen
            filters_attendance = [('check_in', '>=', date_from), ('check_in', '<=', date_to),
                                ('employee_id', '=', payslip.employee_id.id)]

            nilai_potongan_terlambat = 0
            absen = self.env['hr.attendance'].search(filters_attendance)
            hari = self.env['resource.calendar.attendance'].search([('tanggal', '<=', date_to), ('tanggal', '>=', date_from), ('status', '=', 'kerja')])
            hari_kerja = [x.tanggal for x in hari]
            total_tdk_absen = 0
            for x in absen:
                if x.potongan_telat:
                    if x.check_in.date() in hari_kerja:
                        nilai_potongan_terlambat += x.potongan_telat
                if x.attendance_category in ('tanpa_keterangan','tdk_absen'):
                    total_tdk_absen += 1

            list_hari_masuk = [x.check_in.date() for x in absen]

            for x in list_hari_masuk:
                if x not in hari_kerja:
                    total_tdk_absen += 1

            nilai_potongan_tidak_absen = total_tdk_absen * 100000
            potongan_absen = (nilai_potongan_terlambat + nilai_potongan_tidak_absen) * -1
            res.append({'name': 'Potongan Absen',
                        'code': 'ABSEN',
                        'amount': potongan_absen,
                        'contract_id': payslip.contract_id.id,
                        'payslip_id': payslip.id})

        return res
