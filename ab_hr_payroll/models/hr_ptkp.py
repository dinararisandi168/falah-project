from odoo import api, fields, models



class Ptkp(models.Model):
    _name = 'hr.ptkp'

    name = fields.Char('Status')
    tahun = fields.Integer('Tarif/Tahun')

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = "%s" % (record.name)
            result.append((record.id, name))
        return result