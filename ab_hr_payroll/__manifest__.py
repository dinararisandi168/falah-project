# -*- coding: utf-8 -*-
{
    "name": "CUSTOM HR PAYROLL",
    "category": "Custom Modules",
    "summary": """
       Custom Module HR untuk FALAH
       """,
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. Ismata Nusantara Abadi ",
    "website": "ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "hr_payroll", "ab_role", "hr", "hr_payroll_account", "ab_hr_employees", "aa_payment_request", 'ab_hr_overtime','account'],
    "data": [
        "security/groups.xml",
        "data/data.xml",
        "data/ptkp.xml",
        "report/report.xml",
        "report/print_payslip.xml",
        "views/company.xml",
        "views/views.xml",
        "views/hr_ptkp.xml",
        "wizard/recap_wizard_view.xml",
        "views/menu.xml",
        "security/ir.model.access.csv",
    ],
    # only loaded in demonstration
}
