
import datetime
from datetime import date
from io import BytesIO as StringIO
import xlwt
import base64
import itertools
import logging
from collections import defaultdict
from dateutil.relativedelta import relativedelta
import calendar
import re
from collections import OrderedDict
from odoo import models, fields, api, _
from PIL import Image
import os
from datetime import datetime
from dateutil import relativedelta

# Gaji Pokok	Overtime	JKK	                                                  JKM Tunjangan              PPH 21	     Penghasilan Bruto          Biaya Jabatan       JHT	                            JP                     Penghasilan Neto 1 bulan	  Penghasilan Neto 1 Tahun        PTKP	  PKP 1 Tahun	PKP [ROUND DOWN]	PPH Terhutang NPWP	              PPH Terhutang Non NPWP	    PPH Terhutang Real Setahun	          PPH 21
# Gaji Pokok	Overtime	BPJS Jaminan Kecelakaan Kerja 0.24%	     BPJS Jaminan Kematian 0.3%	      Tunjangan PPh21	       Total Bruto	        Biaya Jabatan	BPJS Jaminan Hari Tua 3.7% 	    BPJS Jaminan Pensiun 2% 		   Total Netto               Total Netto             PTKP	   	         PKP	                PPh 21 NPWP Terhutang	         PPh 21 Non NPWP Terhutang          PPh 21 Real Setahun            BELUM
						



_logger = logging.getLogger(__name__)

ID_MONTHES = {
    '01': 'Januari',
    '02': 'Februari',
    '03': 'Maret',
    '04': 'April',
    '05': 'Mei',
    '06': 'Juni',
    '07': 'Juli',
    '08': 'Agustus',
    '09': 'September',
    '10': 'Oktober',
    '11': 'Nopember',
    '12': 'Desember',
}


class RekapPph21(models.TransientModel):
    _name = 'rekap.pph21'
    _description = 'New Description'

    tipe = fields.Selection(string='Tipe', selection=[('all', 'All')], default="all")
    user_id = fields.Many2one(comodel_name='res.users', string='Created By')
    partner_id = fields.Many2one('res.partner', string='Partner')
    department_id = fields.Many2one(comodel_name='hr.department', string='Department')
    name = fields.Char(string='File name')
    data_file = fields.Binary(string='Data File')
    period = fields.Date(string='Tanggal', default=fields.Date.today())

    period_start = fields.Date(string='Period Start', default=datetime.now().strftime('%Y-%m-01'))
    period_end = fields.Date(string='Period End', default=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1)))

    @ api.multi
    def filename_generator(self):
        reformat_year = self.period.strftime('%Y')
        string = "REKAPITULASI PPh21 ("
        data = {
            'city':
                {'data': {
                    'string': 'Kota',
                    'isi': self.partner_id.city,
                    'query': 'and rp.id = %s' % (self.partner_id.id)
                }},
            'dep':
                {'data': {
                    'string': 'Departemen',
                    'isi': self.department_id.name,
                    'query': 'd.id = %s' % (self.department_id.id)
                }},
            'all':
                {'data': {
                    'string': '',
                    'isi': '',
                    'query': '1=1'
                }}
        }
        query = data[self.tipe]['data']['query']
        isi = data[self.tipe]['data']['isi']
        string1 = (data[self.tipe]['data']['string']+' %s, %s).xls') % (isi, '{}/{}'.format(self.period_start, self.period_end))
        hasil = string + string1
        if self.tipe == 'all':
            hasil = 'REKAPITULASI PPh21 (Periode, %s All).xls' % ('{}/{}'.format(self.period_start, self.period_end))
        return hasil, query

    @ api.multi
    def print_report(self):
        val = self
        sheet_name = 'Recap PPh21'
        filename, query = self.filename_generator()
        book = xlwt.Workbook(style_compression=2)
        sheet = book.add_sheet(sheet_name, cell_overwrite_ok=True)
        xlwt.add_palette_colour("soft_yellow", 0x21)
        sheet.show_grid = False
        book.set_colour_RGB(0x21, 241, 225, 164)

        style_num = xlwt.XFStyle()
        borders = xlwt.Borders()
        borders.bottom = xlwt.Borders.THIN
        borders.right = xlwt.Borders.THIN
        borders.left = xlwt.Borders.THIN
        borders.top = xlwt.Borders.THIN
        style_num.num_format_str = '#,##0.00'
        style_num.borders = borders
        style_num.alignment = xlwt.Alignment()
        style_num.alignment.horz = xlwt.Alignment.HORZ_CENTER
        style_num.alignment.vert = xlwt.Alignment.VERT_CENTER
        style_no_bold = xlwt.easyxf('borders: left thin, right thin, top thin, bottom thin;'
                                    'align: wrap on; align: horz center')
        style_no_border = xlwt.easyxf('font: bold 1; align: horz left;')
        style_no_border_title = xlwt.easyxf('font: bold 1, height 250; align: horz left;')
        style_no_bold.num_format_str = '#,##0.00'
        style_no_bold_yell = xlwt.easyxf('borders: left thin, right thin, top thin, bottom thin;'
                                         'align: horz center; pattern: pattern solid, fore_colour yellow;')
        style_no_bold_yell.num_format_str = '#,##0.00'
        style_bold_center_yell = xlwt.easyxf('font: bold 1;''borders: left thin, right thin, top thin, bottom thin;'
                                             'align: horz center; pattern: pattern solid, fore_colour yellow;')
        style_header_blue = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour pale_blue;'
                                        'borders: left thin, right thin, top thin, bottom thin;'
                                        'align: vert centre, horz center')
        style_header_green = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour light_green;'
                                         'borders: left thin, right thin, top thin, bottom thin;'
                                         'align: vert centre, horz center')
        style_header_tan = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour tan;'
                                       'borders: left thin, right thin, top thin, bottom thin;'
                                       'align: vert centre, horz center')
        style_header_yell = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour yellow;'
                                        'borders: left thin, right thin, top thin, bottom thin;'
                                        'align: vert centre, horz center')

        col_width = 256 * 20

        try:
            for i in itertools.count():
                sheet.col(i).width = col_width
                sheet.col(0).width = 256 * 4
                sheet.col(1).width = 256 * 4
                sheet.col(4).width = 256 * 10
                sheet.col(6).width = 256 * 6
                sheet.col(7).width = 256 * 6
                sheet.col(8).width = 256 * 7
                sheet.col(9).width = 256 * 12
                sheet.col(10).width = 256 * 28
                sheet.col(11).width = 256 * 28
                sheet.col(12).width = 256 * 40
                sheet.col(13).width = 256 * 40
                sheet.row(i).height = 5 * 65
                sheet.row(i).height = 5 * 65
                sheet.set_panes_frozen(True)
                sheet.set_vert_split_pos(5)
                sheet.set_remove_splits(True)
        except ValueError:
            pass

        col = 0
        row = 7
        # headers, list_data, total_all, col_pendapatan, col_potongan, col_lainnya = self.get_lines()
        headers, list_data,total_all, col_pendapatan, col_potongan, col_lainnya = self.get_lines()

        color = {
            'ijo': style_header_green,
            'biru': style_header_blue,
            'merah': style_header_tan,
            'kuning': style_header_yell,
        }

        header_employee = ['NO', 'NAME', 'NIK', 'STATUS', 'NPWP','First','Last','P-Total','Work Period']
        period = str(self.period_start) + ' / ' + str(self.period_end)
        sheet.write_merge(3, 3, 1, 3, "REKAPITULASI PPH 21", style_no_border_title)
        sheet.write_merge(4, 4, 1, 3, period, style_no_border)
        colh = 0
        col = 2
        for x in header_employee:
            colh += 1
            sheet.write_merge(row, row, colh, colh, x, style_header_blue)

        col = 10
        for x in headers:
            style = color[x['warna']]
            sheet.write(row, col, x['nama'], style)
            col += 1

        row += 1
        no = 0
        first = 1
        last = 12
        pt = 12
        wp = ''
        wpl = 'Dec'
        wps = 'Jan'
        # INI MASUKIN DATA
        for data in list_data:
            jd = str(data['join_date'])
            if jd != 'None':
                datem = datetime.strptime(jd, "%Y-%m-%d")
                dates = datem.strftime("%b")
                if datem.year == date.today().year:
                    first = datem.month
                    wps = dates
                    wpl = 'Dec'
                else:
                    wps = 'Jan'
                    wpl = 'Dec'
                    first = 1
            else:
                first = 1
                last = 12
            pt = last - first + 1
            wp = wps + '-' + wpl
            no += 1
            isi = [
                str(int(no)),
                data['karyawan'],
                data['identification_id'],
                data['status'],
                data['no_npwp'],
                str(int(first)),
                str(int(last)),
                str(int(pt)),
                wp,
            ]
            # print("====================>",data['karyawan'])
            for x in headers:
                # print("xxxxxxxxxxxxx",x['nama'])
                # if x['nama'] != 'Tunjangan Uang Saku' or x['nama'] != 'Tunjangan Lain':
                isi = isi + [data[x['nama']]]
            # isi = isi + [data[x['nama']] for x in headers]
            col = 1
            for x in isi:
                sheet.write(row, col, x, style_num)
                col += 1
            row += 1

        col = 9

        sheet.write_merge(row, row, 1, 9, 'Total', style_bold_center_yell)
        sheet.row(row).height = 5 * 60

        col += 1
        for x in headers:
            sheet.write(row, col, total_all[x['nama']], style_no_bold_yell)
            col += 1

        MYDIR = os.path.dirname(__file__)
        MYDIR = MYDIR+'/logo.bmp'
        sheet.insert_bitmap(MYDIR, 0, 1, x=2, y=2, scale_x=0.12, scale_y=0.12)

        file_data = StringIO()
        i = book.save(file_data)

        out = base64.encodestring(file_data.getvalue())
        self.write({'data_file': out, 'name': filename})
        view_id = self.env.ref('ab_hr_payroll.hr_rekap_pph21_form_view').id
        return {
            'view_type': 'form',
            'view_id': [view_id],
            'view_mode': 'form',
            'res_id': val.id,
            'res_model': self._name,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    @api.multi
    def get_lines(self):

        def reformat_kategori(lines):
            kategori = {
                'ALW': '1',
                'BASIC': '1',
                'DED': '2',
                'GROSS': '3',
                'NET': '3',
                'COMP':'1',
                'LAIN': '1',
            }
            for x in lines:
                hasil = kategori[x['kategori']]
                x['kategori'] = hasil
            return lines

        def netto_one_year(lines):
            header = []
            for x in lines:
                if x['nama'] == 'Total Netto':
                    if x['nama'] not in header:
                        header.append(x['nama'])
            return header

        def pendapatan(lines):
            header = []
            for x in lines:
                if x['kategori'] == '1':
                    if x['nama'] not in header:
                        header.append(x['nama'])
            # print("HEAD NAME PENDAPATAN===================> ",header)
            return header

        def potongan(lines):
            header = []
            for x in lines:
                if x['kategori'] == '2':
                    if x['nama'] not in header:
                        header.append(x['nama'])
            # print("HEAD NAME POTONGAN===================> ",header)
            return header

        def lainnya(lines):
            header = []
            for x in lines:
                if x['kategori'] == '3':
                    if x['nama'] not in header:
                        header.append(x['nama'])
            # print("HEAD NAME LAINYA===================> ",header)
            return header

        def employee(lines):
            employee = []
            for x in lines:
                if x['emp_id'] not in employee:
                    employee.append(x['emp_id'])
            return employee

        def dict_peremployee(lines, employee):
            return [z for z in lines if z['emp_id'] == employee]

        def dict_value_header(lines, isi, header):
            for x in header:
                # print("=================CARI HEAD==========",x)
                for z in lines:
                    # print(z['karyawan'],"==========> ",z['nama']," ========= ",z['total'])
                    if x in isi:
                        # print("++++++++ DI DALAM ISI+++++++++++++++",x)
                        if z['nama'] == x:
                            # print("==================== NEMU BENAR INI ",z['nama'])
                            # print("==================== ISI XXXX ",isi[x])
                            isi[x] = z['total']
                        # tampung_pph= []
                        # if z['nama'] == 'Tunjangan PPh21':
                        #     print("==================== NEMU BENAR INI ",z['nama'])
                            # isi[x] = z['total']
                        
                        # elif isi[x] != 0:
                        #     pass
                            
                    else:
                        # print("---------------MASUK SINI DONG--------")
                        if z['nama'] == x:
                            isi[x] = z['total']
                            # print("====================MASUK",z['nama'])
                        else:
                            isi[x] = 0
            return isi
        
        def dict_header(pendapatan, potongan,lainnya):
            urutan = 0
            list_header = []
            for x in pendapatan:
                urutan += 1
                list_header.append({
                    'nama': x,
                    'urutan': urutan,
                    'warna': 'biru',
                })
            urutan += 1
            # list_header.append({
            #     'nama': 'Total Pendapatan', 'urutan': urutan,
            #     'warna': 'ijo',
            # })
            for x in potongan:
                urutan += 1
                list_header.append({
                    'nama': x, 'urutan': urutan,
                    'warna': 'biru',
                })
            urutan += 1
            # list_header.append({
            #     'nama': 'Total Potongan', 'urutan': urutan,
            #     'warna': 'merah',
            # })
            # urutan += 1

            for x in lainnya:
                urutan += 1
                list_header.append({
                    'nama': x, 'urutan': urutan,
                    'warna': 'biru',
                })
            urutan += 1
            # TAMBAHIN TAMBAHAN
            # list_header.append({'nama': 'Tunjangan Uang Saku', 'urutan': urutan,'warna': 'biru'})
            # list_header.append({'nama': 'Tunjangan Lain', 'urutan': urutan,'warna': 'biru'})
            # urutan += 1
            # list_header.append({
            #     'nama': 'Total Diterima', 'urutan': urutan,
            #     'warna': 'kuning',
            # })
            # SORT THE DATA
            lh = len(list_header)
            for i in range(lh):
                if list_header[i]['nama'] == 'Gaji Pokok':
                    list_header[i]['urutan'] = 1
                elif list_header[i]['nama'] == 'Overtime':
                    list_header[i]['urutan'] = 2
                elif list_header[i]['nama'] == 'Tunjangan Uang Saku':
                    list_header[i]['urutan'] = 3
                elif list_header[i]['nama'] == 'Tunjangan Lainnya':
                    list_header[i]['urutan'] = 4
                elif list_header[i]['nama'] == 'BPJS Jaminan Kecelakaan Kerja 0.24%':
                    list_header[i]['urutan'] = 5
                elif list_header[i]['nama'] == 'BPJS Jaminan Kematian 0.3%':
                    list_header[i]['urutan'] = 6
                elif list_header[i]['nama'] == 'Tunjangan PPh21':
                    list_header[i]['urutan'] = 7
                elif list_header[i]['nama'] == 'Total Bruto':
                    list_header[i]['urutan'] = 8
                elif list_header[i]['nama'] == 'Biaya Jabatan':
                    list_header[i]['urutan'] = 9
                elif list_header[i]['nama'] == 'BPJS TK (JHT 2%)':
                    list_header[i]['urutan'] = 10
                elif list_header[i]['nama'] == 'BPJS TK (JP 1%)':
                    list_header[i]['urutan'] = 11
                elif list_header[i]['nama'] == 'Total Netto':
                    list_header[i]['urutan'] = 12
                elif list_header[i]['nama'] == 'Total Netto 1 Tahun':
                    list_header[i]['urutan'] = 13
                elif list_header[i]['nama'] == 'PTKP':
                    list_header[i]['urutan'] = 14
                elif list_header[i]['nama'] == 'PKP 1 Tahun':
                    list_header[i]['urutan'] = 15
                elif list_header[i]['nama'] == 'PKP':
                    list_header[i]['urutan'] = 16
                elif list_header[i]['nama'] == 'PKP [ROUND DOWN]':
                    list_header[i]['urutan'] = 17
                elif list_header[i]['nama'] == 'PPh 21 NPWP Terhutang':
                    list_header[i]['urutan'] = 18
                elif list_header[i]['nama'] == 'PPh 21 Non NPWP Terhutang':
                    list_header[i]['urutan'] = 19
                elif list_header[i]['nama'] == 'PPh 21 Real Setahun':
                    list_header[i]['urutan'] = 20
                elif list_header[i]['nama'] == 'Tunjangan PPh21':
                    list_header[i]['urutan'] = 21
                else:    
                    list_header[i]['urutan'] = 28
            return list_header


        def get_total(lines, header):
            hasil = 0

            for z in header:
                hasil += lines[z]
            return hasil

        # For total every row
        def get_total_all(header, lines):
            dict_total_all = {}
            for x in header:
                # if x['nama'] == 'Tunjangan Uang Saku' or x['nama'] == 'Tunjangan Lain':
                #     dict_total_all[x['nama']] = 0
                # else:
                #     dict_total_all[x['nama']] = sum([a[x['nama']] for a in lines])
                dict_total_all[x['nama']] = sum([a[x['nama']] for a in lines])
            return dict_total_all

        def employee_template(lines):
            for nih in lines:
                return nih

        def data_serve(lines, employee, pendapatan, potongan, lainnya):
            lines = dict_peremployee(lines, employee)
            isi = employee_template(lines) # MASIH 0
            lines_pendapatan = dict_value_header(lines, isi, pendapatan)
            # total_pendapatan = get_total(lines_pendapatan, pendapatan)
            # lines_pendapatan['Total Pendapatan'] = total_pendapatan
            isi = employee_template(lines)
            lines_potongan = dict_value_header(lines, isi, potongan)
            # total_potongan = get_total(lines_potongan, potongan)
            # lines_potongan['Total Potongan'] = total_potongan
            isi = employee_template(lines)
            lines_lainnya = dict_value_header(lines, isi, lainnya)
            new_dicts = {}
            for x in lines_pendapatan:
                new_dicts[x] = lines_pendapatan[x]
            for x in lines_potongan:
                new_dicts[x] = lines_potongan[x]
            for x in lines_lainnya:
                new_dicts[x] = lines_lainnya[x]
            # new_dicts['Total Diterima'] = total_pendapatan+total_potongan
            return new_dicts

        val = self

        filename, where_query = self.filename_generator()

        query = """ 

            SELECT
                sc.code as kategori, 
                he.id as emp_id, 
                he.name as karyawan, 
                he.bpjs_tk,
                he.identification_id,
                he.tanggal_join as join_date,
                hpl.name as nama,
                j.name as jabatan, 
                rp.name as alamat, 
                he.nik, 
                b.acc_number as no_rek,
                s.name as status, 
                d.name as department, 
                he.work_location as lokasi,
                he.no_npwp,
                he.ptkp_id,
                sum(hpl.total) as total 
            
            FROM hr_payslip_line hpl
            LEFT JOIN hr_payslip hp on hpl.slip_id = hp.id
            LEFT JOIN hr_employee he on he.id = hp.employee_id
            LEFT JOIN hr_salary_rule_category sc on (sc.id = hpl.category_id)
            LEFT JOIN hr_job j on (j.id = he.job_id)
            LEFT JOIN res_partner rp on (rp.id = he.address_id)
            LEFT JOIN res_partner_bank b on (b.id = he.bank_account_id)
            LEFT JOIN hr_department d on (d.id = he.department_id)
            LEFT JOIN hr_ptkp s on (s.id = he.ptkp_id)            
            
            WHERE hp.date_from >= '%s' and hp.date_to <= '%s' and hp.state = 'done' 
            AND hpl.code IN ('GP', 'OT', 'TM', 'TT', 'TPPH21', 'BRT', 'NET', 'BJ',    'PTKP', 'PKP', 'NPWP', 'NNPWP', 'PPH21TH', 'PPH',  'JKK','JKM','BPJSTKHT','BPJSTKJP','TUS','TL','PPH21')
            group by nama,kategori, bpjs_tk, identification_id, emp_id,karyawan, jabatan, alamat, nik, no_rek, status,
            department, lokasi, ptkp_id, he.no_npwp
            order by nik, karyawan, lokasi
             
         """ % (val.period_start, val.period_end)

            # AND hpl.code IN ('GP', 'OT', 'TM', 'TT',     'TPPH21', 'BRT', 'NET', 'BJ', 'KTK', 'BPJSTK', 'TKJP',    'PTKP', 'PKP', 'NPWP', 'NNPWP', 'PPH21TH', 'PPH',  'JKK','JKM','JHT','JP')
        self._cr.execute(query)
        lines = self._cr.dictfetchall()
        for l in lines:
            if l['nama'] == 'Total Netto':
                first = 1
                last = 12
                jd = str(l['join_date'])
                if jd != 'None':
                    datem = datetime.strptime(jd, "%Y-%m-%d")
                    dates = datem.strftime("%b")
                    if datem.year == date.today().year:
                        first = datem.month
                    else:
                        first = 1
                else:
                    first = 1
                    last = 12
                pt = last - first + 1
                total = l['total'] * pt
                lines.append({
                    'kategori': l['kategori'],
                    'emp_id': l['emp_id'],
                    'karyawan': l['karyawan'],
                    'bpjs_tk': l['bpjs_tk'],
                    'identification_id': l['identification_id'],
                    'join_date': l['join_date'],
                    'jabatan': l['jabatan'],
                    'alamat': l['alamat'],
                    'nik': l['nik'],
                    'no_rek': l['no_rek'],
                    'status': l['status'],
                    'department': l['department'],
                    'lokasi': l['lokasi'],
                    'no_npwp': l['no_npwp'],
                    'ptkp_id': l['ptkp_id'],
                    'nama': 'Total Netto 1 Tahun',
                    'total': total})
            if l['nama'] == 'PKP':
                rd = round(l['total']-500,-3)
                lines.append({
                    'kategori': l['kategori'],
                    'emp_id': l['emp_id'],
                    'karyawan': l['karyawan'],
                    'bpjs_tk': l['bpjs_tk'],
                    'identification_id': l['identification_id'],
                    'join_date': l['join_date'],
                    'jabatan': l['jabatan'],
                    'alamat': l['alamat'],
                    'nik': l['nik'],
                    'no_rek': l['no_rek'],
                    'status': l['status'],
                    'department': l['department'],
                    'lokasi': l['lokasi'],
                    'no_npwp': l['no_npwp'],
                    'ptkp_id': l['ptkp_id'],
                    'nama': 'PKP [ROUND DOWN]',
                    'total': rd})
        lines = reformat_kategori(lines)
        header_pendapatan = pendapatan(lines)
        header_potongan = potongan(lines)
        header_lainnya = lainnya(lines)

        col_pendapatan = len(header_pendapatan)
        col_potongan = len(header_potongan)
        col_lainnya = len(header_lainnya)

        row = 0
        col = 0

        employee_ids = employee(lines)
        list_data = [data_serve(lines, x, header_pendapatan, header_potongan,header_lainnya) for x in employee_ids]

        header = dict_header(header_pendapatan, header_potongan,header_lainnya)
        header = sorted(header, key=lambda i: i['urutan'])

        total_all = get_total_all(header, list_data)

        return header, list_data,total_all, col_pendapatan, col_potongan, col_lainnya
        # return header, list_data, total_all, col_pendapatan, col_potongan,col_lainnya
