
import datetime
from datetime import date, timedelta
from io import BytesIO as StringIO
import xlwt
import xlsxwriter
import base64
import itertools
import logging
from collections import defaultdict
from dateutil.relativedelta import relativedelta
import calendar
import re
from collections import OrderedDict
from odoo import models, fields, api, _
from PIL import Image
import os
from os.path import expanduser
from datetime import datetime
from dateutil import relativedelta

_logger = logging.getLogger(__name__)

ID_MONTHES = {
    '01': 'Januari',
    '02': 'Februari',
    '03': 'Maret',
    '04': 'April',
    '05': 'Mei',
    '06': 'Juni',
    '07': 'Juli',
    '08': 'Agustus',
    '09': 'September',
    '10': 'Oktober',
    '11': 'Nopember',
    '12': 'Desember',
}


class RekapGaji(models.TransientModel):
    _name = 'recap.bpjs'
    _description = 'Recap BPJS'

    tipe = fields.Selection(string='Tipe', selection=[('all', 'All')], default="all")
    user_id = fields.Many2one(comodel_name='res.users', string='Created By')
    partner_id = fields.Many2one('res.partner', string='Partner')
    department_id = fields.Many2one(comodel_name='hr.department', string='Department')
    name = fields.Char(string='File name')
    data_file = fields.Binary(string='Data File')
    period = fields.Date(string='Tanggal', default=fields.Date.today())

    period_start = fields.Date(string='Period Start', default=datetime.now().strftime('%Y-%m-01'))
    period_end = fields.Date(string='Period End', default=str(datetime.now() + relativedelta.relativedelta(months=+1, day=1, days=-1)))

    @api.multi
    def print_report(self):
        template_report = 'ab_hr_payroll.report_bpjstk'
        return self.env.ref(template_report).report_action(self)

    # @api.multi
    # def filename_generator(self):
    #     reformat_month = self.period.strftime('%m')
    #     reformat_year = self.period.strftime('%Y')
    #     string = "REKAPITULASI BPJS KETENAGAKERJAAN ("
    #     data = {
    #         'city':
    #             {'data': {
    #                 'string': 'Kota',
    #                 'isi': self.partner_id.city,
    #                 'query': 'and rp.id = %s' % (self.partner_id.id)
    #             }},
    #         'dep':
    #             {'data': {
    #                 'string': 'Departemen',
    #                 'isi': self.department_id.name,
    #                 'query': 'd.id = %s' % (self.department_id.id)
    #             }},
    #         'all':
    #             {'data': {
    #                 'string': '',
    #                 'isi': '',
    #                 'query': '1=1'
    #             }}
    #     }
    #     query = data[self.tipe]['data']['query']
    #     isi = data[self.tipe]['data']['isi']
    #     string1 = (data[self.tipe]['data']['string']+' %s, %s).xls') % (isi, '{}/{}'.format(self.period_start, self.period_end))
    #     hasil = string + string1
    #     if self.tipe == 'all':
    #         hasil = 'REKAPITULASI BPJS KETENAGAKERJAAN (Periode, %s All).xls' % ('{}/{}'.format(self.period_start, self.period_end))

    #     return hasil, query

    # @api.multi
    # def print_report(self):
    #     val = self
    #     sheet_name = 'Recap BPJS'
    #     filename, query = self.filename_generator()
    #     book = xlwt.Workbook(style_compression=2)
    #     sheet = book.add_sheet(sheet_name, cell_overwrite_ok=True)
    #     xlwt.add_palette_colour("soft_yellow", 0x21)
    #     sheet.show_grid = False
    #     book.set_colour_RGB(0x21, 241, 225, 164)

    #     style_num = xlwt.XFStyle()
    #     borders = xlwt.Borders()
    #     borders.bottom = xlwt.Borders.THIN
    #     borders.right = xlwt.Borders.THIN
    #     borders.left = xlwt.Borders.THIN
    #     borders.top = xlwt.Borders.THIN
    #     style_num.num_format_str = '#,##0.00'
    #     style_num.borders = borders
    #     style_num.alignment = xlwt.Alignment()
    #     style_num.alignment.horz = xlwt.Alignment.HORZ_CENTER
    #     style_num.alignment.vert = xlwt.Alignment.VERT_CENTER
    #     style_no_bold = xlwt.easyxf('borders: left thin, right thin, top thin, bottom thin;'
    #                                 'align: wrap on; align: horz center')
    #     style_no_border = xlwt.easyxf('font: bold 1; align: horz left;')
    #     style_no_border_title = xlwt.easyxf('font: bold 1, height 250; align: horz left;')
    #     style_no_bold.num_format_str = '#,##0.00'
    #     style_no_bold_yell = xlwt.easyxf('borders: left thin, right thin, top thin, bottom thin;'
    #                                      'align: horz center; pattern: pattern solid, fore_colour yellow;')
    #     style_no_bold_yell.num_format_str = '#,##0.00'
    #     style_bold_center_yell = xlwt.easyxf('font: bold 1;''borders: left thin, right thin, top thin, bottom thin;'
    #                                          'align: horz center; pattern: pattern solid, fore_colour yellow;')
    #     style_header_blue = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour pale_blue;'
    #                                     'borders: left thin, right thin, top thin, bottom thin;'
    #                                     'align: vert centre, horz center')
    #     style_header_green = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour light_green;'
    #                                      'borders: left thin, right thin, top thin, bottom thin;'
    #                                      'align: vert centre, horz center')
    #     style_header_tan = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour tan;'
    #                                    'borders: left thin, right thin, top thin, bottom thin;'
    #                                    'align: vert centre, horz center')
    #     style_header_yell = xlwt.easyxf('font: bold 1; pattern: pattern solid, fore_colour yellow;'
    #                                     'borders: left thin, right thin, top thin, bottom thin;'
    #                                     'align: vert centre, horz center')

    #     col_width = 256 * 20

    #     try:
    #         for i in itertools.count():
    #             sheet.col(i).width = col_width
    #             sheet.col(0).width = 256 * 5
    #             sheet.col(1).width = 256 * 10
    #             sheet.col(3).width = 256 * 30
    #             sheet.col(4).width = 256 * 30
    #             sheet.col(5).width = 256 * 30
    #             sheet.col(8).width = 256 * 40
    #             sheet.col(9).width = 256 * 35
    #             sheet.col(10).width = 256 * 40
    #             sheet.row(i).height = 5 * 65
    #             sheet.row(i).height = 5 * 65
    #             sheet.set_panes_frozen(True)
    #             sheet.set_vert_split_pos(6)
    #             sheet.set_remove_splits(True)
    #     except ValueError:
    #         pass

    #     col = 0
    #     row = 7
    #     headers, list_data, total_all, col_pendapatan, col_potongan = self.get_lines()

    #     color = {
    #         'ijo': style_header_green,
    #         'biru': style_header_blue,
    #         'merah': style_header_tan,
    #         'kuning': style_header_yell,
    #     }

    #     header_employee = ['NO', 'NOMOR KJP', 'NOMOR INDUK KARYAWAN', 'NOMOR KARTU TANDA PENDUDUK', 'NAMA LENGKAP TENAGA KERJA']
    #     period = str(self.period_start) + ' / ' + str(self.period_end)
    #     sheet.write_merge(3, 3, 1, 3, "DATA GAJI KARYAWAN", style_no_border_title)
    #     sheet.write_merge(4, 4, 1, 3, period, style_no_border)
    #     colh = 0
    #     col = 2
    #     for x in header_employee:
    #         colh += 1
    #         sheet.write_merge(row, row, colh, colh, x, style_header_blue)

    #     col = 6
    #     for x in headers:
    #         style = color[x['warna']]
    #         sheet.write(row, col, x['nama'], style)
    #         col += 1

    #     row += 1
    #     no = 0
    #     for data in list_data:
    #         no += 1
    #         isi = [
    #             str(int(no)),
    #             data['bpjs_tk'],
    #             data['nik'],
    #             data['identification_id'],
    #             data['karyawan'],

    #         ]

    #         isi = isi + [data[x['nama']] for x in headers]
    #         col = 1
    #         for x in isi:
    #             sheet.write(row, col, x, style_num)
    #             col += 1
    #         row += 1

    #     col = 5

    #     sheet.write_merge(row, row, 1, 5, 'Total', style_bold_center_yell)
    #     sheet.row(row).height = 5 * 60

    #     col += 1
    #     for x in headers:
    #         sheet.write(row, col, total_all[x['nama']], style_no_bold_yell)
    #         col += 1

    #     MYDIR = os.path.dirname(__file__)
    #     MYDIR = MYDIR+'/logo.bmp'
    #     sheet.insert_bitmap(MYDIR, 0, 1, x=2, y=2, scale_x=0.12, scale_y=0.12)

    #     file_data = StringIO()
    #     i = book.save(file_data)

    #     out = base64.encodestring(file_data.getvalue())
    #     self.write({'data_file': out, 'name': filename})
    #     view_id = self.env.ref('ab_hr_payroll.hr_recap_bpjs_form_view').id
    #     return {
    #         'view_type': 'form',
    #         'view_id': [view_id],
    #         'view_mode': 'form',
    #         'res_id': val.id,
    #         'res_model': self._name,
    #         'type': 'ir.actions.act_window',
    #         'target': 'new',
    #     }

    # @api.multi
    # def get_lines(self):

    #     def reformat_kategori(lines):
    #         kategori = {
    #             'ALW': '1',
    #             'BASIC': '1',
    #             'DED': '2',
    #             'GROSS': '3',
    #             'NET': '3',
    #         }
    #         for x in lines:
    #             hasil = kategori[x['kategori']]
    #             x['kategori'] = hasil
    #         return lines

    #     def pendapatan(lines):
    #         header = []
    #         for x in lines:
    #             if x['kategori'] == '1':
    #                 if x['nama'] not in header:
    #                     header.append(x['nama'])
    #         return header

    #     def potongan(lines):
    #         header = []
    #         for x in lines:
    #             if x['kategori'] == '2':
    #                 if x['nama'] not in header:
    #                     header.append(x['nama'])
    #         return header

    #     def employee(lines):
    #         employee = []
    #         for x in lines:
    #             if x['emp_id'] not in employee:
    #                 employee.append(x['emp_id'])
    #         return employee

    #     def dict_peremployee(lines, employee):
    #         return [z for z in lines if z['emp_id'] == employee]

    #     def dict_value_header(lines, isi, header):
    #         for x in header:
    #             for z in lines:
    #                 if x in isi:
    #                     if z['nama'] == x:
    #                         isi[x] = z['total']
    #                     if isi[x] != 0:
    #                         pass
    #                 else:
    #                     isi[x] = 0
    #         return isi

    #     def dict_header(pendapatan, potongan):
    #         urutan = 0
    #         list_header = []
    #         for x in pendapatan:
    #             urutan += 1
    #             list_header.append({
    #                 'nama': x,
    #                 'urutan': urutan,
    #                 'warna': 'ijo',
    #             })

    #         for x in potongan:
    #             urutan += 1
    #             list_header.append({
    #                 'nama': x, 'urutan': urutan,
    #                 'warna': 'ijo',
    #             })

    #         urutan += 1
    #         list_header.append({
    #             'nama': 'Total Potongan', 'urutan': urutan,
    #             'warna': 'ijo',
    #         })

    #         urutan += 1
    #         list_header.append({
    #             'nama': 'Total', 'urutan': urutan,
    #             'warna': 'ijo',
    #         })

    #         return list_header

    #     def get_total(lines, header):
    #         hasil = 0

    #         for z in header:
    #             hasil += lines[z]
    #         return hasil

    #     def get_total_all(header, lines):
    #         dict_total_all = {}
    #         for x in header:
    #             dict_total_all[x['nama']] = sum([a[x['nama']] for a in lines])
    #         return dict_total_all

    #     def employee_template(lines):
    #         for x in lines:
    #             return x

    #     def data_serve(lines, employee, pendapatan, potongan):
    #         lines = dict_peremployee(lines, employee)

    #         isi = employee_template(lines)
    #         lines_pendapatan = dict_value_header(lines, isi, pendapatan)
    #         total_pendapatan = get_total(lines_pendapatan, pendapatan)
    #         # lines_pendapatan['Total Pendapatan'] = total_pendapatan

    #         isi = employee_template(lines)
    #         lines_potongan = dict_value_header(lines, isi, potongan)
    #         total_potongan = get_total(lines_potongan, potongan)
    #         lines_potongan['Total Potongan'] = total_potongan

    #         new_dicts = {}
    #         for x in lines_pendapatan:
    #             new_dicts[x] = lines_pendapatan[x]

    #         for x in lines_potongan:
    #             new_dicts[x] = lines_potongan[x]

    #         new_dicts['Total'] = total_pendapatan + total_potongan
    #         return new_dicts

    #     val = self
    #     filename, where_query = self.filename_generator()

    #     query = """ 

    #         SELECT
    #             sc.code as kategori, 
    #             he.id as emp_id, 
    #             he.name as karyawan, 
    #             he.bpjs_tk,
    #             he.identification_id,
    #             hpl.name as nama,
    #             j.name as jabatan, 
    #             rp.name as alamat, 
    #             he.nik, 
    #             b.acc_number as no_rek,
    #             s.name as status, 
    #             d.name as department, 
    #             he.work_location as lokasi,
    #             sum(hpl.total) as total 
            
    #         FROM hr_payslip_line hpl
    #         LEFT join hr_payslip hp on hpl.slip_id = hp.id
    #         LEFT join hr_employee he on he.id = hp.employee_id
    #         LEFT JOIN hr_salary_rule_category sc on (sc.id = hpl.category_id)
    #         LEFT JOIN hr_job j on (j.id = he.job_id)
    #         LEFT JOIN res_partner rp on (rp.id = he.address_id)
    #         LEFT JOIN res_partner_bank b on (b.id = he.bank_account_id)
    #         LEFT JOIN hr_department d on (d.id = he.department_id)
    #         LEFT JOIN hr_ptkp s on (s.id = he.ptkp_id)            

            
    #         WHERE hp.date_from >= '%s' and hp.date_to <= '%s' and hp.state = 'done' and %s 
    #         AND hpl.code IN ('GP', 'JKK', 'JKM', 'JHT', 'JP', 'TKJP', 'BPJSTKHT', 'BPJSTKJP')

    #         group by kategori, bpjs_tk, nama, identification_id, emp_id,karyawan, jabatan, alamat, nik, no_rek, status,
    #         department, lokasi
    #         order by nik, karyawan, lokasi
             
    #      """ % (val.period_start, val.period_end, where_query)

    #     self._cr.execute(query)
    #     lines = self._cr.dictfetchall()
    #     lines = reformat_kategori(lines)

    #     header_pendapatan = pendapatan(lines)
    #     header_potongan = potongan(lines)

    #     col_pendapatan = len(header_pendapatan)
    #     col_potongan = len(header_potongan)

    #     row = 0
    #     col = 0

    #     employee_ids = employee(lines)
    #     list_data = [data_serve(lines, x, header_pendapatan, header_potongan) for x in employee_ids]

    #     header = dict_header(header_pendapatan, header_potongan)
    #     header = sorted(header, key=lambda i: i['urutan'])


    #     # START
    #     for data in list_data:
    #         for head in header:
    #             if data[head['nama']] == 0:
    #                 data[head['nama']] = self._get_real_value(data['emp_id'], head['nama'])
    #     # END

    #     total_all = get_total_all(header, list_data)
    #     return header, list_data, total_all, col_pendapatan, col_potongan

    # def _get_real_value(self, employee_id, salary_code):
    #     val = self
    #     result = 0
    #     filename, where_query = self.filename_generator()
    #     query = """ 

    #         SELECT
    #             sum(hpl.total) as total 
    #         FROM hr_payslip_line hpl
    #         LEFT join hr_payslip hp on hpl.slip_id = hp.id
    #         LEFT join hr_employee he on he.id = hp.employee_id
    #         LEFT JOIN hr_salary_rule_category sc on (sc.id = hpl.category_id)
    #         LEFT JOIN hr_job j on (j.id = he.job_id)
    #         LEFT JOIN res_partner rp on (rp.id = he.address_id)
    #         LEFT JOIN res_partner_bank b on (b.id = he.bank_account_id)
    #         LEFT JOIN hr_department d on (d.id = he.department_id)
    #         LEFT JOIN hr_ptkp s on (s.id = he.ptkp_id)            

            
    #         WHERE hp.date_from >= '%s' and hp.date_to <= '%s' and hp.state = 'done' and %s 
    #         AND hpl.name = '%s'
    #         AND he.id = %s
    #     """ % (val.period_start, val.period_end, where_query, salary_code, employee_id)

    #     self._cr.execute(query)
    #     record = self._cr.dictfetchall()
    #     if record:
    #         result = record[0]['total']
    #     if result == None:
    #         result = 0
    #     return result
