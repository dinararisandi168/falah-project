from _datetime import datetime, date
from odoo import models, fields, api
from odoo.exceptions import ValidationError, Warning
from dateutil import relativedelta


class HrOvertimeRule(models.Model):
    _name = "hr.overtime.rule"
    _description = "Rule Overtime"

    name = fields.Char("Overtime ID")
    description = fields.Char("Description")
    hourly_rate_option = fields.Char("Hourly Rate Option")
    fixed = fields.Float("Fixed", compute="_fixed", store=True)
    formula = fields.Float("Formula", default="1")
    pembagi = fields.Float()
    maximum_amount = fields.Float("Maximum Amount", track_visibility="onchange")
    type_overtime = fields.Selection(
        [
            ("otday", "Overtime Weekday Project"),
            ("otend", "Overtime Weekend Project"),
            ("non-otday", "Overtime Weekday Non-Project"),
            ("non-otend", "Overtime Weekend Non-Project"),
        ], string="Tipe Overtime")

    basic_salary = fields.Char("Basic Salary")
    overtime_line = fields.One2many("overtime.rule.line", "overtime_id", string="Overtime Rule")
    overtime_rule_id = fields.Many2one("hr.overtime")

    @api.depends("formula", "pembagi", "basic_salary")
    def _fixed(self):
        for res in self:
            if res.pembagi > 0:
                res.fixed = round((res.formula / res.pembagi) * float(res.basic_salary))


class OvertimeRuleLine(models.Model):
    _name = "overtime.rule.line"
    _description = "Overtime Rule Line"

    overtime_id = fields.Many2one("hr.overtime.rule", string="Overtime Rule")
    number = fields.Char("No")
    description = fields.Char("Description")
    duration = fields.Integer("Duration (Hour)")
    half_time = fields.Integer("Half")
    full_time = fields.Integer("Full")
    factor_progresive = fields.Float("Factor")


class HrOvertime(models.Model):
    _inherit = "hr.overtime"

    overtime_rule_id = fields.Many2one("hr.overtime.rule")
