from odoo import fields, models, api
from datetime import timedelta, datetime
from dateutil import relativedelta
import datetime
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell
from io import BytesIO as StringIO
import base64
from PIL import Image
import os
import calendar


class ReportOvertimeListXlsx(models.AbstractModel):
    _name = 'report.ab_hr_overtime.report_overtime_list_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_bold_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'bold': True})
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'left', 'valign': 'left', 'bold': True, 'size': 14})
        sub_header = workbook.add_format({'align': 'left', 'valign': 'left', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#4781fd')
        money_format = workbook.add_format({'num_format': '[$Rp-421]#,##0.00;[RED]-[$Rp-421]#,##0.00', 'font_size': 10, 'left': 1,
                                            'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True})
        worksheet = workbook.add_worksheet('OVERTIME LIST')
        logo = StringIO(base64.b64decode(self.env.user.company_id.logo))
        worksheet.insert_image(
            "B2", "logo.png", {"image_data": logo, "x_scale": 0.3, "y_scale": 0.3}
        )
        worksheet.set_column('A:A', 20)
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 4, 30)
        worksheet.set_column(5, 50, 15)
        worksheet.merge_range('B6:C6', 'OVERTIME LIST', heading_format)
        worksheet.merge_range('B7:C7', 'DATE: %s - %s' % (obj.date_from, obj.date_to), heading_format)
        row = 8
        col = 5
        start = obj.date_from
        while start <= obj.date_to:
            worksheet.write(row + 1, col, start.strftime('%d'), cell_text_format_top_left_right)
            col += 1

            start = start + timedelta(days=1)
        worksheet.merge_range(row, 5, row, col - 1, "TANGGAL", cell_text_format_top_left_right)

        worksheet.merge_range(row, col, row + 1, col,  "Total Jam", cell_text_format_top_left_right)
        no = 1
        NO = []

        worksheet.merge_range('A9:A10', 'NO', cell_text_format_top_left_right)
        worksheet.merge_range('B9:B10', 'Nama Karyawan', cell_text_format_top_left_right)
        worksheet.merge_range('C9:C10', 'Status Karyawan', cell_text_format_top_left_right)
        worksheet.merge_range('D9:D10', 'Gaji Pokok', cell_text_format_top_left_right)
        worksheet.merge_range('E9:E10', 'Upah Lembur/Jam', cell_text_format_top_left_right)

        karyawan = self.get_employee(obj.date_from, obj.date_to)
        col = 1
        if karyawan:
            for kw in karyawan:
                NO.append(no)
                no += 1
                row += 1
                gaji = self.get_wage(kw['id_karyawan'])
                for g in gaji:
                    worksheet.write(row + 1, col + 2, float(g["gaji"]), money_format)
                    worksheet.write(row + 1, col + 3, float(g["upah"]), money_format)

                overtime = self.get_overtime(kw['id_karyawan'])
                start = obj.date_from
                col_date = 5
                while start <= obj.date_to:
                    add = False
                    for s in overtime:

                        if start == s['tanggal']:
                            worksheet.write(row + 1, col_date, s['jumlah'], text_style)
                            add = True
                    if not add:
                        worksheet.write(row + 1, col_date, '-', text_style)

                    col_date += 1
                    start = start + timedelta(days=1)
                worksheet.write(row + 1, col, kw['karyawan'], text_style)
                worksheet.write(row + 1, col + 1, kw['status'], text_style)
                worksheet.write_formula(row + 1, col_date, '=SUM(%s)' % (xl_range(row + 1, 5, row + 1, col_date - 1)), text_style)

            worksheet.write_column('A11', NO, text_style)
            worksheet.merge_range(row + 2, 0, row + 2, col_date - 1, 'Total', text_bold_style)
            worksheet.write_formula(row + 2, col_date, '=SUM(%s)' % (xl_range(10, col_date, row + 1, col_date)), text_style)

    def get_employee(self, date_from, date_to):
        query = """ SELECT he.id as id_karyawan, he.name as karyawan, CASE he.permanent WHEN True THEN 'Permanent' ELSE 'Kontrak' END as status
        FROM hr_overtime ho
        left join hr_employee he on he.id = ho.employee_id
        WHERE ho.date_overtime BETWEEN '%s' AND '%s'
        group by karyawan,id_karyawan""" % (date_from, date_to)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    def get_wage(self, employee):
        query = """select coalesce(hc.wage,0) as gaji, coalesce(round(hc.wage/173),0) as upah
        from hr_contract hc
        left join hr_employee he on he.id = hc.employee_id
        where hc.state = 'open' and he.id = %s
        order by hc.id desc limit 1 """ % (employee)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    def get_overtime(self, employee):
        query = """ select ho.date_overtime as tanggal, he.name as nama, sum(ho.durasi_progresif) as jumlah
        from hr_overtime ho
        left join hr_employee he on he.id = ho.employee_id
        where he.id = %s and (ho.state = 'done' or ho.state = 'paid')
        group by tanggal, he.name """ % (employee)
        self._cr.execute(query)
        return self._cr.dictfetchall()
