# -*- coding: utf-8 -*-

from odoo import api, models

class HrPayslipRun(models.Model):
    _inherit = 'hr.overtime'

    @api.multi
    def spl(self):
        return self.env['report'].get_action(self, 'ab_hr_overtime.spl')