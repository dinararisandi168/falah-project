from odoo import fields, models, api
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell
from datetime import date
from dateutil.relativedelta import relativedelta

class ReportOvertimeProjectXlsx(models.AbstractModel):
    _name = 'report.ab_hr_overtime.report_overtime_project_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_bold_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'bold': True})
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'left', 'valign': 'left', 'bold': True, 'size': 14})
        sub_header = workbook.add_format({'align': 'left', 'valign': 'left', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#4781fd') 
        currency_style = workbook.add_format({'num_format': '#,##0.00', 'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True})
        worksheet = workbook.add_worksheet('OVERTIME SUMMARY %s %s' % (dict(obj._fields['month'].selection).get(obj.month), obj.year))
        worksheet.write(0, 0, 'Summary Overtime - %s %s' %(dict(obj._fields['month'].selection).get(obj.month), obj.year), heading_format)
        worksheet.write(1, 0, 'Project', heading_format)

        start_date = date(obj.year, int(obj.month), 1)
        end_date = date(obj.year, int(obj.month), 1) + relativedelta(months=1, days=-1)
        project_list = self.get_project(start_date, end_date)
        project_header = [ 'SUMMARY ' + x['name'] for x in project_list]

        header = ['Nama Karyawan', 'Status Karyawan', 'Gaji Karyawan', 'Upah Lembur / Jam'] + project_header + ['TOTAL LEMBUR']
        worksheet.set_column(0, 0 + len(header), 20)
        worksheet.write_row(3, 0, header, text_style)
        employee_ids = self.get_employee(start_date, end_date)

        row = 4

        for emp in employee_ids:
            col = 0
            worksheet.write(row, col, emp['name'] or '', text_style)
            worksheet.write(row, col + 1, 'Tetap' if emp['permanent'] else 'Kontrak', text_style)

            wage = self.get_employee_wage(emp['id'])
            if wage:
                worksheet.write(row, col + 2, wage[0]['wage'] or 0, currency_style)
            else:
                worksheet.write(row, col + 2, 0, currency_style)
            worksheet.write_formula(row, col + 3, '=IFERROR(%s/173, 0)' % (xl_rowcol_to_cell(row, col + 2)), currency_style)
            
            col = 4
            for pj in project_list:
                summary = self.get_summary_employee(pj['id'], emp['id'], start_date, end_date)
                if summary:
                    worksheet.write(row, col, summary[0]['amount'], currency_style)
                else:
                    worksheet.write(row, col, 0, currency_style)
                col += 1
            
            worksheet.write_formula(row, col, '=SUM(%s)' % (xl_range(row, 4, row, col - 1)), currency_style)
            row += 1
        
        col = 0
        worksheet.merge_range(row, col, row, 3, 'TOTAL', currency_style)
       
        col = 4
        for pj in project_list:
            worksheet.write_formula(row, col, '=SUM(%s)' % (xl_range(4, col, row - 1, col)), currency_style)
            col += 1
        worksheet.write_formula(row, col, '=SUM(%s)' % (xl_range(4, col, row - 1, col)), currency_style)

        row +=2
        worksheet.write(row, col, 'Approved CEO :', text_style)
        worksheet.merge_range(row + 1, col, row + 5, col, '', text_style)
        worksheet.write(row, col - 1, 'Approved PM :', text_style)
        worksheet.merge_range(row + 1, col - 1, row + 5, col - 1, '', text_style)
        worksheet.write(row, col - 2 , 'Prepared :', text_style)
        worksheet.merge_range(row + 1, col - 2, row + 5, col - 2, '', text_style)
    


    def get_project(self, start_date, end_date):
        sql = """
            SELECT pp.id, pp.name FROM hr_overtime ovt
            LEFT JOIN project_project pp ON pp.id = ovt.project_id
            WHERE ovt.state IN ('done', 'paid') AND
            ovt.project_id IS NOT NULL AND
            ovt.date_overtime BETWEEN '%s' AND '%s'
            GROUP BY pp.id, pp.name
            ORDER BY pp.name
        """ % (start_date, end_date)

        self._cr.execute(sql)
        return self._cr.dictfetchall()
    
    def get_employee(self, start_date, end_date):
        sql = """
            SELECT emp.id, emp.name, emp.permanent FROM hr_overtime ovt
            LEFT JOIN hr_employee emp ON emp.id = employee_id
            WHERE ovt.state IN ('done', 'paid') AND
            project_id IS NOT NULL AND
            ovt.date_overtime BETWEEN '%s' AND '%s'
            GROUP BY emp.id, emp.name, emp.permanent
            ORDER BY emp.name
        """ % (start_date, end_date)

        self._cr.execute(sql)
        return self._cr.dictfetchall()
    
    def get_employee_wage(self, employee_id):
        sql = """
            SELECT employee_id, wage FROM hr_contract WHERE employee_id = %s AND state = 'open' LIMIT 1
        """ % (employee_id)

        self._cr.execute(sql)
        return self._cr.dictfetchall()
    
    def get_summary_employee(self, project_id, employee_id, start_date, end_date):
        sql ="""
            SELECT SUM(COALESCE(lembur, 0)) AS amount FROM hr_overtime
            WHERE state IN ('done', 'paid') AND
            project_id = %s AND
            employee_id = %s AND
            date_overtime BETWEEN '%s' AND '%s'
            GROUP BY employee_id
        """ % (project_id, employee_id, start_date, end_date)

        self._cr.execute(sql)
        return self._cr.dictfetchall()


