from odoo import fields, models, api
from datetime import timedelta, datetime
from dateutil import relativedelta
import datetime
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell
from io import BytesIO as StringIO
import base64
from PIL import Image
import os
import calendar


class ReportSummaryOvertimeDivisiXlsx(models.AbstractModel):
    _name = 'report.ab_hr_overtime.report_summary_overtime_divisi_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_bold_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, 'bold': True})
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'left', 'valign': 'left', 'bold': True, 'size': 14})
        sub_header = workbook.add_format({'align': 'left', 'valign': 'left', 'size': 11, 'bold': True})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#4781fd')
        money_format = workbook.add_format({'num_format': '[$Rp-421]#,##0.00;[RED]-[$Rp-421]#,##0.00', 'font_size': 10, 'left': 1,
                                            'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True})
        money_bold_format = workbook.add_format({'num_format': '[$Rp-421]#,##0.00;[RED]-[$Rp-421]#,##0.00', 'font_size': 10, 'left': 1,
                                            'bottom': 1, 'bold': True, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True})
        worksheet = workbook.add_worksheet('OVERTIME SUMMARY BY DIVISION')
        logo = StringIO(base64.b64decode(self.env.user.company_id.logo))
        worksheet.insert_image(
            "B2", "logo.png", {"image_data": logo, "x_scale": 0.3, "y_scale": 0.3}
        )
        worksheet.set_column('A:A', 20)
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 2, 30)
        worksheet.set_column(3, 3, 30)
        worksheet.set_column(4, 50, 15)
        project_name = ""
        if obj.project_id:
            project_name = "Project: %s" % (obj.project_id.name)
            worksheet.merge_range('B6:C6', 'OVERTIME SUMMARY BY DIVISION BY PROJECT', heading_format)
        else:
            if not obj.is_summary:
                worksheet.merge_range('B6:C6', 'OVERTIME SUMMARY BY DIVISION', heading_format)
                worksheet.write('C7', "Divisi: %s" % (obj.department_id.name), sub_header)
                worksheet.write('D7', "%s" % (project_name), sub_header)
            else:
                worksheet.merge_range('B6:C6', 'OVERTIME SUMMARY ALL DIVISION', heading_format)
        value = dict(obj._fields['month'].selection).get(obj.month)
        BULAN = ("%s %s" % (value.upper(), obj.year))

        if not obj.is_summary:
            worksheet.write('C7', "Divisi: %s" % (obj.department_id.name), sub_header)
            worksheet.write('D7', "%s" % (project_name), sub_header)
        
        worksheet.write('B7', BULAN, sub_header)
        row = 8
        col = 4
        list_week = []
        week = 0
        start = obj.date_from
        while start <= obj.date_to:
            if start.isocalendar()[1] not in list_week:
                list_week.append(start.isocalendar()[1])
                week += 1
                worksheet.write(row + 1, col, week, cell_text_format_top_left_right)
                col += 1

            start = start + timedelta(days=1)
        worksheet.merge_range(row, 4, row, col - 1, "MINGGU", cell_text_format_top_left_right)
        worksheet.merge_range(row, col, row + 1, col,  "Total Jam", cell_text_format_top_left_right)
        worksheet.merge_range(row, col + 1, row + 1, col + 1, "Total Lembur", cell_text_format_top_left_right)
        no = 1
        NO = []

        cal = calendar.Calendar()

        worksheet.merge_range('A9:A10', 'NO', cell_text_format_top_left_right)
        worksheet.merge_range('B9:B10', 'Nama Karyawan', cell_text_format_top_left_right)
        worksheet.merge_range('C9:C10', 'Gaji Pokok', cell_text_format_top_left_right)
        worksheet.merge_range('D9:D10', 'Upah Lembur/Jam', cell_text_format_top_left_right)

        karyawan = self.get_employee(obj.department_id.id, obj.date_from, obj.date_to)
        col = 1
        for kw in karyawan:
            hasil_lembur = []
            weeks_added = []
            NO.append(no)
            no += 1
            row += 1
            gaji = self.get_wage(kw['id_karyawan'])
            for g in gaji:
                worksheet.write(row + 1, col + 1, float(g["gaji"]), money_format)
                worksheet.write(row + 1, col + 2, float(g["upah"]), money_format)
            for week in cal.monthdatescalendar(obj.date_from.year, obj.date_from.month):
                data = list(filter(lambda x: x >= obj.date_from and x <= obj.date_to, week))
                overtime = self.get_overtime(kw['id_karyawan'], data[0], data[-1], obj.project_id)
                print("<><><><><><><", data[0], data[-1])
                qty = []
                for s in overtime:
                    hasil_lembur.append(s['hasil'])
                    qty.append(s['jumlah'])
                    minus = list_week[0] if list_week[0] < list_week[1] else list_week[1]
                    for l in list_week:
                        print("masuk ga si")
                        if s['tanggal'].isocalendar()[1] == l:
                            weeks_added.append(l)
                            worksheet.write(row + 1,(l + 4) - minus, sum(qty), text_style)
                            # worksheet.write(row + 1, l - (7 + len(list_week)), sum(qty), text_style)

                        # if not add and l != list_week[0]:
                        elif s['tanggal'].isocalendar()[1] != l and l not in weeks_added:
                            worksheet.write(row + 1,(l + 4) - minus, '-', text_style)
                            print("<><><><><><><><><>", (l + 4) - minus)
                            # worksheet.write(row + 1, l - (7 + len(list_week)), '-', text_style)

            worksheet.write(row + 1, col, kw['karyawan'], text_style)
            worksheet.write_formula(row + 1, col + len(list_week) + 3, '=SUM(%s)' % (xl_range(row + 1, 4, row + 1, col + len(list_week) + 2)), text_style)
            worksheet.write(row + 1, col + len(list_week) + 4, sum(hasil_lembur), money_format)
            # worksheet.write_formula(row + 1, col + len(list_week) + 4, '=(%s*%s)' % (xl_rowcol_to_cell(row + 1, 3), xl_rowcol_to_cell(row + 1, col + len(list_week) + 3)), money_format)

        worksheet.write_column('A11', NO, text_style)
        worksheet.merge_range(row + 2, 0, row + 2, col + len(list_week) + 2, 'Total', text_bold_style)
        worksheet.write_formula(row + 2, col + len(list_week) + 3, '=SUM(%s)' % (xl_range(10, col + len(list_week) + 3, row + 1, col + len(list_week) + 3)), text_bold_style)
        worksheet.write_formula(row + 2, col + len(list_week) + 4, '=SUM(%s)' % (xl_range(10, col + len(list_week) + 4, row + 1, col + len(list_week) + 4)), money_bold_format)

    # def get_employee(self, divisi, date_from, date_to):
    #     query = """ SELECT he.name as karyawan, he.id as id_karyawan
    #     FROM hr_overtime ho
    #     left join hr_employee he on he.id = ho.employee_id
    #     WHERE he.department_id  = %s AND ho.date_overtime BETWEEN '%s' AND '%s' 
    #     group by karyawan,id_karyawan""" % (divisi, date_from, date_to)
    #     self._cr.execute(query)
    #     return self._cr.dictfetchall()

    def get_employee(self, divisi, date_from, date_to):
        q_divisi = ""
        if divisi:
            q_divisi = "he.department_id = %s and" % (divisi)
        query = """ SELECT he.name as karyawan, he.id as id_karyawan
        FROM hr_overtime ho
        left join hr_employee he on he.id = ho.employee_id
        WHERE %s (ho.state = 'done' or ho.state = 'paid') and ho.date_overtime BETWEEN '%s' AND '%s' 
        group by karyawan,id_karyawan""" % (q_divisi, date_from, date_to)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    def get_wage(self, employee):
        query = """select hc.wage as gaji, coalesce(round(hc.wage/173),0) as upah
        from hr_contract hc
        left join hr_employee he on he.id = hc.employee_id
        where (hc.state = 'open' or hc.state = 'pending') and he.id = %s
        order by hc.id desc limit 1 """ % (employee)
        self._cr.execute(query)
        return self._cr.dictfetchall()

    # def get_overtime(self, employee, first_date, last_date, project_id):
    #     project = ""
    #     if project_id:
    #         project = "ho.project_id = %s and" % (project_id.id)
    #     query = """ select ho.date_overtime as tanggal, he.name as nama, sum(ho.durasi_progresif) as jumlah
    #     from hr_overtime ho
    #     left join hr_employee he on he.id = ho.employee_id
    #     where %s he.id = %s and (ho.state = 'done' or ho.state = 'paid') and ho.date_overtime >= '%s' and ho.date_overtime <= '%s'
    #     group by tanggal, he.name """ % (project,employee, first_date, last_date)
    #     self._cr.execute(query)
    #     return self._cr.dictfetchall()

    def get_overtime(self, employee, first_date, last_date, project_id):
        project = ""
        if project_id:
            project = "ho.project_id = %s and" % (project_id.id)
        query = """ select ho.date_overtime as tanggal, he.name as nama, sum(ho.durasi_progresif) as jumlah, sum(ho.lembur) as hasil
        from hr_overtime ho
        left join hr_employee he on he.id = ho.employee_id
        where %s he.id = %s and (ho.state = 'done' or ho.state = 'paid') and ho.date_overtime >= '%s' and ho.date_overtime <= '%s'
        group by tanggal, he.name """ % (project,employee, first_date, last_date)
        self._cr.execute(query)
        return self._cr.dictfetchall()
