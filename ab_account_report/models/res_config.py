# -*- coding: utf-8 -*-

from odoo import models, fields, api

# class ResConfigSettings(models.TransientModel):	
#     _inherit = 'res.config.settings'	

#     account_balance_sheet = fields.Many2one(comodel_name='account.account',string='Account Balance Sheet', related='company_id.account_balance_sheet')
#     account_profit_loss = fields.Many2one(comodel_name='account.account', string='Account Profit Loss', related='company_id.account_profit_loss')
#     account_re_id = fields.Many2one(comodel_name='account.account', string='Account Retained Earning', related='company_id.account_re_id')

class ResCompany(models.Model):
    _inherit = 'res.company'

    account_balance_sheet = fields.Many2one(comodel_name='account.account',string='Account Balance Sheet')
    account_profit_loss = fields.Many2one(comodel_name='account.account', string='Account Profit Loss')
    account_re_id = fields.Many2one(comodel_name='account.account', string='Account Retained Earning')
    account_equity_id = fields.Many2one('account.account', string='Account Equity')
    init_start_date = fields.Date(string='Init Start Date')
    account_income_id = fields.Many2one('account.account', string="Account Root Operating Revenue")
    account_cogs_id = fields.Many2one('account.account', string="Account Root COGS")
    account_expense_id = fields.Many2one('account.account', string="Account Root Operating Expenses")
    account_other_id = fields.Many2one('account.account', string="Account Root Other Income & Expenses")
    type_reverse_ids = fields.Many2many('account.account.type', relation='account_type_reverses', string="Account Type Reverse")

class FinancialReportSettings(models.TransientModel):
    _name = 'financial.report.settings'
    _inherit = 'res.config.settings'

    account_balance_sheet = fields.Many2one(string='Account Balance Sheet', related='company_id.account_balance_sheet', readonly=False)
    account_profit_loss = fields.Many2one(string='Account Profit Loss', related='company_id.account_profit_loss', readonly=False)
    account_re_id = fields.Many2one(related='company_id.account_re_id', string='Account Retained Earning', readonly=False)
    account_equity_id = fields.Many2one('account.account', related='company_id.account_equity_id', string='Account Equity',  readonly=False)
    init_start_date = fields.Date(string='Init Start Date', related="company_id.init_start_date",  readonly=False)
    account_income_id = fields.Many2one(string="Account Root Operating Revenue", related='company_id.account_income_id', readonly=False)
    account_cogs_id = fields.Many2one(string="Account Root COGS", related='company_id.account_cogs_id', readonly=False)
    account_expense_id = fields.Many2one(string="Account Root Operating Expenses", related='company_id.account_expense_id', readonly=False)
    account_other_id = fields.Many2one(string="Account Root Other Income & Expenses", related='company_id.account_other_id', readonly=False)
    type_reverse_ids = fields.Many2many(related="company_id.type_reverse_ids", relation='account_account_reverses', string="Account Type Reverse", readonly=False)

