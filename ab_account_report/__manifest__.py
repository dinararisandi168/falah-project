{
    'name': "Report Accounting",
    'summary': """
        Report Accounting""",

    'description': """
        Report Accounting
    """,
    'author': "PT. ISMATA NUSANTARA ABADI",
    'website': "http://www.ismata.co.id",

    'category': 'Uncategorized',
    'version': '0.1',
    'depends': ['base', 'report_xlsx', 'account', 'account_parent', "ab_role"],
    'data': [
        'views/res_config.xml',
        'wizard/balance_sheet_views.xml',
        'report/report_account.xml',
        'wizard/profit_loss_views.xml',
    ],
    'demo': [
        'demo/demo.xml',
    ],
}