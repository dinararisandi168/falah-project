from odoo import _, api, fields, models
from datetime import datetime
from calendar import monthrange

class AccountReportWizard(models.TransientModel):
    _name = 'account.report.wizard'
    _description = 'Account Report Wizard'
    
    company_id = fields.Many2one('res.company','Company', required=True, default=lambda self: self.env.user.company_id.id)
    type = fields.Selection([('gl_detail','General Ledger Detail'),('neraca_standard', 'Balance Sheet'),('neraca_saldo','Neraca Saldo')],'Tipe Report')
    date_option = fields.Selection([('today','Hari Ini'),('last_month','Bulan Terakhir'),('last_tri','Triwulan Terakhir'),('last_year','Tahun Terakhir'),('custom','Pilih Tanggal')],'Date Option')
    date_start = fields.Date('Date Start', default=lambda self: self.env.user.company_id.init_start_date)
    date_stop = fields.Date('Date Stop')
    show_debit_credit = fields.Boolean('Show Debit Credit')
    posted = fields.Boolean('Posted Entries', default=True)
    display_zero = fields.Boolean(string='Display Zero Account')
    
    def get_date_option(self, date_option):
        res = {}
        last_quarter = {
                    '01':'Quarter #3','02':'Quarter #3','03':'Quarter #3','04':'Quarter #3',
                    '05':'Quarter #1','06':'Quarter #1','07':'Quarter #1','08':'Quarter #1',
                    '09':'Quarter #2','10':'Quarter #2','11':'Quarter #2','12':'Quarter #2',
                  }

        month_last_quarter = {
                    '01':'12','02':'12','03':'12','04':'12',
                    '05':'04','06':'04','07':'04','08':'04',
                    '09':'08','10':'08','11':'08','12':'08',
                    }

        last_tri = {
                    '01':'Triwulan #4','02':'Triwulan #4','03':'Triwulan #4',
                    '04':'Triwulan #1','05':'Triwulan #1','06':'Triwulan #1',
                    '07':'Triwulan #2','08':'Triwulan #2','09':'Triwulan #2',
                    '10':'Triwulan #3','11':'Triwulan #3','12':'Triwulan #3',
                  }

        month_last_tri = {
                    '01':'12','02':'12','03':'12',
                    '04':'03','05':'03','06':'03',
                    '07':'06','08':'06','09':'06',
                    '10':'09','11':'09','12':'09',
                    }


        if date_option == 'today':
            res['view'] = 'As of '+str(datetime.now().date().strftime('%d %B %Y'))
            res['value'] = str(datetime.now().date().strftime('%Y-%m-%d'))
        elif date_option == 'last_month':
            bulan = str(datetime.now().date().strftime('%m-%Y'))
            if bulan.split('-')[0] == '01':
                bulan_string = '12-'+str(int(bulan.split('-')[1])-1)
            else:
                bulan_string = str(int(bulan.split('-')[0]) - 1).zfill(2)+'-'+bulan.split('-')[1]
            res['view'] = str(datetime.strptime(bulan_string, '%m-%Y').strftime('%B %Y'))
            res['value'] = str(datetime.strptime(bulan_string, '%m-%Y').strftime('%Y-%m-'))+str(monthrange(int(bulan_string.split('-')[1]),int(bulan_string.split('-')[0]))[1]).zfill(2)
        elif date_option == 'last_quarter':
            if str(datetime.now().date().strftime('%m')) in ('01','02','03','04'):
                tahun = int(str(datetime.now().date().strftime('%Y'))) - 1
            else:
                tahun = int(str(datetime.now().date().strftime('%Y'))) 
            res['view'] = last_quarter[str(datetime.now().date().strftime('%m'))]+' '+str(tahun)
            res['value'] = str(tahun)+'-'+month_last_quarter[str(datetime.now().date().strftime('%m'))]+'-'+str(monthrange(tahun,int(month_last_quarter[str(datetime.now().date().strftime('%m'))]))[1]).zfill(2)
        elif date_option == 'last_tri':
            if str(datetime.now().date().strftime('%m')) in ('01','02','03'):
                tahun = int(str(datetime.now().date().strftime('%Y'))) - 1
            else:
                tahun = int(str(datetime.now().date().strftime('%Y'))) 
            res['view'] = last_tri[str(datetime.now().date().strftime('%m'))]+' '+str(tahun)
            res['value'] = str(tahun)+'-'+month_last_tri[str(datetime.now().date().strftime('%m'))]+'-'+str(monthrange(tahun,int(month_last_tri[str(datetime.now().date().strftime('%m'))]))[1]).zfill(2)
        elif date_option == 'last_year':
            res['view'] = str(int(str(datetime.now().date().strftime('%Y'))) - 1)
            res['value'] = str(int(str(datetime.now().date().strftime('%Y'))) - 1)+'-12-31'
        else:
            res['view'] = 'As of '+ str(datetime.strptime(str(self.date_stop), '%Y-%m-%d').strftime('%d %B %Y'))
            res['value'] = self.date_stop
        return res

    @api.multi
    def download_xlsx_report(self):
        template_report = 'ab_account_report.report_account_xlsx'
        return self.env.ref(template_report).report_action(self)
