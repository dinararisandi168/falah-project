# -*- coding: utf-8 -*-
{
    "name": "Minutes of Meetings",
    "summary": """
       ---""",
    "description": """
       Digunakan untuk manajement meetings
    """,
    "author": "PT. ISMATA NUSANTARA ABADI - 087881071515",
    "website": "www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "calendar","hr","purchase","account", "web_widget_digitized_signature", "ab_attachment","project","ab_role"],
    # always loaded
    "data": [
        "security/ir.model.access.csv",
        "security/security.xml",
        "views/views.xml",
        "report/report.xml",
        "views/templates.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}