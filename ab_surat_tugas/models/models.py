# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.ab_role.models.models import _get_uid_from_group
from lxml import etree


class SuratTugas(models.Model):
    _name = "surat.tugas"
    _inherit = "mail.thread"
    _description = "Surat Tugas"

    name = fields.Char("Reference", default="/", readonly=True, track_visibility="onchange")
    employee_line = fields.One2many("surat.tugas.line", "surat_tugas_id", string="Test", track_visibility="onchange")
    attachment_ids = fields.Many2many(comodel_name="ir.attachment", string="Attachment List", track_visibility="onchange")
    catatan_revisi = fields.Text(string="Revisions", readonly="1", track_visibility="onchange")
    catatan_batal = fields.Text(string="Alasan Batal", readonly="1", track_visibility="onchange")
    jenis_tugas = fields.Char(string="Jenis Tugas", track_visibility="onchange")
    proyek_id = fields.Many2one("project.project", string="Proyek", track_visibility="onchange")
    lokasi = fields.Char(string="Tempat / Lokasi", track_visibility="onchange")
    hari_tgl = fields.Date(string="Hari & Tanggal", track_visibility="onchange", default=fields.Date.context_today)
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("done", "Done"),
            ("cancel", "Cancel"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
    )

    @api.model
    def fields_view_get(self, view_id=None, view_type="form", toolbar=False, submenu=False):
        result = super(SuratTugas, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(result["arch"])
        if view_type == "tree" or view_type == "form" or view_type == "kanban":
            if self.user_has_groups("ab_role.group_project_admin") or self.user_has_groups("ab_role.group_hod_hod"):
                for tree in doc.xpath("//tree"):
                    tree.set("create", "true")
                for form in doc.xpath("//form"):
                    form.set("create", "true")
                    form.set("edit", "true")
            else:
                for tree in doc.xpath("//tree"):
                    tree.set("create", "false")
                    tree.set("delete", "false")
                for form in doc.xpath("//form"):
                    form.set("create", "false")
                    form.set("edit", "false")
                    form.set("delete", "false")
                for form in doc.xpath("//kanban"):
                    form.set("create", "false")
                    form.set("delete", "false")

        result["arch"] = etree.tostring(doc, encoding="unicode")
        return result

    @api.multi
    def cetak_surat_tugas(self):
        return self.env.ref("ab_surat_tugas.cetak_surat_tugas").report_action(self)

    @api.multi
    def action_done(self):
        self.write({"state": "done"})

    def action_submit(self):
        user_ids = _get_uid_from_group(
            self, self.env.ref("ab_role.group_hod_human_resource").id
        )
        partner_ids = [
            self.env["res.users"].browse(uid).partner_id.id for uid in user_ids
        ]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\nDear User, This is outstanding transaction that need your action to verify or approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({'state': 'submit'})

    @api.model
    def create(self, vals):
        check = super(SuratTugas, self).create(vals)
        check.name = self.env["ir.sequence"].next_by_code("surat.tugas")

        return check


class SuratTugasLine(models.Model):
    _name = "surat.tugas.line"
    _inherit = "mail.thread"

    surat_tugas_id = fields.Many2one("surat.tugas", string="Surat Tugas")
    employee_id = fields.Many2one("hr.employee", string="Nama")
    jabatan_id = fields.Many2one("hr.job", string="Jabatan", related="employee_id.job_id")
    # job_id = fields.Many2one("hr.job", string="Jabatan", related="employee_id.job_id")
    proyek_id = fields.Many2one("project.project", string="Proyek")


class AttachmentSuratTugas(models.Model):
    _inherit = "ir.attachment"

    name = fields.Char()


class WizardSuratTugas(models.TransientModel):
    _name = "wizard.surat.tugas"
    _description = "Wizard Revisi"

    text_revisi = fields.Text(string="Catatan Revisi")
    # text_cancel = fields.Text(string="Catatan Batal")

    @api.multi
    def revisi_state_draft(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            project = self.env["surat.tugas"].browse(active_id)
            if project.write({"state": "draft"}):
                project.catatan_revisi = self.text_revisi
            else:
                project.catatan_revisi = self.text_revisi


class WizardCancel(models.TransientModel):
    _name = "wizard.cancel"

    text_cancel = fields.Text(string="Catatan Batal")

    @api.multi
    def reason_cancel(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            surgas = self.env["surat.tugas"].browse(active_id)
            if surgas.write({"state": "cancel"}):
                surgas.catatan_batal = self.text_cancel
            else:
                surgas.catatan_batal = self.text_cancel
