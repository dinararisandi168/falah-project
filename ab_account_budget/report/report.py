
from odoo import api, fields, models
import xlsxwriter
import io
import base64
from io import BytesIO


class ReportBudget(models.TransientModel):
    _name = 'report.budget'
    _description = 'New Description'

    crossovered_budget_ids = fields.Many2many(comodel_name='crossovered.budget', string='Budgets')
    
    file = fields.Binary(string='File')
    name = fields.Char(string='Filename')
    
    def generate(self):
        judul = 'Budget.xlsx'
        
        sql_budget = """
        
        select name,id from crossovered_budget 
        where id in %s
        
        """
        
        params = [tuple(self.crossovered_budget_ids.ids) or (None,)]
        self._cr.execute(sql_budget,tuple(params))
        budget_lines = self._cr.dictfetchall()
        
        sql_budget_position = """
        
        select abp.name,cbl.id as budget_id,cbl.planned_amount,cbl.crossovered_budget_id,
        aaa.name as analytic
        from crossovered_budget cb 
        left join crossovered_budget_lines cbl on (cbl.crossovered_budget_id = cb.id)
        left join account_budget_post abp on (cbl.general_budget_id = abp.id)
        left join account_analytic_account aaa on (cbl.analytic_account_id = aaa.id)
        where cb.id in %s
                
        """
        params = [tuple(self.crossovered_budget_ids.ids) or (None,)]
        self._cr.execute(sql_budget_position,tuple(params))
        budget_position_lines = self._cr.dictfetchall()
        
        sql_move_line = """
        
        select cbl.id as budget_id,aa.name as account ,am.name,
        l.ref, aaa.name as analytic,
        COALESCE(l.debit,0) AS debit,
        COALESCE(l.credit,0) AS credit,
        COALESCE(l.debit - l.credit,0) AS balance,
        cbl.date_from,
        l.date
        
        FROM account_move_line l
        left join account_budget_rel abr on (abr.account_id = l.account_id)
        left join account_account aa on (l.account_id = aa.id)
        left join account_budget_post abp on (abr.budget_id = abp.id)
        left join crossovered_budget_lines cbl on (cbl.general_budget_id = abp.id)
        left join account_move am on (l.move_id = am.id)
        left join account_analytic_account aaa on (l.analytic_account_id = cbl.analytic_account_id)
        where 
        
        l.date >= cbl.date_from and l.date <= cbl.date_to and l.analytic_account_id = cbl.analytic_account_id 
        or
        l.date >= cbl.date_from and l.date <= cbl.date_to and cbl.analytic_account_id is null and l.analytic_account_id is null
        
        order by abp.name
        
        """
        
        self._cr.execute(sql_move_line)
        move_lines = self._cr.dictfetchall()
        
        file_data = io.BytesIO()
        workbook = xlsxwriter.Workbook(file_data)
        sheet = workbook.add_worksheet('Budget')

        style = workbook.add_format({'left': 1, 'bottom':1,'top': 1,'right':1,'align':'center','num_format':'#,##0.00'})
        title = workbook.add_format(
            {
                "bold": True,
                "align": "center",
                "font_size": 11,
                "border": False,
                "font": "Arial",
            }
        )
        
        header = ['Budget','Budget Position','Account','Project','Journal','Ref','Debit','Credit','Balance','Actual Date','Budget Balance']
        col = 1;row = 5

        sheet.merge_range(
            0,
            6,
            0,
            3,
            self.env.user.company_id.name,
            title,
        )

        sheet.merge_range(
            2, 6, 2, 3, "As of %s" % (fields.Date.today().strftime("%d %b %Y")), title
        )

        col_dict = {}

        for rd in header:            
            sheet.write(row, col, rd, style)
            col_dict[col] = len(str(rd)) + 5
            sheet.set_column(col, col,  col_dict[col])
            col+=1
        row += 1

        sheet.freeze_panes(6, 0)
        user = self.env["res.users"].browse(self.env.uid)
        partner = self.env["res.partner"].browse(user.company_id.id)
        logo = BytesIO(base64.b64decode(partner.image))
        sheet.insert_image(
            0, 0, "logo.png", {"image_data": logo, "x_scale": 0.35, "y_scale": 0.35}
        )
        
        
        for budget in budget_lines:
            isi = [budget['name'],' ','','','','','','','','','']
            col = 1
            for rd in isi:
                sheet.write(row, col, rd, style)
                col+=1
            row += 1
            
                
            for budget_position in budget_position_lines:
                if budget_position['crossovered_budget_id'] == budget['id']:
                    isi = [' ',budget_position['name'],'',budget_position['analytic'],'','','','','','', budget_position['planned_amount']]
                    col = 1
                    for rd in isi:
                        sheet.write(row, col, rd, style)
                        if len(str(rd)) > 5:
                            col_dict[col] = len(str(rd)) + 5
                            sheet.set_column(col, col,  col_dict[col])
                        col+=1
                    row += 1
                    
                    for move in move_lines:
                        if move['budget_id'] == budget_position['budget_id']:
                            budget_position['planned_amount'] += move['balance']
                            isi = ['','',move['account'],'',move['name'],move['ref'],move['debit'],move['credit'],move['balance'],str(move['date']),budget_position['planned_amount']]                            
                            col = 1
                            for rd in isi:
                                sheet.write(row, col, rd, style)
                                if len(str(rd)) > 1:
                                    col_dict[col] = len(str(rd)) + 5
                                    sheet.set_column(col, col,  col_dict[col])
                                col+=1
                            row += 1
                    

        workbook.close()        
        out = base64.encodestring(file_data.getvalue())
        self.write({'file': out, 'name': judul})
        
        
        return {
            'name': 'Report Budget',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': self._name,
            'views': False,
            'res_id': self.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }