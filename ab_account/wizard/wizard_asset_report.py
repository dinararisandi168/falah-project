
import xlsxwriter
from io import BytesIO
import base64
from odoo import models, fields, api, _
from odoo.modules.module import get_resource_path
import math

class WizardAssetReport(models.TransientModel):
    _name = 'wizard.asset.report'
    _description = 'Wizard Asset Report'

    tipe = fields.Selection(string='Tipe', selection=[('all', 'All')], default="all")
    user_id = fields.Many2one(comodel_name='res.users', string='Created By')
    partner_id = fields.Many2one('res.partner', string='Partner')
    name = fields.Char(string='File name')
    data_file = fields.Binary(string='Data File')

    def get_percentage_depreciation(self, asset_id):
        query = """ 

            SELECT (
                SELECT COUNT(*) from account_asset_depreciation_line as asd
                    WHERE asset_id = '{}' AND move_check = 't'
            ) as count_depre, (
                SELECT COUNT(id) from account_asset_depreciation_line
                    WHERE asset_id = '{}'
            ) as count_all

        """.format(asset_id, asset_id)

        self._cr.execute(query)
        fetch = self._cr.dictfetchall()

        result = ''
        if fetch:
            if fetch[0]['count_depre'] != 0 or fetch[0]['count_all'] != 0:
                percentage = 100 - (fetch[0]['count_depre'] / fetch[0]['count_all'] * 100)
                temp = math.modf(percentage)
                if temp[0] > 0:
                    result = '{:.2f}'.format(percentage)
                else:
                    result = percentage
                
        return result

    def get_record_name(self, object, id):
        res = self.env[object].browse(id).name if id else ''
        return res

    def act_to_report(self):
        for ob in self:
            file_data = BytesIO()
            workbook = xlsxwriter.Workbook(file_data)
            sheet = workbook.add_worksheet('Fixed Asset')
            judul = 'Fix Asset.xlsx'

            company_style = workbook.add_format({'bold': True, 'align': 'center', 'size': 14})
            title_style = workbook.add_format({'bold': True, 'align': 'center', 'size': 16, 'color': 'red'})
            left_col_style = workbook.add_format({'align': 'left', 'size': 12, 'border' : 1})
            right_col_style = workbook.add_format({'align': 'right'})
            header_style = workbook.add_format({'align': 'center', 'size': 12, 'bold': True, 'color': 'blue'})
            header_style.set_bottom(1)

            header = {
                'code': 'Asset Code',                                   # Nomor Register
                'name': 'Asset Name',                                   # Asset Name
                'category_id': 'Asset Type Name',                       # Asset Category
                'category_id.account_asset_id': 'Asset Account No.',    # Asset Type
                'value': 'Asset Cost',                                  # Gross Value
                'date': 'Purchase',                                     # Date
                'category_id.method_period': 'Estimated',               # One Entry Every
                'depr': '% Depr',                                       # depresiation
                'Straight Line Method': 'Depreciation'                  # Still hardcode
            }

            query = """ SELECT * from account_asset_asset as ase """
            self._cr.execute(query)
            fetch_date = self._cr.dictfetchall()

            sheet.merge_range(0, 1, 0, 19, ob.env.user.company_id.name, company_style)
            sheet.merge_range(1, 1, 1, 19, 'Fixed Asset List', title_style)
            
            col = 1
            row = 4

            for i in header.values():
                sheet.write(row, col, i, header_style)
                col += 2

            sheet.set_row(0, 25)
            sheet.set_row(1, 25)
            
            for i in range(20):
                if i % 2 == 0:
                    sheet.set_column(i, i, 3)
                else:
                    if i == 3:
                        sheet.set_column(i, i, 40)
                    else:
                        sheet.set_column(i, i, 17)
                
            col = 0
            for i in fetch_date:
                row += 1
                category = self.get_record_name('account.asset.category', i['category_id'])
                depreciation = self.get_percentage_depreciation(i['id'])

                sheet.write(row, col + 1, i['code'])                                # Nomor Register
                sheet.write(row, col + 3, i['name'])                                # Asset Name
                sheet.write(row, col + 5, category)                                 # Asset Category
                sheet.write(row, col + 7, category)                                 # Asset Type
                sheet.write(row, col + 9, i['value'])                               # Gross Value
                sheet.write(row, col + 11, str(i['date']))                          # Date
                sheet.write(row, col + 13, category)                                # One Entry Every
                sheet.write(row, col + 15, depreciation, right_col_style)                            # No field
                sheet.write(row, col + 17, 'Straight Line Method')                  # Still hardcode

            workbook.close()        
            out = base64.encodestring(file_data.getvalue())
            self.write({'data_file': out, 'name': judul})
            
            return {
                'name': 'Fixed Asset',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': self._name,
                'views': False,
                'res_id': self.id,
                'type': 'ir.actions.act_window',
                'target': 'new',
            }
    
        