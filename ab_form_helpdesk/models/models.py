# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree
from odoo.exceptions import UserError
from odoo.addons.ab_role.models.models import _get_uid_from_group


class HelpdeskliteTicket(models.Model):
    _inherit = "helpdesk_lite.ticket"
    _description = "Helpdesk Ticket"

    phone_number = fields.Char(related="partner_id.mobile", string="Phone Number")
    issue_description = fields.Text("Issue Description")
    root_cause = fields.Text("Root Cause")
    solution = fields.Text("Solution")
    organization_unit_id = fields.Many2one("hr.department")
    attacment_list_line = fields.One2many("ir.attachment", "helpdesk_id")
    attachment_id = fields.Many2one("ir.attachment")
    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List')
    reference_id = fields.Many2one("project.project", "Project Reference")
    company = fields.Char(string="Company", related="partner_id.parent_id.name", track_visibility="onchange")
    is_can_read = fields.Boolean(compute='_compute_is_can_read', string='')
    user_ids = fields.Many2many('res.users', string='team')
    is_assigned = fields.Boolean(string='', copy=False)
    is_solved = fields.Boolean(string='', copy=False)
    is_cancel = fields.Boolean(string='', copy=False)

    def takeit(self):
        no = 0
        if self.user_id:
            self.message_subscribe([self.user_id.partner_id.id])
            no = self.stage_id.id + 1
            self.is_assigned = True
        self.write({'stage_id': no})

    def solved(self):
        no = 0
        no = self.stage_id.id + 1
        self.is_solved = True

        self.write({'stage_id': no})
        self.update({"date_done": fields.Datetime.now()})

    def cancel(self):
        cancel = self.env['helpdesk_lite.stage'].sudo().search([('fold', '=', True)], limit=1)
        self.is_cancel = True
        self.write({'stage_id': cancel.id})

    @api.onchange('team_id')
    def _onchange_team_id(self):
        res = []
        res = self.mapped("team_id.member_ids.id")
        if self.team_id.user_id.id:
            res.append(self.team_id.user_id.id)
        self.user_ids = [(6, 0, res)]
        return{
            'domain': {'user_id': [('id', 'in', res)]}
        }

    @api.depends('stage_id')
    def _compute_is_can_read(self):
        for i in self:
            i.is_can_read = i.team_id.user_id.id == self.env.user.id

    @api.multi
    def write(self, vals):
        # stage change: update date_last_stage_update
        if "stage_id" in vals:
            if vals['stage_id'] != self.stage_id and not self.is_can_read:
                raise UserError(("Selain Project Leader tidak bisa memindahkan Stage!"))
            if "kanban_state" not in vals:
                vals["kanban_state"] = "normal"
            stage = self.env["helpdesk_lite.stage"].browse(vals["stage_id"])
            if stage.last:
                vals.update({"date_done": fields.Datetime.now()})
            else:
                vals.update({"date_done": False})

        return super(HelpdeskliteTicket, self).write(vals)


class AttachmentLine(models.Model):
    _inherit = "ir.attachment"
    _description = "All Atachment"

    helpdesk_id = fields.Many2one("helpdesk_lite.ticket")
    is_helpdesk = fields.Boolean(string="helpdesk")
