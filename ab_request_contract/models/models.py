# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.addons.ab_attachment.models.models import _send_message
from odoo.addons.ab_role.models.models import _get_uid_from_group


class HrRequestContract(models.Model):
    _name = "hr.request.contract"
    _inherit = "mail.thread"
    _description = "Perpanjangan Kontrak"

    name = fields.Char(string="Reference", readonly=True, default="/", track_visibility="onchange")
    employee_id = fields.Many2one("hr.employee", track_visibility="onchange", string="Nama Karyawan", domain=[('contract_ids', '!=', False), ('contract_ids.state', '=', 'pending')])
    date = fields.Date(string="Tanggal", default=fields.Date.context_today, track_visibility="onchange")

    no_contract = fields.Char(string="Nomor Kontrak", readonly=True, track_visibility="onchange")
    nik = fields.Char(string="NIK", readonly=True, track_visibility="onchange")

    start_date = fields.Date(string='From', readonly=True, track_visibility="onchange")
    end_date = fields.Date(string='To', readonly=True, track_visibility="onchange")

    request_id = fields.Many2one("hr.department", string="Pengajuan Oleh", track_visibility="onchange")
    division_id = fields.Many2one("hr.department", string="Division")

    request_line = fields.One2many(
        "request.contract.line", "request_contract_id", string="TEST"
    )
    directleader_id = fields.Many2one("hr.employee", string="Atasan Langsung")
    direct_leader = fields.Selection(
        [
            ("kartap", "Karyawan Tetap"),
            ("kontrak", "Perpanjang Kontrak"),
            ("tdk", "Tidak Perpanjang Kontrak"),
        ],
        string="Status Pegawai",
        track_visibility="onchange",
    )
    supporting_reason = fields.Text(string="Alasan Pendukung", track_visibility="onchange")
    undirectleader_id = fields.Many2one("hr.employee", string="Atasan Tidak Langsung", track_visibility="onchange")
    undirect_leader = fields.Selection(
        [
            ("kartap", "Karyawan Tetap"),
            ("kontrak", "Perpanjang Kontrak"),
            ("tdk", "Tidak Perpanjang Kontrak"),
        ],
        string="Status Pegawai",
        track_visibility="onchange",
    )
    supporting_reasons = fields.Text(string="Alasan Pendukung", track_visibility="onchange")
    hrd_id = fields.Many2one("hr.employee", string="HRD", track_visibility="onchange")
    hrd_department = fields.Selection(
        [
            ("kartap", "Karyawan Tetap"),
            ("kontrak", "Perpanjang Kontrak"),
            ("tdk", "Tidak Perpanjang Kontrak"),
        ],
        string="Status Pegawai",
        track_visibility="onchange",
    )
    supporting_reasonss = fields.Text(string="Alasan Pendukung", track_visibility="onchange")
    ceo_note = fields.Text(string="Catatan CEO", track_visibility="onchange")
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("submit_gen", "Submitted"),
            ("review1", "Review LOD"),
            ("review2", "Review HOD"),
            ("done", "CEO Approved"),
            ("cancel", "Cancel"),
        ],
        string="Status",
        readonly=True,
        copy=False,
        default="draft",
        track_visibility="onchange",
    )

    state_a = fields.Selection(related='state', store=False, track_visibility=False)
    state_b = fields.Selection(related='state', store=False, track_visibility=False)

    ceo = fields.Char(string='CEO', track_visibility="onchange")

    catatan_revisi = fields.Text(
        string="Revisions", readonly="1", track_visibility="onchange"
    )

    is_header = fields.Boolean(compute='_compute_read_only', string='IS HRD')
    is_middle = fields.Boolean(compute='_compute_read_only', string='IS LOD')
    is_middlee = fields.Boolean(compute='_compute_read_only', string='IS HOD')
    is_bottom = fields.Boolean(compute='_compute_read_only', string='IS CEO')

    @api.onchange('employee_id')
    def _onchange_employee_id(self):

        self.start_date = self.employee_id.contract_id.date_start
        self.end_date = self.employee_id.contract_id.date_end

        self.no_contract = self.employee_id.contract_id.name
        self.nik = self.employee_id.nik

        self.directleader_id = self.employee_id.parent_id
        self.undirectleader_id = self.directleader_id.parent_id
        self.hrd_id = self.env['hr.employee'].search([('department_id.name', 'ilike', 'Human')], limit=1)

        self.request_id = self.employee_id.department_id
        self.division_id = self.employee_id.department_id.parent_id


        # self.update({

        #     "start_date": self.employee_id.contract_id.date_start,
        #     "end_date": self.employee_id.contract_id.date_end,
        #     "no_contract": self.employee_id.contract_id.name,
        #     "nik": self.employee_id.nik,

        # })

    # @api.depends("state")
    def _compute_read_only(self):
        if self.user_has_groups('ab_role.group_hod_human_resource'):
            self.is_header = True
        if self.user_has_groups('ab_role.group_lod_lod'):
            self.is_middle = True
        if self.user_has_groups('ab_role.group_hod_hod'):
            self.is_middlee = True
            self.is_middle = True
        if self.user_has_groups('ab_role.group_direktur'):
            self.is_bottom = True
            self.is_middlee = True
        # else:
        #     self.is_header = False
        #     self.is_middle = False
        #     self.is_middle = False

    @api.multi
    def cetak_perpanjangan_kontrak(self):
        return self.env.ref("ab_request_contract.cetak_perpanjangan_kontrak").report_action(self)

    @api.multi
    def action_set_to_draft(self):
        self.write({"state": "draft"})

    @api.multi
    def action_confirm(self):
        self.update({'request_line': [(5, 0, 0)]})
        ppk = self.env['prestasi.kerja'].sudo().search([('employee_id', '=', self.employee_id.id), ('state', '=', 'done')], order='create_date desc', limit=1)
        query = """ with master_aspek_tp as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_tp_line
        group by prestasi_id
        ),

        master_aspek_nt as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_nt_line
        group by prestasi_id
        ),

        master_aspek_pribadi as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_pribadi_line
        group by prestasi_id
        ),

        master_aspek_kpm as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_kpm_line
        group by prestasi_id
        )

        select mat.nilai1+mat.nilai2 as aspek_tp, man.nilai1+man.nilai2 as aspek_nt, map.nilai1+map.nilai2 as aspek_pribadi, mak.nilai1+mak.nilai2 as aspek_kpm
        from master_aspek_tp mat
        left join master_aspek_nt man on man.prestasi_id = mat.prestasi_id
        left join master_aspek_pribadi map on map.prestasi_id = mat.prestasi_id
        left join master_aspek_kpm mak on mak.prestasi_id = mat.prestasi_id 
        where mat.prestasi_id = %s
        """ % (ppk.id)
        nilai = ['ASPEK TEKNIS PEKERJAAN', 'ASPEK NON TEKNIS', 'ASPEK KEPRIBADIAN', 'ASPEK KEPEMIMPINAN']

        def get_klasifikasi(bobot):
            if bobot >= 160:
                return "A"
            elif bobot >= 110:
                return "B"
            elif bobot >= 60:
                return "C"
            else:
                return "D"
        if self.employee_id and ppk:
            self._cr.execute(query)
            data = self.env.cr.dictfetchall()
            hasil_sum = []
            for x in data:
                inc = [
                    x["aspek_tp"],
                    x['aspek_nt'],
                    x['aspek_pribadi'],
                    x['aspek_kpm'],
                ]
            self.update({
                'request_line': [(0, 0, {'berdasarkan_kpi': m, 'bobot': n, 'klasifikasi': get_klasifikasi(n), 'request_contract_id': self.id}) for n, m in zip(inc, nilai)]

            })
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_lod_lod").id)
        # directleader_id = self.employee_id.user_id
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject='Perpanjanan Kontrak Pegawai' + ' - ' + self.name + ' - [Need Review]',
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.write({"state": "submit"})

    @api.multi
    def action_confirm2(self):
        self.update({'request_line': [(5, 0, 0)]})
        ppk = self.env['prestasi.kerja'].sudo().search([('employee_id', '=', self.employee_id.id), ('state', '=', 'done')], order='create_date desc', limit=1)
        query = """ with master_aspek_tp as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_tp_line
        group by prestasi_id
        ),

        master_aspek_nt as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_nt_line
        group by prestasi_id
        ),

        master_aspek_pribadi as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_pribadi_line
        group by prestasi_id
        ),

        master_aspek_kpm as (select prestasi_id,sum(nilai_atasan) as nilai1,sum(nilai_atasan1) as nilai2 
        from aspek_kpm_line
        group by prestasi_id
        )

        select mat.nilai1+mat.nilai2 as aspek_tp, man.nilai1+man.nilai2 as aspek_nt, map.nilai1+map.nilai2 as aspek_pribadi, mak.nilai1+mak.nilai2 as aspek_kpm
        from master_aspek_tp mat
        left join master_aspek_nt man on man.prestasi_id = mat.prestasi_id
        left join master_aspek_pribadi map on map.prestasi_id = mat.prestasi_id
        left join master_aspek_kpm mak on mak.prestasi_id = mat.prestasi_id 
        where mat.prestasi_id = %s
        """ % (ppk.id)
        nilai = ['ASPEK TEKNIS PEKERJAAN', 'ASPEK NON TEKNIS', 'ASPEK KEPRIBADIAN', 'ASPEK KEPEMIMPINAN']

        def get_klasifikasi(bobot):
            if bobot >= 160:
                return "A"
            elif bobot >= 110:
                return "B"
            elif bobot >= 60:
                return "C"
            else:
                return "D"
        if self.employee_id and ppk:
            self._cr.execute(query)
            data = self.env.cr.dictfetchall()
            hasil_sum = []
            for x in data:
                inc = [
                    x["aspek_tp"],
                    x['aspek_nt'],
                    x['aspek_pribadi'],
                    x['aspek_kpm'],
                ]
            self.update({
                'request_line': [(0, 0, {'berdasarkan_kpi': m, 'bobot': n, 'klasifikasi': get_klasifikasi(n), 'request_contract_id': self.id}) for n, m in zip(inc, nilai)]

            })
        if self.division_id:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_lod_lod").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                subject='Perpanjanan Kontrak Pegawai' + ' - ' + self.name + ' - [Need Review]',
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.env['send.email.notification'].send(self.id, 'hr.request.contract', 'submit', 'group', 'ab_role.group_lod_lod')
        else:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
            partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
            self.message_post(
                body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
                subject='Perpanjanan Kontrak Pegawai' + ' - ' + self.name + ' - [Need Review]',
                partner_ids=partner_ids,
                message_type="notification",
                subtype="mail.mt_comment",
            )
            self.env['send.email.notification'].send(self.id, 'hr.request.contract', 'submit', 'group', 'ab_role.group_hod_hod')
        self.write({"state": "submit_gen"})

    @api.multi
    def action_review_one(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_hod").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject='Perpanjanan Kontrak Pegawai' + ' - ' + self.name + ' - [Need Review]',
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'hr.request.contract', 'submit', 'group', 'ab_role.group_hod_hod')
        self.write({"state": "review1"})

    @api.multi
    def action_review_two(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to review. \nThanks",
            subject='Perpanjanan Kontrak Pegawai' + ' - ' + self.name + ' - [Need Review]',
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'hr.request.contract', 'submit', 'group', 'ab_role.group_direktur')
        self.write({"state": "review2"})

    @api.multi
    def action_done(self):
        self.ceo = self.env.user.name
        # contract = self.env['hr.contract']
        # contract.create({
        #     'name': self.nik,
        #     'employee_id': self.employee_id.id,

        # })
        self.write({"state": "done"})

    @api.multi
    def action_cancel(self):
        self.write({"state": "cancel"})

    @api.multi
    def unlink(self):
        for i in self:
            if i.state != "draft":
                raise UserError(
                    ("Form Perpanjangan Kontrak tidak bisa dihapus pada state %s !")
                    % (i.state)
                )
        return super(HrRequestContract, self).unlink()

    # untuk membuat outo save dari Reference
    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("hr.request.contract")
        return super(HrRequestContract, self).create(vals)

    @api.multi
    def create_contract(self):
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'hr.contract',
            'context': {'default_employee_id': self.employee_id.id}
        }


class RequestContractLine(models.Model):
    _name = "request.contract.line"

    request_contract_id = fields.Many2one(
        "hr.request.contract", string="Perpanjangan Kontrak"
    )
    berdasarkan_kpi = fields.Char(string="Berdasarkan KPI")
    klasifikasi = fields.Char(string="Klasifikasi")
    bobot = fields.Float(string="Bobot (%)")


class Contract(models.Model):
    _inherit = 'hr.contract'

    department_id = fields.Many2one('hr.department', string="Division")
    project_department_id = fields.Many2one("hr.project.department", string="Department")

    @api.onchange('employee_id')
    def _onchange_employee_id_x(self):
        if self.employee_id:
            self.project_department_id = self.employee_id.employeetags_id
