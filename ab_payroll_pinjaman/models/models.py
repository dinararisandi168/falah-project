# -*- coding: utf-8 -*-

from odoo import models, fields, api


class HrCicilan(models.Model):
    _inherit = "hr.cicilan"
    
    def _pinjaman_per(self):            
        if self.cicilan_line and self.angsuran:
            nilai = 0
            for x in self.cicilan_line:
                if x.payslip_id:
                    nilai += 1
            hasil = "%s/%s" % (nilai, self.angsuran)
            self.pinjaman = hasil
    
class CicilanLine(models.Model):
    _inherit = 'hr.cicilan.line'
    _order = "payslip_id asc"

    payslip_id = fields.Many2one('hr.payslip', string='Payslip')
    payslip_date = fields.Date('Payslip Date')
        
    @api.multi
    def write(self, vals):
        payslip_id = vals.get('payslip_id')        
        for x in self:
            total = len(x.env['hr.cicilan.line'].search([('cicilan_id', '=', x.cicilan_id.id), ('payslip_id', '=', payslip_id)]))
            if total == 1:
                return False
        return super(CicilanLine, self).write(vals)
    
    
class HrPayslip(models.Model):
    _inherit = 'hr.payslip'

    cicilan_line_ids = fields.One2many(comodel_name='hr.cicilan.line', inverse_name='payslip_id', string='Cicilan Line Ids')

    def to_draft(self):
        res = super(HrPayslip, self).to_draft()
        if self.cicilan_line_ids:
            for x in self.cicilan_line_ids:
                x.cicilan_id.write({'lunas': False})
                x.write({'payslip_id': False, 'payslip_date': False})
        return res

    @api.multi
    def action_payslip_done(self):
        res = super(HrPayslip, self).action_payslip_done()
        cline = self.env['hr.cicilan.line']
        ObjHrCicilan = self.env['hr.cicilan']
        data_cicilan = ObjHrCicilan.search([('employee_id', '=', self.employee_id.id),
                                            ('state', '=', 'done'),
                                            ('lunas', '=', False), ])

        def write_cline(p):
            for x in p.cicilan_id.cicilan_line:
                cline.browse(p.id).write({
                    'payslip_id': self.id,
                    'payslip_date': self.create_date,
                })

                if p.cicilan_id.value == sum([x.cicilan_value for x in p.cicilan_id.cicilan_line if x.payslip_id]):
                    ObjHrCicilan.browse(p.cicilan_id.id).write({'lunas': True})

        for pinjaman in data_cicilan:
            for p in pinjaman.cicilan_line:
                if not p.payslip_id:
                    write_cline(p)
                    break

        return res
