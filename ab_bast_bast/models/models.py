# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.ab_role.models.models import _get_uid_from_group
from datetime import datetime
from odoo.exceptions import UserError


class BastBast(models.Model):
    _name = "bast.bast"
    _inherit = "mail.thread"
    _description = "Form BAST"

    name = fields.Char(string="Reference", readonly=True, default="/", track_visibility="onchange")
    note = fields.Text(
        "", default="Berdasarkan surat Perjanjian Kontrak kerja nomor [NO KONTRAK] antara PIHAK PERTAMA dan PIHAK KEDUA tentang pekerjaan Proyek [JUDUL KONTRAK] dengan ketentuan yang telah diatur sesuai dalam surat Perjanjian Kontrak Kerja", track_visibility="onchange")
    the_first = fields.Char(string="Pihak Pertama", readonly="True", track_visibility="onchange")
    first_id = fields.Many2one("hr.employee", string="Employee", track_visibility="onchange")
    address1 = fields.Char(string="Alamat", related="first_id.company_id.street", track_visibility="onchange")
    position1 = fields.Char(string="Jabatan", related="first_id.job_id.name", track_visibility="onchange")
    date = fields.Date(string="Tanggal", track_visibility="onchange")
    the_second = fields.Char(string="Pihak Kedua", readonly="True", track_visibility="onchange")
    partner_id = fields.Many2one("res.partner", track_visibility="onchange")
    address2 = fields.Char(string="Alamat", related="partner_id.street", track_visibility="onchange")
    position2 = fields.Char(string="Jabatan", related="partner_id.function", track_visibility="onchange")
    company = fields.Char(string="Perusahaan", related="partner_id.parent_id.name", track_visibility="onchange")
    no_contract = fields.Char(string="No Kontrak", readonly=True, track_visibility="onchange", related="contract_reference_id.no_contract")
    contract_reference_id = fields.Many2one("menu.contract", string="Kontrak Reference", track_visibility="onchange")
    bast_line = fields.One2many("bast.line", "bast_id", track_visibility="onchange")
    project_id = fields.Many2one('project.project', string='Nama Project', track_visibility="onchange")
    title_contract = fields.Char("Judul Kontrak", readonly=True, track_visibility="onchange", related="contract_reference_id.title_contract")
    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('submit', 'Submitted'),
        ('review1', 'Reviewed PL'),
        ('review2', 'Reviewed PM'),
        ('done', 'Done'),
    ], default="draft", track_visibility="onchange", copy=False)
    catatan_revisi = fields.Text(string="Revisions", readonly="1", track_visibility="onchange")

    @api.multi
    def action_submit(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_project_leader").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to Review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'bast.bast', 'review', 'group', 'ab_role.group_project_leader')
        self.write({'state': 'submit'})

    @api.multi
    def action_review_pl(self):
        user_ids = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY]\n Dear User, This is outstanding transaction that need your action to Review. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'bast.bast', 'review', 'group', 'project.group_project_manager')
        self.write({"state": "review1"})

    @api.multi
    def action_review_pm(self):
        user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, This is outstanding transaction that need your action to Approve. \nThanks",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        self.env['send.email.notification'].send(self.id, 'bast.bast', 'approve', 'group', 'ab_role.group_direktur')
        self.write({"state": "review2"})

    @api.multi
    def action_done(self):
        if len(self.attachment_ids) == 0:
            raise UserError(("Attachment belum diisi !"))
        user_ids = []
        pm_group = _get_uid_from_group(self, self.env.ref("project.group_project_manager").id)
        for i in pm_group:
            user_ids.append(i)
        pa_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_admin").id)
        for j in pa_group:
            user_ids.append(j)
        ceo_group = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
        # pl_group = _get_uid_from_group(self, self.env.ref("ab_role.group_project_leader").id)
        for k in ceo_group:
            user_ids.append(k)
        
        single = []
        for i in user_ids:
            if i not in single:
                single.append(i)

        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in single]
        sendto_list = [self.env["res.users"].browse(uid).id for uid in single]
        for p in sendto_list:
            email = self.env['send.email.notification']
            sendto = self.env['res.users'].search([('id', '=', p)])
            email.send_bast(self.title_contract, self.no_contract, 
            self.first_id.name, self.date, 'user', sendto)
        self.env['send.email.notification'].send(self.id, 'bast.bast', 'approve', 'group', 'ab_role.group_direktur')

        self.message_post(
            body="BAST has been done by CEO!",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )
        project = self.env["project.project"].search([("id", "=", self.project_id.id)])
        project.update({"state": "pc"})
        self.write({"state": "done"})

    # sequence dari Reference
    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("bast.bast")
        return super(BastBast, self).create(vals)

    @api.onchange("title_contract", "contract_reference_id")
    def _change_note(self):
        if self.project_id or self.contract_reference_id:
            self.note = "Berdasarkan surat Perjanjian Kontrak kerja nomor " + \
                str(self.contract_reference_id.no_contract) + \
                " antara PIHAK PERTAMA dan PIHAK KEDUA tentang pekerjaan Proyek " + \
                str(self.title_contract) + \
                " dengan ketentuan yang telah diatur sesuai dalam surat Perjanjian Kontrak Kerja"

    @api.onchange("project_id")
    def _change_line(self):
        if self.bast_line:
            self.update({"bast_line": [(5, 0, 0)]})

        if self:
            ress = []
            rab_line = self.env["project.rab.quot"].search([("project_id", "=", self.project_id.id)])
            if rab_line:
                for x in rab_line:
                    isi = {
                        "item_id": x.templ_id.id,
                        "jumlah": x.qty_rab,
                        "satuan_id": x.uom_id,
                    }
                    ress.append(isi)

                if ress:
                    self.update({"bast_line": ress})

        data = self.env["menu.contract"].search([("project_id", "=", self.project_id.id)], limit=1)
        if data:
            self.contract_reference_id = data
        if not data:
            self.contract_reference_id = ""

    @api.multi
    def print_xls_report(self):
        return self.env.ref('ab_bast_bast.cetak_bast_xlsx').report_action(self)


class BastLine(models.Model):
    _name = "bast.line"
    _description = "Line Form Bast"

    bast_id = fields.Many2one("bast.bast", string="Bast Line")
    nomor = fields.Char(string="No")
    item_id = fields.Many2one("product.template", string="ITEM")
    jumlah = fields.Float(string="JUMLAH")
    satuan_id = fields.Many2one("uom.uom", string="SATUAN")


class AccountMove(models.Model):
    _inherit = "account.move"


class WizardBast(models.TransientModel):
    _name = "wizard.bast"
    _description = "Wizard Revisi"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def revisi_state(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            project = self.env["bast.bast"].browse(active_id)
            if project.write({"state": "draft"}):
                project.catatan_revisi = self.text_revisi
            else:
                project.catatan_revisi = self.text_revisi
