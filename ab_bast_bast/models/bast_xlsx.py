from odoo import fields, models, api
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, Warning
from odoo.tools.translate import _
from datetime import datetime, timedelta
from odoo.http import content_disposition, request
from odoo.modules.module import get_resource_path


class ReportBastXlsx(models.AbstractModel):
    _name = 'report.ab_bast_bast.report_cetak_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        heading_format = workbook.add_format(
            {'align': 'center', 'valign': 'vleft', 'bold': True, 'size': 12})
        alamat = workbook.add_format(
            {'valign': 'vleft', 'bold': True, 'text_wrap': 'wrap'})
        # alamat.set_text_wrap()
        sub_heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True,
                                                  'size': 9, 'color': 'white', 'bg_color': '#808080', 'border_color': 'white', 'border': 1})
        bold_data = workbook.add_format({'bold': True})
        normal_num_bold_no_currency = workbook.add_format(
            {'bold': True, 'num_format': '#,###0.00'})

        # add sheet
        worksheet = workbook.add_worksheet('BAST')

        # setting width of the column
        worksheet.set_column('A:A', 5)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('C:C', 20)
        worksheet.set_column('D:D', 10)
        worksheet.set_column('E:E', 10)
        worksheet.set_column('F:F', 25)

        row = 2
        worksheet.merge_range(
            'B2:E2', 'BERITA ACARA SERAH TERIMA', heading_format)
        # image = get_resource_path(
        #     'ab_bast_bast', 'static/description', 'logo.png')
        # worksheet.insert_image(
        #     row, 0, image,  {'x_scale': 0.2, 'y_scale': 0.2})

        worksheet.write('B7', 'Nama', bold_data)
        worksheet.write('B8', 'Jabatan', bold_data)
        worksheet.write('B9', 'Alamat', bold_data)
        worksheet.merge_range(
            'B14:E14', 'Selanjutnya disebut pihak PERTAMA')
        worksheet.write('B16', 'Nama', bold_data)
        worksheet.write('B17', 'Jabatan', bold_data)
        worksheet.write('B18', 'Perusahaan', bold_data)
        worksheet.write('B19', 'Alamat', bold_data)
        worksheet.merge_range(
            'B23:E23', 'Yang selanjutnya disebut sebagai pihak KEDUA')
        worksheet.merge_range(
            'B30:E30', 'Dengan perincian pekerjaan sebagai berikut: ')

        worksheet.write('B32', 'No', sub_heading_format)
        worksheet.write('C32', 'ITEM', sub_heading_format)
        worksheet.write('D32', 'JUMLAH', sub_heading_format)
        worksheet.write('E32', 'SATUAN', sub_heading_format)

        no = 0
        number = []
        item = []
        amount = []
        jumlah = []
        row = 33
        for x in obj:
            for y in x.bast_line:
                worksheet.merge_range(
                    'B3:E3', x.name, workbook.add_format({'align': 'center', 'bold': True}))
                worksheet.merge_range('C7:E7', x.first_id.name, bold_data)
                worksheet.merge_range('C8:E8', x.position1, bold_data)
                worksheet.merge_range(
                    'C9:E12', x.address1, alamat)
                worksheet.merge_range('C16:E16', x.partner_id.name, bold_data)
                worksheet.merge_range('C17:E17', x.position2, bold_data)
                worksheet.merge_range('C18:E18', x.company, bold_data)
                worksheet.merge_range('C19:E21', x.address2, bold_data)
                worksheet.merge_range(
                    'B5:E5', 'Kami yang bertanda tangan di bawah ini, pada tanggal ' + str(x.date))
                worksheet.merge_range('B25:E28', x.note, workbook.add_format(
                    {'text_wrap': 'wrap', 'align': 'justify'}))

                no += 1
                number.append(str(no))
                item.append(y.item_id.name or '')
                amount.append(str(y.jumlah) or '')
                jumlah.append(y.satuan_id.name or '')

                worksheet.write_column('B33', number)
                worksheet.write_column('C33', item)
                worksheet.write_column('D33', amount)
                worksheet.write_column('E33', jumlah)

                row += 1
        row += 1
        worksheet.merge_range('B' + str(row) + ':E' + str(row + 2), 'Selanjutnya dengan sudah selesainya pekerjaan tersebut, maka pihak PERTAMA dapat memperoleh pembayaran atas pekerjaanya. Sesuai dengan Surat Perjanjian Kerja yang bersangkutan',
                              workbook.add_format({'text_wrap': 'wrap', 'align': 'justify'}))
        row += 5
        worksheet.merge_range('B' + str(row) + ':C' +
                              str(row), 'PIHAK PERTAMA', workbook.add_format({'align': 'center'}))
        worksheet.merge_range('D' + str(row) + ':E' + str(row), "PIHAK KEDUA",
                              workbook.add_format({'align': 'center'}))

        row += 4
        worksheet.merge_range('B' + str(row) + ':C' + str(row),
                              x.first_id.name, workbook.add_format({'align': 'center'}))
        worksheet.merge_range('D' + str(row) + ':E' + str(row), x.partner_id.name,
                              workbook.add_format({'align': 'center'}))
        row += 1
        worksheet.merge_range('B' + str(row) + ':C' +
                              str(row), self.env.user.company_id.name, workbook.add_format({'align': 'center'}))
        worksheet.merge_range('D' + str(row) + ':E' + str(row), x.company,
                              workbook.add_format({'align': 'center'}))
