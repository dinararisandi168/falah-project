# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from odoo import models, fields, api, exceptions, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import models, fields, api
from lxml import etree
import time
import tempfile
import binascii
import xlrd
import io

class UploadFingerspot(models.TransientModel):
    _name = "upload.fingerspot"


    file_data = fields.Binary('File', required=True,)
    file_name = fields.Char('File Name')

    @api.multi
    def upload_fingerspot(self):
        if not self.file_data:
            raise Warning('Tidak Ada File Yang Untuk Di Import')
        fp = tempfile.NamedTemporaryFile(delete= False,suffix=".xlsx")
        fp.write(binascii.a2b_base64(self.file_data))
        fp.seek(0)

        workbook = xlrd.open_workbook(fp.name)
        sheet = workbook.sheet_by_index(0)

        cont = 0
        for row_no in range(sheet.nrows):
            cont += 1
            val = {}
            if row_no <= 0:
                fields = map(lambda row:row.value.encode('utf-8'), sheet.row(row_no))
            elif row_no>=2:
                line = list(map(lambda row:isinstance(row.value, bytes) and row.value.encode('utf-8') or str(row.value), sheet.row(row_no)))

                tanggal = line[0]
                nip = line[5]
                masuk = line[14]
                keluar = line[25]
                if len(masuk)>0 or len(keluar)>0:
                    employee = self.env['hr.employee'].search([('nik','=',nip)])
                    if employee:
                        cek_in = cek_out = False
                        if len(masuk)>0:
                            cek_in = datetime.strptime(tanggal + ' ' + masuk,"%d-%m-%Y %H:%M:%S") - timedelta(hours=7)
                        if len(keluar)>0:
                            cek_out = datetime.strptime(tanggal + ' ' + keluar,"%d-%m-%Y %H:%M:%S") - timedelta(hours=7)
                            
                        if not cek_in and cek_out:
                            cek_in = cek_out

                        absen_exist = self.env['hr.attendance'].search([('employee_id','=',employee.id),
                                                                        ('check_in','=',cek_in),
                                                                        ('check_out','=',cek_out),])
                        if not absen_exist:
                            absensi = self.env['hr.attendance'].create({
                                    'employee_id': employee.id,
                                    'check_in': cek_in,
                                    'check_out': cek_out,
                                        })    
#                        raise UserError(_('employee %s \n tanggal %s \n nip %s \n masuk %s \n keluar %s')%(employee.name,tanggal,nip,cek_in,cek_out,))

        return {
            'type': 'ir.actions.client',
            'tag': 'reload',
            }
