# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import AccessError, UserError, RedirectWarning, ValidationError, Warning
from odoo.addons.ab_role.models.models import _get_uid_from_group


class AttachmentList(models.Model):
    _inherit = "ir.attachment"
    _description = "List Attachment"

    name = fields.Char(string="Name")
    doc_type_id = fields.Many2one("document.type", string="Document Type")
    is_falah = fields.Boolean(string="Falah User")
    note = fields.Text(string="Description")
    check_attachment = fields.Boolean(string='')

    @api.model
    def create(self, vals):
        vals["name"] = self.env["ir.sequence"].next_by_code("no.attachment")
        if vals.get('datas_fname') and vals.get('check_attachment') == True:
            hasil = len(vals.get('datas_fname').split("-"))
            if not (hasil >= 4) or not (hasil <= 5):
                raise UserError(
                    "filename harus sesuai dengan format :\nDIVISI-JUDUL-NIK-YYYYMMDD\natau\nDIVISI-JUDUL-NIK-YYYYMMDD-REVISI KE")
        return super(AttachmentList, self).create(vals)


class DocType(models.Model):
    _name = "document.type"
    _description = "Mater Data Document Type"

    name = fields.Char(string="Document Type")
    note = fields.Text(string="Description")


def _send_message(object, group_user, subject=None, message_body=None):
    user_ids = _get_uid_from_group(object, object.env.ref(group_user).id)
    partner_ids = [(4, object.env['res.users'].browse(uid).partner_id.id) for uid in user_ids] if user_ids else []

    if partner_ids:
        object.env['mail.message'].create({
            'message_type': "email",
            "subtype": object.env.ref("mail.mt_comment").id,
            'body': message_body,
            'subject': subject or object.name or 'Notification',
            'needaction_partner_ids': partner_ids,
            'starred_partner_ids': partner_ids,
            'model': object._name,
            'res_id': object.id
        })
