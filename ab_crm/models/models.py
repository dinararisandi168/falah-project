# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CrmLead(models.Model):
    _inherit = "crm.lead"

    team_id = fields.Many2one('crm.team', string='Sales Team', oldname='section_id', default=False,
        index=True, track_visibility='onchange')


class CrmLeadLost(models.TransientModel):
    _inherit = "crm.lead.lost"

    lost_reason = fields.Text("Lost Reason")
