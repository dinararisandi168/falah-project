# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning
from datetime import datetime, timedelta
from odoo.exceptions import UserError


class ProjectTask(models.Model):
    _inherit = 'project.task'
    _description = "Custom Project Task"
    _order = "complete_name,parent_id,distribution"

    tasks = fields.One2many('project.task', 'project_id', string="Task Activities", domain=[("is_additional", "=", False)])
    tasks2 = fields.One2many('project.task', 'parent_id', string="Task Activities", domain=[("is_additional", "=", False)])
    sequence = fields.Integer(string=" ")
    complexity = fields.Selection(
        [
            ("vlow", "Very Low"),
            ("1", "Low"),
            ("2", "Medium"),
            ("3", "High"),
            ("4", "Very High"),
        ],
        string="Complexity",track_visibility="onchange")
    weight = fields.Float(string="Persentase(%)",track_visibility="onchange", store=True)
    weight2 = fields.Float(string="Persentase(%)",track_visibility="onchange", store=True)
    weight_fix = fields.Float(string="Weight Fix(%)") #ini tidak terpakai
    distribution = fields.Selection(string="Distribution", selection=[("prorate", "Prorate"),("fix", "Fix")],default="prorate",track_visibility="onchange")
    # state = fields.Selection(
    #     string="Status",
    #     selection=[
    #         ("open", "Open"),
    #         ("pending", "Pending"),
    #         ("close", "Close"),
    #     ],
    #     default="open",
    #     track_visibility="onchange",
    # )
    # progress = fields.Float(string="Progress(%)", track_visibility="onchange")
    progress = fields.Selection([(100, 'Yes'), (0, 'No'), ], string='Progress')
    progress_pct = fields.Float(string="Progress (%)",compute="_compute_progress_percentage")
    progress_deep = fields.Float(string="Total Subtasks",compute="_amount_deep_progress")
    issue_log = fields.Text(string="Issue Log", track_visibility="onchange")
    deliverables_id = fields.Many2one("deliverables.task", string="Deliverables", track_visibility="onchange")
    is_warn = fields.Boolean(string="color", readonly=False, compute="get_warning_situation")
    is_additional = fields.Boolean(string="", default=False)
    edit_task = fields.Boolean(string="Can Edit",compute="_change_auth_task", default=True)
    show_button = fields.Boolean(string='Can Show Button',compute="_change_auth_task", default=True)
    show_button2 = fields.Boolean(string='Can Show Button2',compute="_change_auth_task", default=True)
    pl_only = fields.Boolean(string='PL Only', compute="_change_auth_task")
    pl_pic = fields.Boolean(string='PL and Assign To', compute="_change_auth_task", default=True)
    # edit_timesheet = fields.Boolean(string='Can Edit Timesheet',compute="_change_auth_task", default=True)
    complete_name = fields.Char(string='Complete Name', compute="_compute_name", store=True)
    bobot = fields.Float(string='Bobot (%)', compute="_compute_bobot", store=True)
    
    state = fields.Selection(
        [
            ("draft", "Draft"),
            ("submit", "Submitted"),
            ("review", "PM Reviewed"),
            ("verify", "HR Reviewed"),
            ("approve", "FAT Reviewed"),
            ("plan", "CEO Approved / Draft Soldes"),
            ("submit2", "Submitted"),
            ("review2", "PM Reviewed"),
            ("verify2", "HR Reviewed"),
            ("approve2", "FAT Reviewed"),
            ("ps", "CEO Approved / Planning Schedule"),
            ("excute", "Submitted"),
            ("pm_review", "Reviewed PM"),
            ("close", "Approved CEO / Starting Project"),
            ("pc", "Project Closed"),
        ],
        default="draft",
        track_visibility="onchange", related="project_id.state"
    )
    # state = fields.Char(string='State', related="project_id.state")

    # @api.model
    # def create(self, vals):
    #     if vals.get('project_id') and vals.get('state') != 'ps' :
    #         raise UserError("INPUT PROJECT TASK HANYA BISA DI STATE (CEO Approved / Planning Schedule)")
    #     return super(ProjectTask, self).create(vals)
    
    @api.multi
    def _message_auto_subscribe_notify(self, partner_ids, template):
        pass
    
    @api.one
    @api.depends('progress','progress_pct')
    def _compute_progress_percentage(self):
        if self.progress==100 and self.subtask_count==0:
            self.progress_pct = 100
        elif self.subtask_count==0:
            self.progress_pct = 0

        if self.subtask_count>0:
            child_task = self.env['project.task'].search([('parent_id','=',self.id)])
            progress_pct = progress = 0
            bobot = 0
            for line in child_task:
                bobot += line.bobot * line.progress_pct / 100 

            if bobot>0 and self.bobot >0:
                self.progress_pct = (bobot / self.bobot) * 100
            else:
                self.progress_pct = 0
        self.parent_id._compute_progress_percentage()
                    

    @api.depends('subtask_count','weight','parent_id.weight')
    def _compute_bobot(self):
        for rec in self:
            if rec.parent_id:
                bobot = rec.weight * (rec.parent_id.weight /100)
                lanjut = True
                cur_rec = rec.parent_id
                while lanjut:
                    if cur_rec.parent_id:
                        bobot = bobot * (cur_rec.parent_id.weight /100)
                        cur_rec = cur_rec.parent_id
                    else:
                        lanjut = False
#                rec.bobot = bobot 
                rec.update({'bobot': bobot})
            else:
#                rec.bobot = rec.weight
                rec.update({'bobot': rec.weight})

    @api.onchange('subtask_count','weight','parent_id.weight')
    def _onchange_bobot(self):
        for rec in self:
            if rec.subtask_count>0:
                rec.bobot = 0
            if rec.parent_id:
                bobot = rec.weight * (rec.parent_id.weight /100)
                lanjut = True
                cur_rec = rec.parent_id
                while lanjut:
                    if cur_rec.parent_id:
                        bobot = bobot * (cur_rec.parent_id.weight /100)
                        cur_rec = cur_rec.parent_id
                    else:
                        lanjut = False
                rec.bobot = bobot 
#                rec.bobot = rec.weight * (rec.parent_id.weight /100)
            else:
                rec.bobot = rec.weight


    @api.depends('name','parent_id')
    def _compute_name(self):
        for rec in self:
            if not rec.parent_id:
                complete_name = rec.name
            else:
                if rec.parent_id.complete_name:
                    complete_name = rec.parent_id.complete_name + ' / ' + rec.name
                else:
                    complete_name = rec.parent_id.name + ' / ' + rec.name
            rec.complete_name = complete_name
            
    @api.multi
    def write(self, vals):
        res = super(ProjectTask, self).write(vals)
        return res

    @api.multi
    def unlink(self):
        anaknya = self.search([('parent_id', 'in', self.ids)])
        for child in anaknya:
            child.unlink()
        data_exist = self.env['project.task'].search([('id','=',self.ids)])
        if data_exist:
            super(ProjectTask, self).unlink()
        return 


    def _change_auth_task(self):
        for i in self:
            if i.project_id.user_id == i.env.user:
                i.pl_only=True
            else:
                i.pl_only=False

            if i.project_id.user_id == i.env.user or i.user_id == i.env.user:
                i.pl_pic=True
            else:
                i.pl_pic=False
                    
            if i.project_id.user_id == i.env.user or  i.env.user.id in i.project_id.asistant_ids.ids or i.user_id == i.env.user:
                i.edit_task = True
                i.show_button = True
            else:
                i.edit_task = False
                i.show_button = False
            for line in i.timesheet_ids:
                if i.env.user == line.employee_id.user_id:
                    i.show_button2 = True
                else:
                    i.show_button2 = False
    
    #  Kasih default stage_id
    def _get_default_stage_id(self):
        project_id = self.env.context.get("default_project_id")
        if not project_id:
            return False
        return self.stage_find(project_id, [("fold", "=", False)])

    stage_id = fields.Many2one("project.task.type",string="Status", ondelete="restrict",default=_get_default_stage_id,track_visibility="onchange",index=True,domain="[('sequence', '!=', -56),('name','in',['NEW','OPEN','CLOSE','CANCEL','PENDING'])]",)
    stage = fields.Char(string="Stage", related="stage_id.name", compute="_progress_yes")

    @api.onchange('stage')
    def _progress_yes(self):
        if self.stage in ('CANCEL','CLOSE'):
            self.update({"progress": 100})
        else:
            self.update({"progress": 0})

        



    @api.depends('date_end')
    def get_warning_situation(self):
        cur_date = datetime.today()
        acuan = cur_date + timedelta(days=3)
        for rec in self:
            if rec.date_end:
                if cur_date < rec.date_end <= acuan:
                    rec.is_warn = True
                else:
                    rec.is_warn = False

    # @api.onchange('weight', 'distribution')
    # def onchange_weight(self):
    #     for x in self:
    #         if x.weight and x.distribution == 'fix':
    #             x.weight = x.weight/100
    
    
    # SLOW LOADING
    def compute_wight_task(self):     
#        def update_db(value,tasks):
#            sql = """            
#            update project_task set weight = %s where id in %s            
#            """
#            # Contoh Menampilakan Error print (sql, 'Ini Debug SQL')
#            params= [value,tuple(tasks.ids) or (None,)]            
#            self._cr.execute(sql,tuple(params))
        
#        def search_task(task):            
#            where = "project_id = %s and parent_id = %s" % (self.project_id.id,self.parent_id.id)
#            where_ = "project_id = %s and parent_id is null" %  (self.project_id.id)                        
#            sql = " select id from project_task where " + (where if task else where_)            
#            self._cr.execute(sql)
#            return self._cr.dictfetchall()
        
#        def get_berat_parent():
#            value = 1
#            for x in self:
#                if x.parent_id:
#                    value = x.parent_id.weight
#            return value
                    
#        def generate_weight(tasks):
#            prorate = 0
#            tasks = self.search([('id','in',[x['id'] for x in tasks])])
#            task_prorate = self.env['project.task']
#            for task in tasks:                
#                list_prorate = [z.id for z in tasks if z.distribution == "prorate"]
#                total_fix = sum([z.weight for z in tasks if z.distribution == "fix"])
#                berat_parent = get_berat_parent()
#                if not task.parent_id:
#                    berat_parent = 100
#                prorate = (
#                    (berat_parent - total_fix) / len(list_prorate)
#                    if len(list_prorate) != 0
#                    else 1
#                )
#                if task.distribution == "prorate":
#                    task_prorate += task
#            update_db(prorate,task_prorate)
            
#        for task in self:
#            list_task = False
#            if not task.parent_id:
#                list_task = search_task(False)
#            if task.parent_id:
#                list_task = search_task(True)
#            generate_weight(list_task)
        return True

    @api.onchange("distribution","weight")
    def check_fix(self):
        for me in self:
            if me.distribution=='fix' and me.parent_id and me.parent_id.weight < me.bobot:
                raise UserError("The weight must be lower or the same as the parent weight!")

    @api.onchange("date_start","date_end")
    def change_date_end(self):
        for i in self:
            if i.date_start >= i.date_end:
                i.date_end = i.date_start 
                # i.date_start = i.date_end - timedelta(days=1)
                # raise UserError("The starting date must be before or the same as the ending date!")
            break

    @api.multi
    def action_open_form(self):
        view_id = self.env.ref('project.view_task_form2')
        return {
                "name": "Task",
                "view_type": "form",
                "view_mode": "form",
                "res_model": "project.task",
                "view_id": view_id.id,
                "type": "ir.actions.act_window",
                "target" : "new",
                "res_id" : self.id,
            }


    @api.multi
    def action_sharing(self):
        user_ids = []
        for ts in self.timesheet_ids:
            if ts.employee_id.user_id:
                user_ids.append(ts.employee_id.user_id.id)
        partner_ids = [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids]
        self.message_post(
            body="Task Available, Please Check!",
            subject=self.name,
            partner_ids=partner_ids,
            message_type="notification",
            subtype="mail.mt_comment",
        )


    @api.multi
    def action_commit(self):
        stage = self.env["project.task.type"].search([("name", "!=", "Z9")])
        for i in stage:
            if i.name == "Complete":
                self.write({"stage_id": i.id})

    @api.multi
    def action_revisi(self):
        stage = self.env["project.task.type"].search([("name", "!=", "Z9")])
        for i in stage:
            if i.name == "Open":
                self.write({"stage_id": i.id})

    @api.multi
    def action_pending(self):
        stage = self.env["project.task.type"].search([("name", "!=", "Z9")])
        for i in stage:
            if i.name == "Pending":
                self.write({"stage_id": i.id})

    @api.depends("tasks.progress", "tasks.weight")
    def _amount_deep_progress(self):
        total_progres = 0
        # persen = 0
        for me in self:
            for hasil in me.tasks:
                if hasil.progress == 100:
                    total_progres += hasil.weight
            me.update({"progress_deep": total_progres/100})
            # for me in self: TIDAK JADI
            # for hasil in me.timesheet_ids: 
            #     if hasil.progress == 100:
            #         total_progres += hasil.weight
            # if total_progres > 0 and me.weight > 0:
            #     persen = total_progres/me.weight
            #     me.update({"progress_deep": persen, })

    @api.onchange('timesheet_ids')
    def _onchange_weight(self):
        total = 0
        for o in self.timesheet_ids:
            # o.weight = o.weight/100
            total += o.weight
        if total > self.weight:
            raise UserError("Weight Overload!")
    
    @api.onchange('progress')
    def _onchange_progress(self):
        for rec in self:
            progress = self.progress
            if rec.subtask_count==0:
                if rec.parent_id:
                    task = self.env['project.task'].search([('parent_id','=',rec.parent_id.id)])
                    for line in task:
                        if line.id!=rec.id and line.progress!=100:
                            progress = 0
                    rec.parent_id.update({'progress': progress})

        for line in self.timesheet_ids:
            if line.progress != 100:            
                raise UserError("Action Denied! Please Finished First Progress In Timesheet!")

class Deliverables(models.Model):
    _name = "deliverables.task"
    _description = "Deliverables(Attachment) dan Supporting Job(Cheklist)"

    name = fields.Char(string="Name", default="Deiverables")
    user_id = fields.Many2one('res.users', string='Created By',default=lambda self: self.env.user)    
    attachment_ids = fields.Many2many(comodel_name='ir.attachment', string='Attachment List')
    support_line = fields.One2many("supporting.job", "deliverables_id", string="Supporting Job")

class SupportingJob(models.Model):
    _name = "supporting.job"
    _rec_name = 'pic_id'
    _description = "Isi attachment deliverables"

    # name = fields.Char(string="NAME")
    is_job = fields.Boolean(string="Checklist")
    pic_id = fields.Many2one("hr.employee", string="PIC")
    job_id = fields.Many2one("hr.job", string="Job Position")
    remarks = fields.Char(string="Remarks")
    deliverables_id = fields.Many2one("deliverables.task", string="deliverables")

    @api.onchange('pic_id')
    def _onchange_pic_id(self):
        if self.pic_id:
            self.job_id = self.pic_id.job_id

class AnalyticAccountLine(models.Model):
    _inherit = "account.analytic.line"

    progress = fields.Selection([(100, 'Yes'), (0, 'No'), ], string='Progress')
    weight = fields.Float(string="Persentase(%)",digits=(0, 0),)
    yes_ts = fields.Boolean(string='PL PIC Employee Detected',compute='_change_author')
    input_ts = fields.Boolean(string='Pl dan PIC Detected',compute='_change_author')
    
    def _change_author(self):
        for i in self:
            if i.task_id.pl_only or i.task_id.pl_pic:
                i.yes_ts = True
            else:
                i.yes_ts = False
                
            if i.task_id.pl_only or i.task_id.pl_pic or i.employee_id.user_id == i.env.user:
                i.yes_ts = True
            else:
                i.yes_ts = False

    # @api.onchange('weight')
    # def _onchange_weight(self):
    #     for o in self:
    #         o.weight = o.weight/100
  
