# -*- coding: utf-8 -*-
{
    "name": "Project Task",
    "summary": """
        ---""",
    "description": """
        ---    """,
    "author": "PT. ISMATA NUSANTARA ABADI - 087881071515",
    "website": "www.ismata.co.id",
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "project", "hr_timesheet","hr", "ees_tree_no_open", "ab_attachment", 
    "base_automation", "ab_project", "ab_role",'project_native', 'ab_send_email'],
    # always loaded
    "data": [
        "security/security.xml",
        "security/ir.model.access.csv",
        "data/data.xml",
        "views/all_task.xml",
        "views/project_task.xml",
        "views/progress_status.xml",
        "views/deliverables.xml",
        "views/automation.xml",
        "views/additional_task.xml",
        "report/report_analysis.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}
