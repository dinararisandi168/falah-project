from odoo import api, fields, models, _
from odoo.addons.ab_role.models.models import _get_uid_from_group
from odoo.exceptions import UserError, RedirectWarning, ValidationError


class PaymentRequest(models.Model):
    _inherit = "payment.request"

    employee_id = fields.Many2one("hr.employee", string="Nama")
    start_date = fields.Date(string="Tanggal Mulai Bergabung / Bekerja", related='employee_id.tanggal_join')
    date_submission = fields.Date(string="Tanggal Pengajuan Klaim")
    sick_date = fields.Date(string="Tanggal Sakit")
    medical_expenses = fields.Monetary(string="Jumlah Biaya Berobat", compute="_amount")
    medical_claimed = fields.Monetary(string="Biaya Berobat Yang Sudah Diklaim")
    limit_claimed = fields.Monetary(string="Limit Biaya Berobat Yang Tersisa")
    total_medical = fields.Monetary(string="Jumlah Biaya Berobat Saat Ini", compute="_amount")
    job_id = fields.Many2one("hr.job", string="Jabatan",related='employee_id.job_id')
    hospital = fields.Char(string="Rumah Sakit / Klinik")
    outpatient = fields.Boolean(string="Rawat Jalan")
    inpatient = fields.Boolean(string="Rawat Inap")
    operation = fields.Boolean(string="Operasi")
    catatan_revisi = fields.Text(string="Revisions", readonly="1", track_visibility="onchange")
    is_medical = fields.Boolean()
    
    
    def get_contract(self, employee_id):
        running = self.env['hr.contract'].search([('employee_id', '=', employee_id), ('state', '=', 'open')], limit=1)
        if running:
            last_contract = self.env['hr.contract'].search([('employee_id', '=', employee_id), ('id', '<', running.id)], limit=1)
            return running if not last_contract else last_contract
        else:
            raise UserError(('Employee Does not Have Running Contract'))

    @api.onchange('employee_id')
    def _onchange_employee_id(self):
        if self.employee_id and self.payment_type == 'medical':
            running = self.env['hr.contract'].search([('employee_id','=',self.employee_id.id),('state','=','open')], limit=1)
            if running:
                data_hc = self.env['payment.request'].search([('employee_id','=',self.employee_id.id),('is_medical','=',True),('date_submission','>=',running.date_start),('state','in',['done','paid']),('payment_type','=','medical')])
                self.medical_claimed = sum(data_hc.mapped('amount'))
            self.limit_claimed = self.get_contract(self.employee_id.id).wage - self.medical_claimed
    
    @api.onchange('amount', "medical_expenses", "total_medical")
    def _amount(self):
        for amount in self:
            amount.medical_expenses = amount.amount
            amount.total_medical = amount.amount

    @api.model
    def create(self, vals):
        res = super(PaymentRequest, self).create(vals)
        if res.is_medical:
            res.name = self.env["ir.sequence"].next_by_code("health.claims")
        return res

    @api.multi
    def payment_draft(self):
        for o in self:
            return o.write({"state": "draft"})

    @api.multi
    def payment_cancel(self):
        for o in self:
            return o.write({"state": "cancel"})

    @api.multi
    def payment_verified(self):
        if self.is_medical:
            user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_hod_fat").id)
            partner = [(6, 0, [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids])]
            form_name = 'Health Claims'

            if partner[0][2][0]:
                self.message_post(
                    body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, \nThis is outstanding transaction that need your action to review. \nThanks",
                    subject=form_name + ' - ' + self.name + ' - [Need Review]',
                    partner_ids=partner,
                    message_type="notification",
                    subtype="mail.mt_comment",
                )
        self.write({"state": "confirm"})

    @api.multi
    def payment_open(self):
        res = super(PaymentRequest, self).payment_open()
        if self.is_medical:
            self.write({"state": "verified"})
        return res

    # @api.multi
    # def payment_done(self):
    #     res = super(PaymentRequest, self).payment_open()
    #     print("=======================WOYYYYY")
    #     if self.is_medical:
    #         user_ids = _get_uid_from_group(self, self.env.ref("ab_role.group_direktur").id)
    #         partner = [(6, 0, [self.env["res.users"].browse(uid).partner_id.id for uid in user_ids])]
    #         form_name = 'Health Claims'

    #         if partner[0][2][0]:
    #             self.message_post(
    #                 body="[THIS IS AUTOMATIC MESSAGING SYSTEM. NO REPLY] \nDear User, \nThis is outstanding transaction that need your action to review. \nThanks",
    #                 subject=form_name + ' - ' + self.name + ' - [Need Review]',
    #                 partner_ids=partner,
    #                 message_type="notification",
    #                 subtype="mail.mt_comment",
    #             )
    #         self.write({"state": "done"})
    #     return res


class WizardHealthClaim(models.TransientModel):
    _name = "wizard.health.claim"
    _description = "Wizard Health Claim"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def revisi_state_draft(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            project = self.env["payment.request"].browse(active_id)
            if project.write({"state": "draft"}):
                project.catatan_revisi = self.text_revisi
            else:
                project.catatan_revisi = self.text_revisi
