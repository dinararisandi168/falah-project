from odoo import models, fields, api

class RefusePOWizard(models.TransientModel):
    _name = "refuse.po.wizard"
    _description = "Refuse PO Wizard"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def submit(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            po = self.env["purchase.order"].browse(active_id)
            po.revision = self.text_revisi
            po.write({"state": "draft"})
