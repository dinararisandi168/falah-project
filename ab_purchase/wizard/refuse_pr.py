from odoo import models, fields, api

class RefusePRWizard(models.TransientModel):
    _name = "refuse.pr.wizard"
    _description = "Refuse PR Wizard"

    text_revisi = fields.Text(string="Catatan Revisi")

    @api.multi
    def submit(self):
        active_id = self.env.context.get("active_id")
        if active_id:
            pr = self.env["purchase.requisition"].browse(active_id)
            pr.revision = self.text_revisi
            pr.write({"state": "draft"})
