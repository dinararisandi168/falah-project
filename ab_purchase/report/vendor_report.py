from odoo import fields, models, api
from datetime import timedelta, datetime
from dateutil import relativedelta
import datetime
from xlsxwriter.utility import xl_range
from xlsxwriter.utility import xl_rowcol_to_cell


class VendorReportXlsx(models.AbstractModel):
    _name = 'report.ab_purchase.vendor_report_xlsx'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, obj):
        text_style = workbook.add_format({'font_size': 10, 'left': 1, 'bottom': 1, 'right': 1, 'top': 1, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        text_no_border = workbook.add_format({'font_size': 10, 'align': 'center', 'valign': 'vcenter', 'text_wrap': True, })
        heading_format = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 14})
        cell_text_format = workbook.add_format({'align': 'left', 'valign': 'vcenter', 'bold': True, 'size': 12})
        cell_text_format_top_left_right = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'bold': True, 'size': 12, 'top': 1, 'left': 1, 'right': 1, 'bottom': 1})
        cell_text_format_top_left_right.set_bg_color('#4781fd')
        worksheet = workbook.add_worksheet('Vendor List Report')
        worksheet.set_column('A:A', 20)
        worksheet.set_column(0, 0, 5)
        worksheet.set_column(1, 1, 20)
        worksheet.set_column(2, 2, 50)
        worksheet.set_column(3, 5, 20)
        # column row column row
        worksheet.merge_range(4, 0, 5, 0, 'NO', cell_text_format_top_left_right)
        worksheet.merge_range('C1:E2', 'VENDOR LIST REPORT', heading_format)

        no = 1
        NO = []

        row = 5

        query = """with master_data as(
                select rp.id as id_contact, rp.parent_id as parent, rp.vendor_number as vendor_no,rp.name as vendor_name,rp.phone as phone,rp.active as active
                from res_partner rp
                where rp.supplier = True),

                contact_name as (
                select name as name_contact, parent_id
                from res_partner
                where parent_id is not null and supplier = True
                )
                select md.vendor_no, md.vendor_name, cn.name_contact, md.phone, md.active
                from master_data md
                left join contact_name cn on cn.parent_id = md.id_contact
                where md.parent is null """

        self.env.cr.execute(query)
        data = self.env.cr.dictfetchall()

        worksheet.merge_range(4, 1, 5, 1, 'Vendor No', cell_text_format_top_left_right)
        worksheet.merge_range(4, 2, 5, 2, 'Name', cell_text_format_top_left_right)
        worksheet.merge_range(4, 3, 5, 3, 'Contact', cell_text_format_top_left_right)
        worksheet.merge_range(4, 4, 5, 4, 'Phone', cell_text_format_top_left_right)
        worksheet.merge_range(4, 5, 5, 5, 'Suspended', cell_text_format_top_left_right)

        for x in data:

            inc = [
                x["vendor_no"],
                x["vendor_name"],
                x["name_contact"],
                x["phone"],
                'Yes' if x["active"] == False else "",
            ]
            NO.append(no)
            no += 1
            row += 1
            col = 1

            for v in inc:
                worksheet.write(row, col, v, text_style)
                col += 1

        worksheet.write_column('A7', NO, text_style)
