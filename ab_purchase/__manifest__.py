# -*- coding: utf-8 -*-
{
    "name": "Purchase Custom Falah",
    "summary": """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",
    "description": """
        Long description of module's purpose
    """,
    "author": "PT. ISMATA NUSANTARA ABADI",
    "website": "http://www.ismata.co.id",
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    "category": "Uncategorized",
    "version": "0.1",
    # any module necessary for this one to work correctly
    "depends": ["base", "purchase", "purchase_requisition", 
    "project","ab_attachment", "aa_payment_request","ab_role", "ab_send_email"],
    # always loaded
    "data": [
        'security/ir.model.access.csv',
        'security/security.xml',
        'wizard/refuse_po.xml',
        'wizard/refuse_pr.xml',
        "report/report_rfq.xml",
        "report/report_po.xml",
        "views/views.xml",
        "views/templates.xml",
        "report/action_report.xml",
        "report/vendor_report.xml",
        "report/purchase_request_report.xml",
    ],
    # only loaded in demonstration mode
    "demo": [
        "demo/demo.xml",
    ],
}