## Module <Project Native Report Advance>


#### Version 12.21.01.19.0
##### Update
PDF arrows


#### Version 12.20.11.06.
##### FIX
- Item: DateTime if none or ... print none.


#### Version 12.20.02.07.2
##### FIX
- Item: DateTime in TZ.


#### Version 13.20.02.06.0
##### ADD
- Export to PDF from any in web.



#### Version 13.20.02.07.2
##### FIX
- Scale: First-Last day.
